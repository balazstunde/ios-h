//
//  DayPickerController.swift
//  Choo Choo
//
//  Created by Halcyon on 29/07/2019.
//

import UIKit
import FSCalendar

protocol DayPickControllerDelegate: AnyObject {
    func didTapConfirm(on viewController: DayPickController)
    func didTapClose(on viewController: DayPickController)
}

class DayPickController: BaseViewController {
    weak var delegate: DayPickControllerDelegate?
    
    private var upperSection: UIView!
    private var lowerSection: UIView!
    private var selectedDateSection: UIStackView!
    private var outboundLabel: UILabel!
    private var selectedDateLabel: UILabel!
    private var titleView: UILabel!
    private var confirmButton: UIButton!
    private var nextImage: UIImageView!
    private var prevImage: UIImageView!
    private let cornerRadius: CGFloat = 5
    private var calendarPicker: FSCalendar!
    private var calendar: FSCalendar!
    private var selectedDate: Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupConstants()
    }
    
    private func setupUI() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.iconClose.image, style: .plain, target: self, action: #selector(closeButtonSelected))
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: Font.barlowSemiBold(size: .large)]
        navigationItem.title = "Select outbound date"
        
        view.backgroundColor = .primary
        
        upperSection = UIView()
        upperSection.backgroundColor = .primary
        
        outboundLabel = UILabel()
        outboundLabel.text = "Outbound"
        outboundLabel.textColor = .white
        outboundLabel.font = Font.barlowRegular(size: FontSize.medium)
        
        selectedDateLabel = UILabel()
        selectedDateLabel.text = DateFormatter.getDateAsString(inFormat: DateFormats.nameOfTheDayMonthDay, date: Date())
        selectedDateLabel.textColor = .white
        selectedDateLabel.font = Font.barlowSemiBold(size: FontSize.medium)
        
        selectedDateSection = UIStackView(arrangedSubviews: [outboundLabel, selectedDateLabel])
        selectedDateSection.addBackground(color: .selected, cornerRadius: cornerRadius)
        selectedDateSection.axis = .vertical
        selectedDateSection.layoutMargins = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        selectedDateSection.isLayoutMarginsRelativeArrangement = true
        selectedDateSection.distribution = .fillEqually
        
        lowerSection = UIView()
        lowerSection.backgroundColor = .white
        
        confirmButton = UIButton()
        confirmButton.backgroundColor = .secondary
        confirmButton.setTitle("Confirm", for: .normal)
        confirmButton.layer.cornerRadius = cornerRadius
        confirmButton.titleLabel?.font = Font.barlowSemiBold(size: FontSize.medium)
        confirmButton.addTarget(self, action: #selector(confirmedDate), for: .touchUpInside)
        
        nextImage = UIImageView(image: Asset.iconNavigation.image)
        nextImage.isUserInteractionEnabled = true
        nextImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextTapped)))
        
        prevImage = UIImageView(image: Asset.iconNavigation.image)
        prevImage.transform = CGAffineTransform(scaleX: -1, y: 1)
        prevImage.isUserInteractionEnabled = true
        prevImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(previousTapped)))
        
        calendarPicker = FSCalendar()
        calendarPicker.firstWeekday = 2
        calendarPicker.clipsToBounds = true
        calendarPicker.appearance.headerMinimumDissolvedAlpha = 0.0
        calendarPicker.appearance.todayColor = .white
        calendarPicker.appearance.selectionColor = .secondary
        calendarPicker.appearance.borderRadius = 0.2
        calendarPicker.appearance.weekdayTextColor = .black
        calendarPicker.appearance.weekdayFont = Font.barlowRegular(size: .medium)
        calendarPicker.appearance.headerTitleFont = Font.barlowSemiBold(size: .medium)
        calendarPicker.appearance.titleTodayColor = .black
        calendarPicker.appearance.headerTitleColor = .black
        calendarPicker.appearance.titleFont = Font.barlowSemiBold(size: .mediumSmall)
        calendarPicker.appearance.caseOptions = FSCalendarCaseOptions.weekdayUsesSingleUpperCase
        calendarPicker.rowHeight = 20
        calendarPicker.dataSource = self
        calendarPicker.delegate = self
        
        view.addSubview(upperSection)
        view.addSubview(lowerSection)
        upperSection.addSubview(selectedDateSection)
        lowerSection.addSubview(confirmButton)
        lowerSection.addSubview(calendarPicker)
        lowerSection.addSubview(nextImage)
        lowerSection.addSubview(prevImage)
    }
    
    private func setupConstants() {
        upperSection.translatesAutoresizingMaskIntoConstraints = false
        lowerSection.translatesAutoresizingMaskIntoConstraints = false
        selectedDateSection.translatesAutoresizingMaskIntoConstraints = false
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        calendarPicker.translatesAutoresizingMaskIntoConstraints = false
        nextImage.translatesAutoresizingMaskIntoConstraints = false
        prevImage.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
        upperSection.topAnchor.constraint(equalTo: view.topAnchor),
        upperSection.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        upperSection.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        upperSection.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.20),
        
        lowerSection.topAnchor.constraint(equalTo: upperSection.bottomAnchor),
        lowerSection.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        lowerSection.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        lowerSection.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        
        selectedDateSection.leadingAnchor.constraint(equalTo: upperSection.leadingAnchor, constant: 20),
        selectedDateSection.trailingAnchor.constraint(equalTo: upperSection.trailingAnchor, constant: -20),
        selectedDateSection.centerYAnchor.constraint(equalTo: upperSection.centerYAnchor),
        selectedDateSection.heightAnchor.constraint(equalToConstant: 60),
        
        confirmButton.bottomAnchor.constraint(greaterThanOrEqualTo: lowerSection.bottomAnchor, constant: -25),
        confirmButton.leadingAnchor.constraint(equalTo: lowerSection.leadingAnchor, constant: 16),
        confirmButton.trailingAnchor.constraint(equalTo: lowerSection.trailingAnchor, constant: -16),
        confirmButton.heightAnchor.constraint(equalTo: lowerSection.heightAnchor, multiplier: 0.12),

        calendarPicker.leadingAnchor.constraint(equalTo: lowerSection.leadingAnchor, constant: 35),
        calendarPicker.trailingAnchor.constraint(equalTo: lowerSection.trailingAnchor, constant: -35),
        calendarPicker.topAnchor.constraint(lessThanOrEqualTo: lowerSection.topAnchor, constant: 66.5),
        calendarPicker.bottomAnchor.constraint(greaterThanOrEqualTo: confirmButton.topAnchor, constant: -63.5),
        calendarPicker.heightAnchor.constraint(equalTo: lowerSection.heightAnchor, multiplier: 0.6),
        
        nextImage.trailingAnchor.constraint(equalTo: lowerSection.trailingAnchor, constant: -51.5),
        nextImage.topAnchor.constraint(equalTo: lowerSection.topAnchor, constant: 75.5),
        
        prevImage.leadingAnchor.constraint(equalTo: lowerSection.leadingAnchor, constant: 51.5),
        prevImage.topAnchor.constraint(equalTo: lowerSection.topAnchor, constant: 75.5)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func getSelectedDate() -> Date {
        return selectedDate
    }
    
    @objc private func nextTapped(_ sender: Any) {
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: calendarPicker.currentPage)
        calendarPicker.setCurrentPage(nextMonth!, animated: true)
    }
    
    @objc private func previousTapped(_ sender: Any) {
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendarPicker.currentPage)
        calendarPicker.setCurrentPage(previousMonth!, animated: true)
    }
    
    @objc private func confirmedDate() {
        delegate?.didTapConfirm(on: self)
    }
    
    @objc func closeButtonSelected() {
        delegate?.didTapClose(on: self)
    }
}

extension DayPickController: FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    // if a date is selected set the selectedDateLabel text properly
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedDate = date
        selectedDateLabel.text = DateFormatter.getDateAsString(inFormat: DateFormats.nameOfTheDayMonthDay, date: date)
    }
    
    // the minimum selectable date is today
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    // the maximum date is the end of 2019
    func maximumDate(for calendar: FSCalendar) -> Date {
        return DateFormatter.getStringAsDate(inFormat: DateFormats.yearMonthDay, date: "2019/12/31") ?? Date()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        if Calendar.current.isDateInToday(date) {
            return #colorLiteral(red: 0.8666666667, green: 0.8156862745, blue: 0.8392156863, alpha: 1)
        }
        return .white
    }
}
