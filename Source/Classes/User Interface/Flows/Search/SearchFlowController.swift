//
//  SearchFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

class SearchFlowController: NavigationFlowController {
    private let passengerDetails = BookingService()
    
    private lazy var searchViewController: SearchViewController = {
        let viewController = SearchViewController()
        viewController.delegate = self
        return viewController
    }()

    override var firstScreen: UIViewController {
        return searchViewController
    }
}

extension SearchFlowController: SearchViewControllerFlowDelegate {
    func didTouchSwitchButton(on viewController: SearchViewController) {
        if passengerDetails.ticketDetails.fromDestination != nil && passengerDetails.ticketDetails.toDestination != nil {
            let fromDestination = passengerDetails.ticketDetails.fromDestination
            passengerDetails.ticketDetails.fromDestination = passengerDetails.ticketDetails.toDestination
            passengerDetails.ticketDetails.toDestination = fromDestination
        }
    }
    
    func didTapDatePickerView(on viewController: SearchViewController) {
        let datePickerController = DayPickController()
        datePickerController.delegate = self
        navigationController.present(UINavigationController(rootViewController: datePickerController), animated: true)
    }
    
    func didTapPersonPickerView(on viewController: SearchViewController) {
        let passengerDetailsViewController = PassengerDetailsViewController(passengersDetailsService: passengerDetails)
        passengerDetailsViewController.delegate = self
        navigationController.present(passengerDetailsViewController, animated: true)
    }
    
    func didTouchLocationField(on viewController: SearchViewController, location: SearchStationType) {
        let searchLocationViewController = SearchStationViewController(type: location)
        searchLocationViewController.delegate = self
        navigationController.present(UINavigationController(rootViewController: searchLocationViewController), animated: true)
    }
    func didTapSearchButton(on viewController: SearchViewController) {
        let findingFareView = FindingFareViewController(withDetails: passengerDetails)
        findingFareView.delegate = self
        navigationController.pushViewController(findingFareView, animated: true)
    }
}

extension SearchFlowController: PassengerDetailViewControllerDelegate {
    func didTapOutOfDetails(on viewController: PassengerDetailsViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapConfirm(on viewController: PassengerDetailsViewController) {
        viewController.dismiss(animated: true, completion: nil)
        guard let details = passengerDetails.getDetails() else { return }
        searchViewController.updatePersonDetails(with: details)
    }
}

extension SearchFlowController: DayPickControllerDelegate {
    func didTapClose(on viewController: DayPickController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapConfirm(on viewController: DayPickController) {
        passengerDetails.ticketDetails.date = viewController.getSelectedDate()
        viewController.dismiss(animated: true, completion: nil)
        searchViewController.updateDateDetails(with: passengerDetails.ticketDetails.date)
    }
}

extension SearchFlowController: SearchLocationDelegate {
    func didSelectLocation(on viewController: SearchStationViewController, location: Station, viewControllerType: SearchStationType) {
        switch viewControllerType {
        case .departure:
            passengerDetails.ticketDetails.fromDestination = location
            searchViewController.setDeparture(departure: location.name)
            if passengerDetails.ticketDetails.toDestination != nil {
                searchViewController.enableSearchButton()
            }
        case .destination:
            passengerDetails.ticketDetails.toDestination = location
            searchViewController.setDestination(destination: location.name)
            if passengerDetails.ticketDetails.fromDestination != nil {
                searchViewController.enableSearchButton()
            }
        }

        viewController.dismiss(animated: true, completion: nil)
        navigationController.popToRootViewController(animated: false)
    }
    
    func didPressBackButton(on viewController: SearchStationViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension SearchFlowController: FindingFareViewControllerDelegate {

    func didRequestFail(on viewController: FindingFareViewController) {
        navigationController.presentAlert(customTitle: "Alert!", customMessage: "No stations found!\nCheck connection!")
    }

    func didTapToLabel(on viewController: FindingFareViewController, type: SearchStationType) {
        viewController.dismiss(animated: false, completion: nil)
        let searchLocationViewController = SearchStationViewController(type: type)
        searchLocationViewController.delegate = self
        navigationController.present(UINavigationController(rootViewController: searchLocationViewController), animated: true)
    }
    
    func didTapFromLabel(on viewController: FindingFareViewController, type: SearchStationType) {
        viewController.dismiss(animated: false, completion: nil)
        let searchLocationViewController = SearchStationViewController(type: type)
        searchLocationViewController.delegate = self
        navigationController.present(UINavigationController(rootViewController: searchLocationViewController), animated: true)
    }
    
    func didTouchOnCell(on viewController: FindingFareViewController, journey: Journey) {
        viewController.dismiss(animated: true, completion: nil)
        let bookingViewController = BookingViewController(journey: journey, bookingService: passengerDetails)
        bookingViewController.delegate = self
        navigationController.pushViewController(bookingViewController, animated: true)
    }
}

extension SearchFlowController: BookingViewControllerDelegate {
    func didTapCheckOutButton(on viewController: BookingViewController, journey: Journey) {
        // TODO: add logic over this button
    }

    func didTapSeeStopsButton(on viewController: BookingViewController, journey: Journey) {
        navigationController.pushViewController(FareOverviewViewController(journey: journey), animated: true)
    }
    
    func didTapToLabel(on viewController: BookingViewController, type: SearchStationType) {
        viewController.dismiss(animated: false, completion: nil)
        let searchLocationViewController = SearchStationViewController(type: type)
        searchLocationViewController.delegate = self
        navigationController.present(UINavigationController(rootViewController: searchLocationViewController), animated: true)
    }
    
    func didTapFromLabel(on viewController: BookingViewController, type: SearchStationType) {
        viewController.dismiss(animated: false, completion: nil)
        let searchLocationViewController = SearchStationViewController(type: type)
        searchLocationViewController.delegate = self
        navigationController.present(UINavigationController(rootViewController: searchLocationViewController), animated: true)
    }
    
    func presentActivityVC(on viewController: UIViewController, departureStation: String, destinationStation: String, trainNumber: String, departureTime: String) {
        let activityViewController = viewController.createActivityVC(departureStation: departureStation, destinationStation: destinationStation, trainNumber: trainNumber, departureTime: departureTime)
        navigationController.present(activityViewController, animated: true, completion: nil)
    }
}

