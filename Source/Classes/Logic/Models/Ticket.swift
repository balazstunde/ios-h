import Foundation

struct Ticket: Codable {
    let barCode: String
    let date: String
    let endTrainStopDto: TrainStop
    let id: Int
    let pricingCategory: PricingCategory
    let pricingDto: Price
    let startTrainStopDto: TrainStop
    let travellingClass: TravelType
    let tripIdentifier: String
    let userId: Int
}
