//
//  PassengerDetailsCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

class PassengerDetailsCell: UITableViewCell {
    
    private let nameLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let plusButton = UIButton()
    private let minusButton = UIButton()
    private let numberLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configurewithFareCategory(_ fareCategory: FareCategory) {
        nameLabel.text = fareCategory.title
        descriptionLabel.text = fareCategory.description
    }
    
    private func setupView() {
        
        plusButton.setTitle("+", for: .normal)
        plusButton.backgroundColor = UIColor(red: 176/255, green: 8/255, blue: 58/255, alpha: 1)
        plusButton.setTitleColor(UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1), for: .normal)
        plusButton.layer.cornerRadius = 5
        plusButton.addTarget(self, action: #selector(increaseNumber), for: .touchUpInside)
        
        minusButton.setTitle(" - ", for: .normal)
        minusButton.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        minusButton.setTitleColor(UIColor(red: 176/255, green: 8/255, blue: 58/255, alpha: 1), for: .normal)
        minusButton.layer.cornerRadius = 5
        minusButton.addTarget(self, action: #selector(decreaseNumber), for: .touchUpInside)
        
        nameLabel.font = Font.barlowSemiBold(size: FontSize.medium)
        descriptionLabel.font = Font.regular(size: FontSize.medium)
        numberLabel.font  = Font.regular(size: FontSize.medium)
        
        numberLabel.contentMode = .center
        contentView.addSubview(plusButton)
        contentView.addSubview(minusButton)
        contentView.addSubview(numberLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(descriptionLabel)
    }
    
    private func setupConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        minusButton.translatesAutoresizingMaskIntoConstraints = false
        plusButton.translatesAutoresizingMaskIntoConstraints = false
        numberLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            nameLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            nameLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 5),
            nameLabel.widthAnchor.constraint(equalToConstant: 250),
            
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 2),
            descriptionLabel.widthAnchor.constraint(equalToConstant: 250),
            
            plusButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            plusButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            plusButton.heightAnchor.constraint(equalToConstant: 35),
            plusButton.widthAnchor.constraint(equalToConstant: 35),
            
            numberLabel.trailingAnchor.constraint(equalTo: plusButton.leadingAnchor, constant: -20),
            numberLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 2),
            numberLabel.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: 2),
            
            minusButton.trailingAnchor.constraint(equalTo: numberLabel.leadingAnchor, constant: -20),
            minusButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            minusButton.heightAnchor.constraint(equalToConstant: 35),
            minusButton.widthAnchor.constraint(equalToConstant: 35)
            
        ]
        NSLayoutConstraint.activate(constraints)
        
    }
    func configureWithFare(_ fare: FareCategory) {
        nameLabel.text = fare.title
        descriptionLabel.text = fare.description
    }
    
    @objc private func increaseNumber() {
        guard let numberString = numberLabel.text, let numberInt = Int(numberString) else { return }
        numberLabel.text = String(numberInt + 1)
        
    }
    
    @objc private func decreaseNumber() {
        guard let numberString = numberLabel.text, let numberInt = Int(numberString) else { return }
        if numberInt != 0 {
            numberLabel.text = String(numberInt - 1)
        }
    }
    
    func getData() -> String? {
        guard let numberString = numberLabel.text else { return nil }
        return numberString
    }
    func getName() -> String? {
        guard let nameString = nameLabel.text else { return nil }
        return nameString
    }
    func setNumber(number: Int) {
        numberLabel.text = String(number)
    }
}
