//
//  StackView+aExtensions.swift
//  Choo Choo
//
//  Created by Halcyon on 01/08/2019.
//

import UIKit

extension UIStackView {
    func addBackground(color: UIColor, cornerRadius: CGFloat) {
        let subview = UIView(frame: bounds)
        subview.backgroundColor = color
        subview.layer.cornerRadius = cornerRadius
        subview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subview, at: 0)
    }

    func setupStackView(subviews: [UIView], axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, spacing: CGFloat) {
        for view in subviews {
            self.addArrangedSubview(view)
        }
        self.axis = axis
        self.distribution = distribution
        self.spacing = spacing
    }
}
