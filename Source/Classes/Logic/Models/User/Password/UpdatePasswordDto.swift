//
//  UpdatePasswordDto.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import Foundation

struct UpdatePasswordDto: Codable {
    let newPassword: String
    let oldPassword: String
}
