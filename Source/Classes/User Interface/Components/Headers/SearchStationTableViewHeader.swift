//
//  SearchStationTableViewHeader.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import UIKit

class SearchStationTableViewHeader: UIView {

    private let icon = UIImageView()
    private let nameLabel = UILabel()
    private let imageHeight: CGFloat = 70.0
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setupView() {
        icon.image = Asset.icNavigationLocationEnabled.image
        icon.image = icon.image?.withRenderingMode(.alwaysTemplate)
        icon.tintColor = .black
        icon.contentMode = .center
        
        nameLabel.text = "Use location"
        nameLabel.font = Font.barlowSemiBold(size: .medium)
        
        addSubview(icon)
        addSubview(nameLabel)
    }
    
    private func setupConstraints() {
        icon.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            icon.leadingAnchor.constraint(equalTo: leadingAnchor),
            icon.topAnchor.constraint(equalTo: topAnchor),
            icon.bottomAnchor.constraint(equalTo: bottomAnchor),
            icon.widthAnchor.constraint(equalToConstant: imageHeight),
            icon.heightAnchor.constraint(equalToConstant: imageHeight),
            
            nameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: AppStyle.Layout.padding),
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
