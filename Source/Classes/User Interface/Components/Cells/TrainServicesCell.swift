//
//  TrainServicesCell.swift
//  Choo Choo
//
//  Created by Halcyon on 12/08/2019.
//

import Foundation
import UIKit

class TrainServicesCell: UITableViewCell {
    private let icon = UIImageView()
    private let nameLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        contentView.addSubview(icon)
        contentView.addSubview(nameLabel)
        
        nameLabel.font = Font.barlowRegular(size: .small)
        
        icon.image = icon.image?.withRenderingMode(.alwaysTemplate)
        icon.tintColor = .black
        icon.contentMode = .center
    }
    
    private func setupConstraints() {
        icon.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            icon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.padding),
            icon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            icon.widthAnchor.constraint(equalTo: icon.heightAnchor),
            icon.heightAnchor.constraint(equalToConstant: LayoutValues.imageHeight),
            
            nameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: LayoutValues.padding),
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func configureCell(name: String, icon: UIImage) {
        nameLabel.text = name
        self.icon.image = icon
    }
}

extension TrainServicesCell {
    private enum LayoutValues {
        static let imageHeight: CGFloat = 20
        static let padding: CGFloat = 16
    }
}
