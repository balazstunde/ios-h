//
//  CityLabel.swift
//  Choo Choo
//
//  Created by Halcyon on 06/08/2019.
//

import Foundation
import UIKit

class CityLabel: UILabel {
    var didTapLabelClosure: (() -> Void)?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLabel()
    }
    
    private func setupLabel() {
        backgroundColor = .selected
        font = Font.barlowBold(size: .medium)
        textColor = .white
        clipsToBounds = true
        layer.cornerRadius = LayoutValues.cornerRadius
        
        isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapLabel))
        addGestureRecognizer(tap)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: LayoutValues.verticalPadding,
                                       left: LayoutValues.horizontalPadding,
                                       bottom: LayoutValues.verticalPadding,
                                       right: LayoutValues.horizontalPadding)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + LayoutValues.horizontalPadding + LayoutValues.horizontalPadding,
                      height: size.height + LayoutValues.verticalPadding + LayoutValues.verticalPadding)
    }
    
    @objc func didTapLabel() {
        didTapLabelClosure?()
    }
}

extension CityLabel {
    private enum LayoutValues {
        static let verticalPadding: CGFloat = 5
        static let horizontalPadding: CGFloat = 16
        static let cornerRadius: CGFloat = 4
    }
}
