//
//  CreateNewAccountViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 

//

import UIKit

protocol CreateNewAccountViewControllerFlowDelegate: AnyObject {
    func didTouchPrivacyAndPolicy(on viewController: CreateNewAccountViewController)
}

class CreateNewAccountViewController: BaseViewController {
    
    weak var delegate: CreateNewAccountViewControllerFlowDelegate?
    
    private let firstNameTextField = UITextField()
    private let emailTextField = UITextField()
    private let passwordTextField = UITextField()
    private let underlineFirstName = UIView()
    private let underlineEmail = UIView()
    private let underlinePassword = UIView()
    
    private let signUpButton = UIButton()
    private let receiveEmailSwitch = UISwitch()
    
    private let newsAndOffers = UILabel()
    private let privacyButton = UIButton()
    
    private let userIcon = UIImageView()
    private let emailIcon = UIImageView()
    private let passwordIcon = UIImageView()
    
    private let showPassword = UIButton()
    
    private let createAccount = UIStackView()
    private let firstNameStack = UIStackView()
    private let emailStack = UIStackView()
    private let passwordStack = UIStackView()
    private let offersStack = UIStackView()
    private let backArrowIcon = UIImageView()
    
    private var isButtonDown = false
    
    private let firstNameLabel = UILabel()
    private let emailLabel = UILabel()
    private let passwordLabel = UILabel()
    
    private let errorEmailLabel = UILabel()
    private let errorPasswordLabel = UILabel()
    private let authService = AuthenticationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpContraints()
        setupAddTargetIsCorrect()
    }
    
    private func initnavBar() {
        backArrowIcon.image = Asset.icNavigationBack.image
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backArrowIcon.image, style: .plain, target: self,
                                                           action: #selector(backButtonTapped))
        navigationItem.title = "Create account"
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: Font.barlowSemiBold(size: .mediumLarge)]
        navigationItem.leftBarButtonItem?.tintColor = .white
    }
    
    private func setUpView() {
        view.backgroundColor = .white
        initnavBar()
        initTextFields()
        initIcons()
        initButtons()
        initLabels()
        firstNameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        setUpStack()
        view.addSubview(createAccount)
    }
    
    private func initTextFields() {
        underlineFirstName.backgroundColor = .textDescriptionColor
        underlineEmail.backgroundColor = .textDescriptionColor
        underlinePassword.backgroundColor = .textDescriptionColor
        
        firstNameTextField.placeholder = "First name"
        firstNameTextField.font = Font.barlowRegular(size: FontSize.medium)
        
        emailTextField.placeholder = "Email address"
        emailTextField.font = Font.barlowRegular(size: FontSize.medium)
        emailTextField.autocapitalizationType = .none
        
        passwordTextField.placeholder = "Password(6+ characters)"
        passwordTextField.font = Font.barlowRegular(size: FontSize.medium)
        passwordTextField.isSecureTextEntry = true
        
        if #available(iOS 12, *) {
            passwordTextField.textContentType = .oneTimeCode
        } else {
            emailTextField.textContentType = .init(rawValue: "")
            passwordTextField.textContentType = .init(rawValue: "")
        }
        newsAndOffers.text = "I'd like to receive news and offers via email"
        newsAndOffers.font = Font.barlowRegular(size: FontSize.mediumSmall)
    }
    
    private func initIcons() {
        userIcon.image = Asset.icNavigationProfileDisabled.image
        emailIcon.image = Asset.icNavigationEmail.image
        passwordIcon.image = Asset.icNavigationPassword.image
        emailIcon.image = emailIcon.image?.withRenderingMode(.alwaysTemplate)
        emailIcon.tintColor = .iconColor
        passwordIcon.image = passwordIcon.image?.withRenderingMode(.alwaysTemplate)
        passwordIcon.tintColor = .iconColor
        userIcon.image = userIcon.image?.withRenderingMode(.alwaysTemplate)
        userIcon.tintColor = .iconColor
    }
    
    private func initButtons() {
        signUpButton.setTitle("Sign up", for: .normal)
        signUpButton.titleLabel!.font = Font.barlowBold(size: FontSize.medium)
        signUpButton.backgroundColor = .lola
        signUpButton.layer.cornerRadius = 5
        signUpButton.layer.shadowColor = #colorLiteral(red: 0.8, green: 0.7529411765, blue: 0.7764705882, alpha: 1)
        signUpButton.layer.shadowRadius = 5
        signUpButton.layer.shadowOpacity = 0.5
        signUpButton.isEnabled = false
        signUpButton.addTarget(self, action: #selector(didPressSignUp), for: .touchUpInside)
        
        showPassword.setImage(Asset.icNavigationShowPassword.image, for: .normal)
        showPassword.tintColor = .iconColor
        showPassword.addTarget(self, action: #selector(didPressShowPassword), for: .touchUpInside)
        
        receiveEmailSwitch.onTintColor = .textDescriptionColor
        receiveEmailSwitch.thumbTintColor = .lola
        
        receiveEmailSwitch.addTarget(self, action: #selector(didTurnedOn), for: .allTouchEvents)
        
        privacyButton.setTitleColor(.black, for: .normal)
        let yourAttributes : [NSAttributedString.Key: Any] = [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Privacy policy applies", attributes: yourAttributes)
        privacyButton.setAttributedTitle(attributeString, for: .normal)
        privacyButton.titleLabel!.font = Font.barlowRegular(size: FontSize.mediumSmall)
        privacyButton.addTarget(self, action: #selector(didTouchPrivacyPolicy), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func initLabels() {
        firstNameLabel.textColor = .white
        emailLabel.textColor = .white
        passwordLabel.textColor = .white
        errorEmailLabel.textColor = .white
        errorPasswordLabel.textColor = .white
        
        firstNameLabel.text = "First name"
        emailLabel.text = "Email"
        passwordLabel.text = "Password"
        errorPasswordLabel.text = "Check fields for any error"
        errorEmailLabel.text = "Check fields for any error"
        
        firstNameLabel.font = Font.barlowRegular(size: .extraSmall)
        emailLabel.font = Font.barlowRegular(size: .extraSmall)
        passwordLabel.font = Font.barlowRegular(size: .extraSmall)
        errorPasswordLabel.font = Font.barlowRegular(size: .extraSmall)
        errorEmailLabel.font = Font.barlowRegular(size: .extraSmall)
    }
    
    private func setUpStack() {
        firstNameStack.addArrangedSubview(userIcon)
        firstNameStack.addArrangedSubview(firstNameTextField)
        
        emailStack.addArrangedSubview(emailIcon)
        emailStack.addArrangedSubview(emailTextField)
        
        passwordStack.addArrangedSubview(passwordIcon)
        passwordStack.addArrangedSubview(passwordTextField)
        passwordStack.addArrangedSubview(showPassword)
        
        offersStack.addArrangedSubview(newsAndOffers)
        offersStack.addArrangedSubview(receiveEmailSwitch)
        
        createAccount.addArrangedSubview(firstNameLabel)
        createAccount.addArrangedSubview(firstNameStack)
        createAccount.addArrangedSubview(underlineFirstName)
        createAccount.addArrangedSubview(emailLabel)
        createAccount.addArrangedSubview(emailStack)
        createAccount.addArrangedSubview(underlineEmail)
        createAccount.addArrangedSubview(errorEmailLabel)
        createAccount.addArrangedSubview(passwordLabel)
        createAccount.addArrangedSubview(passwordStack)
        createAccount.addArrangedSubview(underlinePassword)
        createAccount.addArrangedSubview(errorPasswordLabel)
        createAccount.addArrangedSubview(offersStack)
        createAccount.addArrangedSubview(signUpButton)
        createAccount.addArrangedSubview(privacyButton)
        
        firstNameStack.axis = .horizontal
        firstNameStack.spacing = 50
        firstNameStack.distribution = .fillProportionally
        
        emailStack.axis = .horizontal
        emailStack.spacing = 40
        emailStack.distribution = .fillProportionally
        
        passwordStack.axis = .horizontal
        passwordStack.spacing = 40
        passwordStack.distribution = .fillProportionally
        
        offersStack.axis = .horizontal
        offersStack.spacing = 20
        offersStack.distribution = .fillProportionally
        offersStack.alignment = .center
        
        createAccount.axis = .vertical
        createAccount.spacing = 10
        createAccount.distribution = .fill
    }
    
    private func setUpContraints() {
        createAccount.translatesAutoresizingMaskIntoConstraints = false
        underlineFirstName.translatesAutoresizingMaskIntoConstraints = false
        underlineEmail.translatesAutoresizingMaskIntoConstraints = false
        underlinePassword.translatesAutoresizingMaskIntoConstraints = false
        userIcon.translatesAutoresizingMaskIntoConstraints = false
        emailIcon.translatesAutoresizingMaskIntoConstraints = false
        passwordIcon.translatesAutoresizingMaskIntoConstraints = false
        showPassword.translatesAutoresizingMaskIntoConstraints = false
        emailStack.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            createAccount.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            createAccount.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            createAccount.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            userIcon.widthAnchor.constraint(equalToConstant: 22),
            emailIcon.widthAnchor.constraint(equalToConstant: 28),
            passwordIcon.widthAnchor.constraint(equalToConstant: 22),
            passwordIcon.heightAnchor.constraint(equalToConstant: 28),
            showPassword.widthAnchor.constraint(equalToConstant: 30),
            
            underlineFirstName.heightAnchor.constraint(equalToConstant: 1),
            underlineEmail.heightAnchor.constraint(equalToConstant: 1),
            underlinePassword.heightAnchor.constraint(equalToConstant: 1),
            
            signUpButton.heightAnchor.constraint(equalToConstant: 48)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didTurnedOn() {
        if receiveEmailSwitch.isOn == true {
            
            receiveEmailSwitch.thumbTintColor = .secondary
            receiveEmailSwitch.onTintColor = .lola
        } else {
            receiveEmailSwitch.onTintColor = .textDescriptionColor
            receiveEmailSwitch.thumbTintColor = .lola
        }
    }
    
    @objc private func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func didPressShowPassword() {
        isButtonDown = !isButtonDown
        if !isButtonDown {
            showPassword.setImage(Asset.icNavigationShowPassword.image, for: .normal)
            showPassword.tintColor = .iconColor
            passwordTextField.isSecureTextEntry = true
            
        } else {
            showPassword.setImage(Asset.icNavigationHidePassword.image, for: .normal)
            showPassword.tintColor = .iconColor
            passwordTextField.isSecureTextEntry = false
        }
    }
    
    @objc private func didPressSignUp() {
        guard let name = firstNameTextField.text, let email = emailTextField.text,
            let password = passwordTextField.text else {
                return
        }
        let user = NewUserAccount(email: email, firstName: name, password: password, pushNotificationEnabled: receiveEmailSwitch.isOn)
        authService.createNewAccount(newUser: user, completion: {
            print("AFTER CREATE NEW ACCOUNT")
        }, failure: { _ in
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Duplicate Email", message: "Try again", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    @objc private func didTouchPrivacyPolicy() {
        delegate?.didTouchPrivacyAndPolicy(on: self)
    }
    
    func setupAddTargetIsCorrect() {
        signUpButton.isEnabled = false
        signUpButton.backgroundColor = .lola
        firstNameTextField.addTarget(self, action: #selector(textFieldsCorrect),
                                     for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textFieldsCorrect),
                                 for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldsCorrect),
                                    for: .editingChanged)
    }
    
    @objc func textFieldsCorrect(sender: UITextField) {
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        guard
            let name = firstNameTextField.text, !name.isEmpty,
            let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty,
            checkPassword(),
            checkEmail()
            else {
                self.signUpButton.isEnabled = false
                signUpButton.backgroundColor = .lola
                return
        }
        signUpButton.backgroundColor = .primary
        signUpButton.isEnabled = true
    }
    
    private func checkPassword() -> Bool {
        guard let password = passwordTextField.text else { return false}
        if password.count >= 6 && password.count <= 25 && password.matches("[A-Z]") && password.matches("[a-z]") && password.matches("[0-9]") {
            errorPasswordLabel.textColor = .white
            return true
            
        }
        errorPasswordLabel.textColor = .primary
        return false
    }
    
    private func checkEmail() -> Bool {
        guard let email = emailTextField.text else { return false}
        if 0 < email.count && email.matches("[a-z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}") {
            errorEmailLabel.textColor = .white
            return true
        }
        errorEmailLabel.textColor = .primary
        return false
    }
    private func checkTextFields() {
        if !passwordTextField.text!.isEmpty && !checkPassword() {
            errorPasswordLabel.textColor = .primary
        } else {
            errorPasswordLabel.textColor = .white
        }
        if !emailTextField.text!.isEmpty && !checkEmail() {
            errorEmailLabel.textColor = .primary
        } else {
            errorEmailLabel.textColor = .white
        }
    }
}

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}

extension CreateNewAccountViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == firstNameTextField {
            underlineFirstName.backgroundColor = .primary
            textField.placeholder = ""
            firstNameLabel.textColor = .primary
            checkTextFields()
        } else {
            if textField == emailTextField {
                underlineEmail.backgroundColor = .primary
                textField.placeholder = ""
                emailLabel.textColor = .primary
                checkTextFields()
            } else {
                if textField == passwordTextField {
                    underlinePassword.backgroundColor = .primary
                    textField.placeholder = ""
                    passwordLabel.textColor = .primary
                    checkTextFields()
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == firstNameTextField {
            underlineFirstName.backgroundColor = .textDescriptionColor
            if textField.text?.isEmpty == true {
                textField.placeholder = "First name"
                firstNameLabel.textColor = .white
            }
        } else {
            if textField == emailTextField {
                underlineEmail.backgroundColor = .textDescriptionColor
                if textField.text?.isEmpty == true {
                    textField.placeholder = "Email address"
                    emailLabel.textColor = .white
                }
            } else {
                if textField == passwordTextField {
                    underlinePassword.backgroundColor = .textDescriptionColor
                    if textField.text?.isEmpty == true {
                        textField.placeholder = "Password(6+ characters)"
                        passwordLabel.textColor = .white
                    }
                }
            }
        }
    }
}
