//
//  Font.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 18/07/16.
//  Copyright © 2016 Halcyon Mobile. All rights reserved.
//

import UIKit

// TODO: Use own font sizes and font family
enum FontSize: CGFloat {
    /// 11
    case extraSmall     = 11
    ///12
    case small          = 12
    /// 14
    case mediumSmall    = 14
    /// 16
    case medium         = 16
    /// 18
    case mediumLarge    = 18
    /// 20
    case mediumExtra    = 20
    /// 22
    case smallLarge     = 22
    /// 24
    case large          = 24
    
    case title          = 30
}

private struct FontFamily {
    
    enum Barlow: String, FontConvertible {
        case semiBold         = "Barlow-SemiBold"
        case regular          = "Barlow-Regular"
        case bold             = "Barlow-Bold"
    }

    enum BarlowFonts: String, FontConvertible {
        case barlowBold = "Barlow-Bold"
        case barlowMedium = "Barlow-Medium"
        case barlowRegular = "Barlow-Regular"
        case barlowSemibold = "Barlow-SemiBold"
        case barlowLight = "Barlow-Light"
        case barlowThin = "Barlow-Thin"
    }
}

struct Font {
    static func bold(size: FontSize) -> UIFont {
        return FontFamily.Barlow.bold.font(size: size)
    }
    
    static func regular(size: FontSize) -> UIFont {
        return FontFamily.Barlow.regular.font(size: size)
    }
    
    static func semibold(size: FontSize) -> UIFont {
        return FontFamily.Barlow.semiBold.font(size: size)
    }
    
    static func barlowBold(size: FontSize) -> UIFont {
        return FontFamily.BarlowFonts.barlowBold.font(size: size)
    }
    
    static func barlowMedium(size: FontSize) -> UIFont {
        return FontFamily.BarlowFonts.barlowMedium.font(size: size)
    }
    
    static func barlowRegular(size: FontSize) -> UIFont {
        return FontFamily.BarlowFonts.barlowRegular.font(size: size)
    }
    
    static func barlowSemiBold(size: FontSize) -> UIFont {
        return FontFamily.BarlowFonts.barlowSemibold.font(size: size)
    }
    
    static func barlowLight(size: FontSize) -> UIFont {
        return FontFamily.BarlowFonts.barlowLight.font(size: size)
    }
    
    static func barlowThin(size: FontSize) -> UIFont {
        return FontFamily.BarlowFonts.barlowThin.font(size: size)
    }
}

protocol FontConvertible {
    func font(size: FontSize) -> UIFont!
}

extension FontConvertible where Self: RawRepresentable, Self.RawValue == String {
    
    func font(size: FontSize) -> UIFont! {
        return UIFont(name: self.rawValue, size: size.rawValue)
    }
}
