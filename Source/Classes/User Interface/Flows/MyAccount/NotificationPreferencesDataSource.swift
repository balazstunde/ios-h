//
//  NotificationPreferencesDataSource.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import Foundation

class NotificationPreferencesDataSource {
    static func createSectionData() -> [NotificationPreferencesSection] {
        var section: [NotificationPreferencesSection] = []
        
        let generalSectionRows = ["Receive push notifications"]
        let generalSection = NotificationPreferencesSection.general(generalSectionRows)
        section.append(generalSection)
        
        let notifySectionRows = ["There is a  trip disruption", "There is a  trip delay", "Train has arrived in station", "Platform has changed"]
        let notifySection = NotificationPreferencesSection.notify(notifySectionRows)
        section.append(notifySection)
        
        let discountsSectionRow = ["Get discounts and promotional info"]
        let discountSection = NotificationPreferencesSection.discounts(discountsSectionRow)
        section.append(discountSection)
        
        return section
    }
}
