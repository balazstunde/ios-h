//
//  KeyStore.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 08/01/2019.
//

import Foundation

private enum Key {
    static let accessToken          = "accessToken"
    static let recentSearches       = "recentSearches"
    static let userID               = "user_id"
    static let currentUser          = "currentUser"
    static let currentCreditCard    = "currentCreditCard"
    static let fareCategories       = "fareCategories"
    static let fareCategoriesMap    = "fareCategoriesMap"
    static let fareCategoriesPriceMap = "fareCategoriesPriceMap"
}

class KeyStore {
    
    private let userDefaults = UserDefaults.standard
    private let keychainStore = KeychainStore(serviceName: "com.application.identifier")
    
    // MARK: - Lifecycle
    
    static let shared = KeyStore()
    
    private init() {}
    
    // MARK: - Methods
    
    var accessToken: String? {
        get { return keychainStore.get(forKey: Key.accessToken) }
        set { keychainStore.set(newValue, forKey: Key.accessToken) }
    }
    
    var userID: Int? {
        get { return userDefaults.integer(forKey: Key.userID) }
        set { userDefaults.set(newValue, forKey: Key.userID) }
    }
    var currentUser: UserInformations? {
        get { return userDefaults.object(forKey: Key.currentUser) }
        set { userDefaults.setObject(newValue, forKey: Key.currentUser) }
    }
    
    var fareCategories: [FareCategory]? {
        get { return userDefaults.object(forKey: Key.fareCategories)}
        set { userDefaults.setObject(newValue, forKey: Key.fareCategories)}
    }
    
    var fareCategoriesMap: [String:Int]? {
        get { return userDefaults.object(forKey: Key.fareCategoriesMap)}
        set { userDefaults.setObject(newValue, forKey: Key.fareCategoriesMap) }
    }
    
    var fareCategoriesPriceMap: [String: Double]? {
        get { return userDefaults.object(forKey: Key.fareCategoriesPriceMap)}
        set { userDefaults.setObject(newValue, forKey: Key.fareCategoriesPriceMap) }
    }
    
    var recentSearches: [Int] {
        get {
            guard let data = userDefaults.value(forKey: Key.recentSearches) as? Data, let stops = try? JSONDecoder().decode([Int].self, from: data) else { return [] }
            return stops
        }
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            userDefaults.set(data, forKey: Key.recentSearches)
        }
    }
    
    func removeUserData() {
        let dictionary = userDefaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            userDefaults.removeObject(forKey: key)
        }
        keychainStore.set(nil, forKey: Key.accessToken)
    }
    
    var currentCreditCard: CreditCard? {
        get {
            guard let data = userDefaults.value(forKey: Key.currentCreditCard) as? Data, let card = try? JSONDecoder().decode(CreditCard.self, from: data) else { return nil }
            
            return card
        }
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            userDefaults.set(data, forKey: Key.currentCreditCard)
        }
    }
}

private extension UserDefaults {
    func setObject<T: Encodable>(_ value: T?, forKey key: String) {
        guard let value = value else {
            set(nil, forKey: key)
            return
        }
        let jsonEncoder = JSONEncoder()
        set(try? jsonEncoder.encode(value), forKey: key)
    }
    
    func object<T: Decodable>(forKey key: String) -> T? {
        guard let data = data(forKey: key) else { return nil }
        let jsonDecoder = JSONDecoder()
        return try? jsonDecoder.decode(T.self, from: data)
    }
}
