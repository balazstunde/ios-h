//
//  FacilityType.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 08/08/2019.
//

import Foundation

enum FacilityType {
    case bus
    case tram
    case taxi
    case bike
    
    var image: AssetImageTypeAlias {
        switch self {
        case .bus: return Asset.icTicketDetailsBus.image
        case .tram: return Asset.icTicketDetailsTram.image
        case .taxi: return Asset.icTicketDetailsTaxi.image
        case .bike: return Asset.icTicketDetailsBike.image
        }
    }
}
