//
//  SearchResultCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

class SearchLocationCell: UITableViewCell {
    static let cellIdentfier = "SearchResultCell"
    private let nameLabel = UILabel()
    private let icon = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        contentView.addSubview(icon)
        contentView.addSubview(nameLabel)
        
        icon.image = UIImage(named: "ic_navigation_compass-enabled")
        icon.image = icon.image?.withRenderingMode(.alwaysTemplate)
        icon.tintColor = .lightGray
        icon.contentMode = .center
        
        nameLabel.font = Font.barlowRegular(size: .mediumSmall)
    }
    
    private func setupConstraints() {
        icon.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            icon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            icon.topAnchor.constraint(equalTo: contentView.topAnchor),
            icon.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            icon.widthAnchor.constraint(equalTo: icon.heightAnchor),
            
            nameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: AppStyle.Layout.padding),
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func updateUI(with name: String, subtextSize: Int) {
        nameLabel.text = name
        highlightSubtext(first: subtextSize)
    }
    
    public func highlightSubtext(first numberOfLetters: Int) {
        guard numberOfLetters > 0, let text = nameLabel.text else {
            nameLabel.textColor = .black
            return
        }
        
        let normalText = String(text.prefix(numberOfLetters))
        let suffix = text.count - numberOfLetters
        let grayText = String(text.suffix(suffix))
        
        let normalTextAttributedString = NSMutableAttributedString(string: normalText)
        let grayTextAttributedString = NSMutableAttributedString(string: grayText, attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.highlightedText
            ])
        
        normalTextAttributedString.append(grayTextAttributedString)
        nameLabel.attributedText = normalTextAttributedString
    }
}
