//
//  JourneyCost.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import Foundation

struct PricingInformation: Codable {
    let secondClassPrice: Price
    let firstClassPrice: Price
    let coachPrice: Price
}
