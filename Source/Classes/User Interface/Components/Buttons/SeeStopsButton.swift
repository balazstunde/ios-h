//
//  SeeStopsButton.swift
//  Choo Choo
//
//  Created by Halcyon on 07/08/2019.
//

import Foundation
import UIKit

final class SeeStopsButton: UIButton {
    var didTapButtonClosure: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        titleLabel?.font = Font.barlowRegular(size: .mediumSmall)
        backgroundColor = .softPeach
        setTitle("See stops  >", for: .normal)
        setTitleColor(.black, for: .normal)
        layer.cornerRadius = LayoutValues.cornerRadius
        contentEdgeInsets.left = LayoutValues.horizontalPadding
        contentEdgeInsets.right = LayoutValues.horizontalPadding
        contentEdgeInsets.bottom = LayoutValues.verticalPadding
        contentEdgeInsets.top = LayoutValues.verticalPadding
        sizeToFit()
        addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    @objc private func didTapButton() {
        didTapButtonClosure?()
    }
}

extension SeeStopsButton {
    private enum LayoutValues {
        static let cornerRadius: CGFloat = 15.5
        static let horizontalPadding: CGFloat = 10
        static let verticalPadding: CGFloat = 5
    }
}
