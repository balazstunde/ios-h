//
//  UIView+ConcaveEnds.swift
//  Choo Choo
//
//  Created by Halcyon on 13/08/2019.
//

import UIKit

extension UIView {
    func concaveEnds() {
        let width = self.frame.size.width
        let height = self.frame.size.height
        
        let path = UIBezierPath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: width, y: 0))
        path.addArc(withCenter: CGPoint(x: width, y: height/2), radius: height/2, startAngle: -.pi/2, endAngle: .pi/2, clockwise: false)
        path.addLine(to: CGPoint(x: 0, y: height))
        path.addArc(withCenter: CGPoint(x: 0, y: height/2), radius: height/2, startAngle: .pi/2, endAngle:-.pi/2 , clockwise: false)
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = false
    }
}
