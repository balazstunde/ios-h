//
//  DottedLineView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import UIKit

class DottedLineView: UIView {

    private let leftDotsLabel = UILabel()
    private let rightDotsLabel = UILabel()
    private let trainImageView = UIImageView()
    private let trainNoLabel = UILabel()
    private let horizontalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .fillProportionally
        return stack
    }()

    private let verticalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        return stack
    }()

    var setTrainNo: String? {
        set { trainNoLabel.text = newValue }
        get { return trainNoLabel.text }
    }

    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        trainImageView.tintColor = .lola
        trainImageView.image = trainImageView.image?.withRenderingMode(.alwaysTemplate)

        rightDotsLabel.text = "- - - - - -"
        rightDotsLabel.font = Font.barlowLight(size: .mediumSmall)
        rightDotsLabel.adjustsFontSizeToFitWidth = true

        leftDotsLabel.text = "- - - - - -"
        leftDotsLabel.font = Font.barlowLight(size: .mediumSmall)
        leftDotsLabel.adjustsFontSizeToFitWidth = true

        rightDotsLabel.textColor = .lola
        leftDotsLabel.textColor = .lola

        trainNoLabel.font = Font.barlowRegular(size: .mediumSmall)

        horizontalStackView.addArrangedSubview(leftDotsLabel)
        horizontalStackView.addArrangedSubview(trainImageView)
        horizontalStackView.addArrangedSubview(rightDotsLabel)

        verticalStackView.addArrangedSubview(horizontalStackView)
        verticalStackView.addArrangedSubview(trainNoLabel)

        trainImageView.image = Asset.icNavigationTrain.image
        trainNoLabel.font = Font.barlowBold(size: .mediumSmall)
        trainImageView.tintColor = .alto
        trainImageView.image = trainImageView.image?.withRenderingMode(.alwaysTemplate)

        addSubview(verticalStackView)
    }

    private func setupConstraints() {
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            horizontalStackView.topAnchor.constraint(equalTo: topAnchor),
            horizontalStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            horizontalStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            horizontalStackView.trailingAnchor.constraint(equalTo: trailingAnchor)
            ])
    }
}
