//
//  LocalNotificationService.swift
//  Choo Choo
//
//  Created by halcyonmobile on 05/08/2019.
//

import Foundation
import UserNotifications

struct NotificationData: Equatable {
    let startDate: String
    let startTime: String
    let endTime: String
    let departure: String
    let destination: String
}

class LocalNotificationService {
    private var oneDayEarlyComponent = [DateComponents]()
    private var twoHoursEarlyComponent = [DateComponents]()
    
    private func getDateComponent(date: Date) -> DateComponents {
        var dateComponents = DateComponents()
        dateComponents.year = 2019
        dateComponents.month = Calendar.current.component(.month, from: date)
        dateComponents.day = Calendar.current.component(.day, from: date)
        dateComponents.hour = Calendar.current.component(.hour, from: date)
        dateComponents.minute = Calendar.current.component(.minute, from: date)
        return dateComponents
    }
    
    func setUpOneDayNotification(notificationDetails: NotificationData) {
        guard let dateFromString = DateFormatter.getStringAsDate(inFormat: DateFormats.ticketDayAndHourFormat, date: notificationDetails.startDate+" "+notificationDetails.startTime) else {
            return
        }
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "YOU HAVE A TRIP IN ONE DAY!"
        notificationContent.subtitle = "Don't forget to check your tickets!"
        notificationContent.sound = UNNotificationSound.default
        notificationContent.badge = 1
        
        guard let oneDayEarlier = Calendar.current.date(byAdding: .day, value: -1, to: dateFromString) else {
            return
        }
        oneDayEarlyComponent.append(getDateComponent(date: oneDayEarlier))
        makeNotificationRequests(identifier: "oneDay \(notificationDetails.startDate + notificationDetails.startTime)", dateComponents: oneDayEarlyComponent, notificationContent: notificationContent)
    }
    
    func setUpTwoHoursNotification(notificationDetails: NotificationData) {
        guard let dateFromString = DateFormatter.getStringAsDate(inFormat: DateFormats.ticketDayAndHourFormat, date: notificationDetails.startDate+" "+notificationDetails.startTime) else {
            return
        }
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "YOU HAVE A TRIP IN TWO HOURS!"
        notificationContent.subtitle = "Check your tickets!"
        notificationContent.sound = UNNotificationSound.default
        notificationContent.badge = 1
        
        guard let twoHoursEarlier = Calendar.current.date(byAdding: .hour, value: -2, to: dateFromString) else {
            return
        }
        twoHoursEarlyComponent.append(getDateComponent(date: twoHoursEarlier))
        makeNotificationRequests(identifier: "twoHours \(notificationDetails.startDate + notificationDetails.startTime)", dateComponents: twoHoursEarlyComponent, notificationContent: notificationContent)
    }
    
    private func makeNotificationRequests(identifier: String, dateComponents: [DateComponents], notificationContent: UNMutableNotificationContent) {
        for date in dateComponents {
            let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: false)
            let notificationRequest = UNNotificationRequest(identifier: "\(identifier)", content: notificationContent, trigger: notificationTrigger)
            UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: nil)
        }
    }
}
