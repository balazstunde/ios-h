//
//  PasswordTokenValidationDto.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation

class PasswordTokenValidationDto: Codable {
    let expired: Bool
    let token: String
    
    enum CodingKeys: String, CodingKey {
        case expired = "expired"
        case token = "token"
    }
}
