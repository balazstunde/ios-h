//
//  FindingFareHeaderView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import UIKit

class FindingFareHeaderView: UIView {
    
    private let outboundsTitleLabel = UILabel()
    private let clientOutboundsLabel = UILabel()
    private let sortedTitleLabel = UILabel()
    private let sortMethodLabel = UILabel()
    private let sortMethodsButton = UIButton()
    private let outbounds: String?
    
    var didTouchSortMethodsButton: (() -> Void)?
    
    var sortMethod: String? {
        set { sortMethodLabel.text = newValue }
        get { return sortMethodLabel.text }
    }
    
    init(withOutbounds outbounds: String) {
        //TODO create outbounds DTO
        self.outbounds = outbounds
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        sortMethodsButton.setImage(Asset.icDots.image, for: .normal)
        sortMethodsButton.tintColor = .shiraz
        sortMethodsButton.addTarget(self, action: #selector(didTouchSortMethod), for: .touchUpInside)

        outboundsTitleLabel.text = "Outbounds"
        outboundsTitleLabel.font = Font.barlowBold(size: FontSize.mediumLarge)
        clientOutboundsLabel.font = Font.barlowRegular(size: FontSize.medium)
        clientOutboundsLabel.text = outbounds
        sortedTitleLabel.text = "Sorted by"
        sortedTitleLabel.font = Font.barlowBold(size: FontSize.mediumLarge)
        sortMethodLabel.text = "Departure time"
        sortMethodLabel.font = Font.barlowRegular(size: FontSize.medium)
        
        addSubview(outboundsTitleLabel)
        addSubview(clientOutboundsLabel)
        addSubview(sortedTitleLabel)
        addSubview(sortMethodLabel)
        addSubview(sortMethodsButton)
    }
    
    private func setupConstraints() {
        outboundsTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        clientOutboundsLabel.translatesAutoresizingMaskIntoConstraints = false
        sortMethodsButton.translatesAutoresizingMaskIntoConstraints = false
        sortedTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        sortMethodLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            outboundsTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: PaddingValues.veryBigPadding),
            outboundsTitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: PaddingValues.mediumPadding),
            
            clientOutboundsLabel.topAnchor.constraint(equalTo: outboundsTitleLabel.bottomAnchor),
            clientOutboundsLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: PaddingValues.mediumPadding),
            clientOutboundsLabel.trailingAnchor.constraint(lessThanOrEqualTo: centerXAnchor, constant: -PaddingValues.smallPadding),
            clientOutboundsLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -PaddingValues.veryBigPadding),
            
            sortMethodsButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -PaddingValues.mediumPadding),
            sortMethodsButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            sortMethodsButton.heightAnchor.constraint(equalTo: sortMethodsButton.widthAnchor),
            sortMethodsButton.widthAnchor.constraint(equalToConstant: LayoutValues.buttonWidth),
            
            sortedTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: PaddingValues.veryBigPadding),
            sortedTitleLabel.trailingAnchor.constraint(equalTo: sortMethodsButton.leadingAnchor, constant: -PaddingValues.mediumPadding),
            
            sortMethodLabel.topAnchor.constraint(equalTo: sortedTitleLabel.bottomAnchor),
            sortMethodLabel.leadingAnchor.constraint(greaterThanOrEqualTo: centerXAnchor, constant: PaddingValues.smallPadding),
            sortMethodLabel.trailingAnchor.constraint(equalTo: sortMethodsButton.leadingAnchor, constant: -PaddingValues.smallPadding)
            ])
    }
    
    @objc private func didTouchSortMethod() {
        //TODO: call delegate in order to Sort the search list
        didTouchSortMethodsButton?()
    }
}
extension FindingFareHeaderView {
    private enum PaddingValues {
        static let smallPadding: CGFloat = 8.0
        static let mediumPadding: CGFloat = 16.0
        static let bigPadding: CGFloat = 24.0
        static let veryBigPadding: CGFloat = 32.0
        static let hugePadding: CGFloat = 40.0
    }
    private enum LayoutValues {
        static let buttonWidth: CGFloat = 24.0
    }
}
