//
//  MyAccountDataSource.swift
//  Choo Choo
//
//  Created by halcyonmobile on 29/07/2019.
//

import Foundation

class MyAccountDataSource {
    
    static func createSectionData() -> [MyAccountSection] {
        var section: [MyAccountSection] = []
        let generalSectionRows = [
            MyAccountSection.MyAccountRowData(title: "Profile Settings", description: "Name, Email, Password",
                                              leftImage: Asset.iconSettings),
            MyAccountSection.MyAccountRowData(title: "Payment Method", description: "Change your payment method",
                                              leftImage: Asset.iconPayment),
            MyAccountSection.MyAccountRowData(title: "Notifications", description: "How and when you receive them",
                                              leftImage: Asset.iconNofication)
        ]
        
        let generalSection = MyAccountSection.general(generalSectionRows)
        section.append(generalSection)
        
        let supportSectionRows = [
            MyAccountSection.MyAccountRowData(title: "Privacy Policy", description: "Know more about the company",
                                              leftImage: Asset.iconPrivacy),
            MyAccountSection.MyAccountRowData(title: "Terms and conditions",
                                              description: "Know more about rules when riding trains",leftImage: Asset.iconTerms),
            MyAccountSection.MyAccountRowData(title: "Customer Support",
                                              description: "Get assistance or file a complaint",leftImage: Asset.iconSupport)
        ]
        
        let supportSection = MyAccountSection.support(supportSectionRows)
        section.append(supportSection)
        
        let logOutSectionRow = [
            MyAccountSection.MyAccountRowData(title: "Log Out", description: "", leftImage: Asset.iconLogout)
        ]
        let logOutSection = MyAccountSection.logout(logOutSectionRow)
        section.append(logOutSection)
        
        return section
    }
}
