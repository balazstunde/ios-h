//
//  Facility.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 08/08/2019.
//

import Foundation

struct Facility {
    let type: FacilityType
    var connections: [String]
    let walkingTime: Int
}
