//
//  TripConfigurationViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import UIKit
import DLRadioButton

protocol TripConfigurationViewControllerFlowDelegate: AnyObject {
    func close()
    func closeWithConfirm(selected: TravelPreferences)
}

class TripConfigurationViewController: UIViewController {
    private var container: RoundedView!
    private var tripConfigurationLabel: UILabel!
    private var chooseLabel: UILabel!
    private var textStack: UIStackView!
    private var confirmButton: UIButton!
    private var tableView: UITableView!
    private var travelPreferencesCellIdentifier = "travelPreferences"
    private var travelPreferences: [TravelPreferences] = []
    weak var delegate: TripConfigurationViewControllerFlowDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        initConstraints()
        loadData()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func initUI() {
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        tapRecognizer.delegate = self
        view.addGestureRecognizer(tapRecognizer)
        
        container = RoundedView()
        container.backgroundColor = .white
        
        tripConfigurationLabel = UILabel()
        tripConfigurationLabel.text = "Trip configuration"
        tripConfigurationLabel.textColor = .black
        tripConfigurationLabel.font = Font.barlowSemiBold(size: .mediumExtra)
        
        chooseLabel = UILabel()
        chooseLabel.text = "Choose your desired travel preferences "
        chooseLabel.textColor = .black
        chooseLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        textStack = UIStackView(arrangedSubviews: [tripConfigurationLabel, chooseLabel])
        textStack.axis = .vertical
        textStack.distribution = .fillEqually
        
        confirmButton = UIButton()
        confirmButton.setTitle("Done", for: .normal)
        confirmButton.titleLabel?.font = Font.barlowBold(size: .mediumSmall)
        confirmButton.backgroundColor = .shiraz
        confirmButton.setTitleColor(.white, for: .normal)
        confirmButton.layer.cornerRadius = 3
        confirmButton.addTarget(self, action: #selector(closeWithConfirm), for: .touchUpInside)
        
        tableView = UITableView()
        tableView.register(TravelPreferencesCell.self, forCellReuseIdentifier: travelPreferencesCellIdentifier)
        tableView.rowHeight = 45
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(container)
        container.addSubview(textStack)
        container.addSubview(tableView)
        container.addSubview(confirmButton)
    }
    
    private func initConstraints() {
        container.translatesAutoresizingMaskIntoConstraints = false
        textStack.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            container.topAnchor.constraint(equalTo: view.centerYAnchor),
            container.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            container.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            container.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            textStack.topAnchor.constraint(equalTo: container.topAnchor, constant: 32),
            textStack.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 32),
            
            tableView.topAnchor.constraint(equalTo: textStack.bottomAnchor, constant: 5),
            tableView.leadingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.leadingAnchor, constant: 32),
            tableView.trailingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.trailingAnchor, constant: -32),
            tableView.bottomAnchor.constraint(equalTo: confirmButton.topAnchor, constant: -5),
            
            confirmButton.leadingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            confirmButton.trailingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            confirmButton.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -16),
            confirmButton.heightAnchor.constraint(equalToConstant: 45)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    private func loadData() {
        travelPreferences = TravelPreferencesDataSource.getData()
    }
    
    @objc private func close() {
        delegate?.close()
    }
    
    @objc private func closeWithConfirm() {
        delegate?.closeWithConfirm(selected: getSelectedOneWithPrice())
    }
    
    private func getSelectedOneWithPrice() -> TravelPreferences {
        for indexPath in tableView.indexPathsForVisibleRows ?? [] {
            guard let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: indexPath.section)) as? TravelPreferencesCell else {
                return travelPreferences[0]
            }
            // if the selected one isn't the first I have to calculate the price
            if cell.radioButton.isSelected && indexPath.row != 0 {
                return TravelPreferences(name: travelPreferences[indexPath.row].name, price: travelPreferences[indexPath.row].price + travelPreferences[0].price)
            } else {
                if cell.radioButton.isSelected {
                    return travelPreferences[indexPath.row]
                }
            }
        }
        return travelPreferences[0]
    }
}

extension TripConfigurationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travelPreferences.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: travelPreferencesCellIdentifier, for: indexPath) as? TravelPreferencesCell else {
            assertionFailure("Could not dequeue")
            return UITableViewCell()
        }
        
        cell.delegate = self
        cell.setContent(travelPreference: travelPreferences[indexPath.row])
        return cell
    }
}

extension TripConfigurationViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension TripConfigurationViewController: TravelPreferencesCellDelegate {
    func radioButtonTapped(_ radioButton: DLRadioButton, on cell: TravelPreferencesCell) {
        if radioButton.isSelected {
            // if one is selected you have to deselect the others
            deselectOtherButton(cell)
            refreshTable(cell)
        } else {
            // you can just change to another, but can't deselect
            radioButton.isSelected = true
        }
    }
    
    func deselectOtherButton(_ cell: TravelPreferencesCell) {
        let tappedCellIndexPath = tableView.indexPath(for: cell) ?? IndexPath(row: 0, section: 0)
        let indexPaths = tableView.indexPathsForVisibleRows ?? []
        for indexPath in indexPaths {
            guard let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: indexPath.section)) as? TravelPreferencesCell else {
                return
            }
            if indexPath.row != tappedCellIndexPath.row && indexPath.section == tappedCellIndexPath.section {
                if cell.radioButton.isSelected {
                    cell.radioButton.isSelected = false
                    cell.priceLabel.textColor = .black
                }
            } else {
                cell.priceLabel.textColor = .shiraz
            }
        }
    }
    
    private func refreshTable(_ cell: TravelPreferencesCell) {
        
        let row = tableView.indexPath(for: cell)?.row
        if row != 0 {
            cell.priceLabel.text = "\(travelPreferences[0].price + travelPreferences[row ?? 0].price) Lei"
            guard let firstCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TravelPreferencesCell else {
                return
            }
            firstCell.priceLabel.text = "\(travelPreferences[0].price) Lei"
        }
    }
}

extension TripConfigurationViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return gestureRecognizer.view === touch.view
    }
}
