//
//  BackgroundViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import UIKit

class BackgroundViewController: BaseViewController {
    private let passengerDetailService = BookingService()
    private let myButton = UIButton(frame: CGRect(x: 30, y: 30, width: 100, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        myButton.backgroundColor = .black
        myButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        view.addSubview(myButton)
        
    }
    @objc private func buttonTapped() {
        let passengerDetailsVC = PassengerDetailsViewController(passengersDetailsService: passengerDetailService)
        passengerDetailsVC.modalPresentationStyle = .custom
        present(passengerDetailsVC, animated: true, completion: nil)
    }
    
}
