//
//  BackendManager.swift
//  packmen
//
//  Created by Alexandra Muresan on 20/07/2018.
//  Copyright © 2018 Halcyon Mobile. All rights reserved.
//

import Foundation

public struct ErrorMessages {
    public static let unknownError = "Oops! Something went wrong while processing your request."
    public static let invalidUrl = "URL is not valid:"
    public static let invalidParameters = "Invalid parsmeters!"
    public static let missingURL = "URL request to encode was missing a URL"
    public static let invalidData = "the data returned is not a valid JSON"
}

class BackendManager {

    // MARK: - Private properties

    private static let baseURL = "http://192.168.1.50:7081/cfr/api/v1"
    private static var headers: HTTPHeaders = ["Content-Type": "application/json", "Accept": "application/json", "X-CFR-API-TIME-ZONE" : "Europe/Bucharest"]
    private static let defaultSession = URLSession(configuration: .default)

    // MARK: - Lifecycle

    static func execute<T: Decodable>(request: RequestRepresentable, success: @escaping ([T]) -> Void, failure: @escaping (Error) -> Void) {
        performRequest(request: request, headers: headers) { response in
            switch response {
            case .success(let data):
                do {
                    let returnValue = try JSONDecoder().decode([T].self, from: data)
                    success(returnValue)
                } catch {
                    failure(InterfaceError(error: error))
                }
            case .failure(let error):
                failure(InterfaceError(error: error))
            }
        }
    }

    static func execute<T: Decodable>(request: RequestRepresentable, success: @escaping (T) -> Void, failure: @escaping (Error) -> Void) {
        performRequest(request: request, headers: headers) { response in
            switch response {
            case .success(let data):
                do {
                    print(T.self)
                    let returnValue = try JSONDecoder().decode(T.self, from: data)
                    success(returnValue)
                } catch {
                    failure(InterfaceError(error: error))
                }
            case .failure(let error):
                failure(InterfaceError(error: error))
            }
        }
    }

    static func execute(request: RequestRepresentable, success: @escaping (JSONDict) -> Void, failure: @escaping (Error) -> Void) {
        performRequest(request: request, headers: headers) { response in
            switch response {
            case .success(let data):
                    do {
                        guard !data.isEmpty else { return success(JSONDict()) }

                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let json = json as? JSONDict {
                            return success(json)
                        } else {
                            return failure(InterfaceError(message: ErrorMessages.invalidData))
                        }
                    } catch {
                        return failure(InterfaceError(error: error))
                    }
            case .failure(let error):
                failure(error)
            }
            
        }
    }

    // MARK: - Perform request helpers

    private static func performRequest(request: RequestRepresentable, headers: HTTPHeaders, completion: @escaping (Result<Data>) -> Void) {
        let urlString  = baseURL + request.suffix
        
        guard let url = URL(string: urlString) else {  
            completion( .failure(InterfaceError(message: ErrorMessages.invalidUrl + urlString)))
            return
        }   
        
        var httpHeaders = headers
        
        if !isPublicAPI(url: request.suffix) {
            if let token = KeyStore.shared.accessToken {
                httpHeaders[API.HeaderKeys.authorization] = "\(API.HeaderKeys.tokenType)\(token)"
            }
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        
        if request.paramType == .formData {
            httpHeaders["Content-Type"] = "application/x-www-form-urlencoded"
        }
        for (headerField, headerValue) in httpHeaders {
            urlRequest.setValue(headerValue, forHTTPHeaderField: headerField)
        }
        switch request.method {
        case .get:
            performDataRequest(request: urlRequest, parameters: request.parameters, completion: completion)
        case .post, .put, .delete:
            if request.paramType == .body {
                performUploadRequest(request: urlRequest, parameters: request.parameters, completion: completion)
            } else {
                performFormDataRequest(request: urlRequest, parameters: request.parameters, completion: completion)
            }
        }
    }

    static func performDataRequest(request: URLRequest, parameters: Parameters?, completion: @escaping (Result<Data>) -> Void) {
        guard let url = request.url else {
             completion( .failure(InterfaceError(message: ErrorMessages.missingURL)))
            return
        }
        var urlRequest = request
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), let parameters = parameters, !parameters.isEmpty {
            let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + parameters.map { "\($0)=\($1)" }.joined(separator: "&").replacingOccurrences(of: " ", with: "+")
            urlComponents.percentEncodedQuery = percentEncodedQuery
            urlRequest.url = urlComponents.url
        }
        
        let task = defaultSession.dataTask(with: urlRequest) { data, response, error in
            if let httpResponse = response as? HTTPURLResponse, let networkError = InterfaceError(httpStatusCode: httpResponse.statusCode) {
                completion( .failure(networkError))
                return
            }
            
            if let data = data {
                completion(.success(data))
            } else {
                completion(.failure(InterfaceError(error: error)))
            }
        }
        // execute the HTTP request
        task.resume()
    }
    
    static func performUploadRequest(request: URLRequest, parameters: Parameters?, completion: @escaping (Result<Data>) -> Void) {
        var postData: Data?
        if let parameters = parameters {
            postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        let task = defaultSession.uploadTask(with: request, from: postData) { data, response, error in
            if let httpResponse = response as? HTTPURLResponse, let networkError = InterfaceError(httpStatusCode: httpResponse.statusCode) {
                completion( .failure(networkError))
                return
            }
            
            if let data = data {
                completion(.success(data))
            } else {
                completion(.failure(InterfaceError(error: error)))
            }
        }
        // execute the HTTP request
        task.resume()
    }
    
    static func performFormDataRequest(request: URLRequest, parameters: Parameters?, completion: @escaping (Result<Data>) -> Void) {
        var urlRequest = request
        
        if let parameters = parameters as? [String : String], !parameters.isEmpty {
            urlRequest.encodeParameters(parameters: parameters)
        }
        
        let task = defaultSession.dataTask(with: urlRequest) { data, response, error in
            if let httpResponse = response as? HTTPURLResponse, let networkError = InterfaceError(httpStatusCode: httpResponse.statusCode) {
                completion( .failure(networkError))
                return
            }
            
            if let data = data {
                completion(.success(data))
            } else {
                completion(.failure(InterfaceError(error: error)))
            }
        }
        // execute the HTTP request
        task.resume()
    }
    
    private static func isPublicAPI(url: String) -> Bool {
        if  url.contains(URLs.locationSuffix) ||
            url.contains(URLs.stopSuffix) ||
            url.contains(URLs.recentsSuffix) ||
            url.contains(API.Path.journeySearch) ||
            url.contains(API.Path.journeyPricingCategories) ||
            url.contains(API.Path.passwordRecovery) ||
            url.elementsEqual(API.Path.signup) ||
            url.contains(API.Path.login) {
            return true
        }
        return false
    }
}

public enum Result<Value> {
    case success(Value)
    case failure(InterfaceError)
    
    /// Returns the associated value if the result is a success, `nil` otherwise.
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
}

/**
 Error object to use on the presentation layer.
 
 The message may be displayed to the user, so it should be friendly.
 */
open class InterfaceError: Error {
    
    public let message: String
    
    public init(message: String) {
        self.message = message
    }
    
    public init(error: Error?) {
        if let message = error?.localizedDescription { 
            self.message = message
        } else {
            self.message = ErrorMessages.unknownError
        }
    }
    
    public init?(httpStatusCode: Int) {
        
        switch httpStatusCode {
        case 200 ..< 300:
            return nil
        case 400:
            self.message = "Bad Request"
        case 401:
            self.message = "Unauthorized"
        case 403:
            self.message = "Forbidden"
        case 500:
            self.message = "Internal Server Error"
        case 503:
            self.message = "Service unavailable"
        default:
            return nil
        }
    }
}

extension URLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func encodeParameters(parameters: [String : String]) {
        let parameterArray = parameters.map { (arg) -> String in
            let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
        
        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
    }
}
