//
//  FindingFareTableCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import UIKit

class FareDetailsView: UIView {
    
    private let departureStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        return stack
    }()
    private let destinationStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .trailing
        return stack
    }()
    private lazy var horizontalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .fillEqually
        return stack
    }()
    
    private let departureTimeLabel = UILabel()
    private let departureCityLabel = UILabel()
    private let destinationTimeLabel = UILabel()
    private let destinationCityLabel = UILabel()
    private let trainImageView = DottedLineView()
    private let trainConnectionLabel = UILabel()
    private let tripTimeLabel = UILabel()
    private let priceLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
        setupComponentStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(horizontalStackView)
        addSubview(trainConnectionLabel)
        addSubview(tripTimeLabel)
        addSubview(priceLabel)
        
        departureStackView.addArrangedSubview(departureTimeLabel)
        departureStackView.addArrangedSubview(departureCityLabel)
        
        destinationStackView.addArrangedSubview(destinationTimeLabel)
        destinationStackView.addArrangedSubview(destinationCityLabel)
        
        horizontalStackView.addArrangedSubview(departureStackView)
        horizontalStackView.addArrangedSubview(trainImageView)
        horizontalStackView.addArrangedSubview(destinationStackView)
    }
    
    private func setupConstraints() {
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        trainConnectionLabel.translatesAutoresizingMaskIntoConstraints = false
        tripTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            horizontalStackView.topAnchor.constraint(greaterThanOrEqualTo: safeAreaLayoutGuide.topAnchor, constant: PaddingValues.bigPadding),
            horizontalStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: PaddingValues.mediumPadding),
            horizontalStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -PaddingValues.mediumPadding),
            
            trainConnectionLabel.topAnchor.constraint(equalTo: horizontalStackView.bottomAnchor, constant: PaddingValues.smallPadding),
            trainConnectionLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: PaddingValues.mediumPadding),
            
            tripTimeLabel.topAnchor.constraint(equalTo: trainConnectionLabel.bottomAnchor),
            tripTimeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -PaddingValues.bigPadding),
            tripTimeLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: PaddingValues.mediumPadding),
            
            priceLabel.topAnchor.constraint(equalTo: horizontalStackView.bottomAnchor, constant: PaddingValues.mediumPadding),
            priceLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -PaddingValues.mediumPadding),
            priceLabel.leadingAnchor.constraint(greaterThanOrEqualTo: centerXAnchor, constant: PaddingValues.hugePadding),
            priceLabel.widthAnchor.constraint(lessThanOrEqualToConstant: StyleValues.buttonWidth),
            priceLabel.heightAnchor.constraint(equalToConstant: StyleValues.buttonHeight),
            priceLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -PaddingValues.bigPadding)
            ])
    }
    
    private func setupComponentStyle() {
        departureTimeLabel.font = Font.barlowBold(size: .smallLarge)
        departureCityLabel.font = Font.barlowRegular(size: .mediumLarge)
        departureCityLabel.lineBreakMode = .byWordWrapping
        departureCityLabel.numberOfLines = 0
        
        destinationTimeLabel.font = Font.barlowBold(size: .smallLarge)
        destinationCityLabel.font = Font.barlowRegular(size: .mediumLarge)
        destinationCityLabel.lineBreakMode = .byWordWrapping
        destinationCityLabel.numberOfLines = 0
        
        trainConnectionLabel.font = Font.barlowBold(size: .medium)
        tripTimeLabel.font = Font.barlowRegular(size: .medium)
        tripTimeLabel.textColor = .pinkSwan
        
        priceLabel.layer.masksToBounds = true
        priceLabel.font = Font.barlowBold(size: .medium)
        priceLabel.backgroundColor = .shiraz
        priceLabel.layer.cornerRadius = StyleValues.radiusValue
        priceLabel.textColor = .white
        priceLabel.textAlignment = .center
    }
    
    public func configure(withJourney journey: Journey) {
        departureTimeLabel.text = getHourMinutes(fromDate: journey.departureStation.departureTime)
        departureCityLabel.text = journey.departureStation.stopName
        
        destinationTimeLabel.text = getHourMinutes(fromDate: journey.arrivalStation.arrivalTime)
        destinationCityLabel.text = journey.arrivalStation.stopName
        
        trainImageView.setTrainNo = journey.trainIdentifier
        
        trainConnectionLabel.text = "DIRECT"
        
        priceLabel.text = "\(journey.pricingInformation.secondClassPrice.value) \(journey.pricingInformation.secondClassPrice.unit)"
        
        guard let startTime = DateFormatter.getStringAsDate(inFormat: DateFormats.hourMinutesSeconds, date: journey.arrivalStation.arrivalTime), let endTime = DateFormatter.getStringAsDate(inFormat: DateFormats.hourMinutesSeconds, date: journey.departureStation.departureTime) else {
            print("error while parsing journey time")
            return
        }
        tripTimeLabel.text = DateFormatter.getIntervalBetween(startDate: startTime, endDate: endTime)
    }
    
    public func changePriceStyle() {
        priceLabel.backgroundColor = .white
        priceLabel.textColor = .shiraz
        priceLabel.textAlignment = .right
    }
    
    private func getHourMinutes(fromDate date: String) -> String {
        return date.split(separator: ":")[0] + ":" + date.split(separator: ":")[1]
    }
}

extension FareDetailsView {
    private enum PaddingValues {
        static let verySmallPadding: CGFloat = 4.0
        static let smallPadding: CGFloat = 8.0
        static let mediumPadding: CGFloat = 16.0
        static let bigPadding: CGFloat = 24.0
        static let hugePadding: CGFloat = 48.0
    }
    
    private enum StyleValues {
        static let radiusValue: CGFloat = 6.0
        static let shadowOpacity: Float = 0.3
        static let buttonWidth: CGFloat = 91.0
        static let buttonHeight: CGFloat = 34.0
    }
}
