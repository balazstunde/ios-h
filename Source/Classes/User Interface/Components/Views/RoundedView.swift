//
//  RoundedView.swift
//  Choo Choo
//
//  Created by Halcyon on 08/08/2019.
//

import UIKit

class RoundedView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners(corners: [.topLeft, .topRight], radius: 10)
    }
}
