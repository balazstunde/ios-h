//
//  FareOverviewStationsCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

class FareOverviewStationsCell: UITableViewCell {
    static let cellIdentifier = "FareOverviewStationsCell"
    private let timeLabel = UILabel()
    private let nameLabel = UILabel()
    private let separatorView = StationBulletPointView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        contentView.addSubview(timeLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(separatorView)
        
        nameLabel.font = Font.barlowRegular(size: .extraSmall)
        timeLabel.font = Font.barlowRegular(size: .extraSmall)
    }
    
    private func setupConstraints() {
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            timeLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            timeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Layout.timeLabelPadding),
            timeLabel.widthAnchor.constraint(equalToConstant: Layout.timeLabelWidth),
            
            separatorView.topAnchor.constraint(equalTo: contentView.topAnchor),
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separatorView.leadingAnchor.constraint(equalTo: timeLabel.trailingAnchor, constant: AppStyle.Layout.padding),
            separatorView.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor),
            
            nameLabel.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: separatorView.trailingAnchor, constant: AppStyle.Layout.padding),
            nameLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func configure(time: String, stopName: String) {
        timeLabel.text = String(time.prefix(5))
        nameLabel.text = stopName
    }
}

extension FareOverviewStationsCell {
    private enum Layout {
        static let timeLabelWidth: CGFloat = 34
        static let timeLabelPadding: CGFloat = 10
    }
}
