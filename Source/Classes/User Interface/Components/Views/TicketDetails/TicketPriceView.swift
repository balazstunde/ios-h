//
//  TicketPriceView.swift
//  Choo Choo
//
//  Created by Halcyon on 08/08/2019.
//

import Foundation
import UIKit

class TicketPriceView: UIView {
    private let ticketsLabel = UILabel()
    private let totalLabel = UILabel()
    private let totalPriceLabel = UILabel()
    private let passengerLabel = UILabel()
    private let priceLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        layer.cornerRadius = 3
        
        addSubview(ticketsLabel)
        addSubview(totalLabel)
        addSubview(totalPriceLabel)
        addSubview(passengerLabel)
        addSubview(priceLabel)

        ticketsLabel.font = Font.barlowRegular(size: .small)
        ticketsLabel.text = "Tickets:"
        
        totalLabel.font = Font.barlowBold(size: .mediumExtra)
        totalLabel.text = "Total:"
        
        totalPriceLabel.font = Font.barlowBold(size: .mediumExtra)
        totalPriceLabel.textColor = .shiraz
        
        passengerLabel.font = Font.barlowRegular(size: .small)
        passengerLabel.numberOfLines = 0
        priceLabel.font = Font.barlowRegular(size: .small)
        priceLabel.numberOfLines = 0
        priceLabel.textAlignment = .right
        
        setupConstraints()
    }
    
    func setDetails(passenger: String, price: String, totalPrice: String) {
        passengerLabel.text = passenger
        priceLabel.text = price
        totalPriceLabel.text = totalPrice
    }
    
    private func setupConstraints() {
        ticketsLabel.translatesAutoresizingMaskIntoConstraints = false
        totalLabel.translatesAutoresizingMaskIntoConstraints = false
        totalPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        passengerLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            ticketsLabel.topAnchor.constraint(equalTo: topAnchor, constant: LayoutValues.marginPadding),
            ticketsLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            
            passengerLabel.topAnchor.constraint(equalTo: ticketsLabel.topAnchor),
            passengerLabel.leadingAnchor.constraint(equalTo: ticketsLabel.trailingAnchor, constant: LayoutValues.marginPadding),
            
            priceLabel.topAnchor.constraint(equalTo: ticketsLabel.topAnchor),
            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            
            totalLabel.topAnchor.constraint(equalTo: passengerLabel.bottomAnchor, constant: LayoutValues.totalTopPadding),
            totalLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            
            totalPriceLabel.centerYAnchor.constraint(equalTo: totalLabel.centerYAnchor),
            totalPriceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            
            totalPriceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -LayoutValues.marginPadding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension TicketPriceView {
    private enum LayoutValues {
        static let marginPadding: CGFloat = 16
        static let totalTopPadding: CGFloat = 4
    }
}
