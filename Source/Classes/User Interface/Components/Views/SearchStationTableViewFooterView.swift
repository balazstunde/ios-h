//
//  SearchStationTableViewFooter.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 07/08/2019.
//

import UIKit

class SearchStationTableViewFooterView: UIView {
    private let spinner = UIActivityIndicatorView(style: .gray)
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(spinner)
        backgroundColor = .white
    }
    
    private func setupConstraints() {
        spinner.translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func stopAnimation() {
        spinner.isHidden = true
        spinner.stopAnimating()
    }
    
    public func startAnimation() {
        spinner.isHidden = false
        spinner.startAnimating()
    }
}
