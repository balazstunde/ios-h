//
//  TravelPreferencesDataSource.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation

class TravelPreferencesDataSource {
    
    static func getData() -> [TravelPreferences] {
        var travelPreferences: [TravelPreferences] = []
        travelPreferences.append(TravelPreferences(name: "2nd Class", price: 73.45))
        travelPreferences.append(TravelPreferences(name: "1st Class", price: 50))
        travelPreferences.append(TravelPreferences(name: "Coach", price: 78))
        
        return travelPreferences
    }
}
