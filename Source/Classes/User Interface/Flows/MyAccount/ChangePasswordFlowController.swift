//
//  ChangePasswordFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 07/08/2019.
//

import UIKit

class ChangePasswordFlowController: NavigationFlowController {
    override var firstScreen: UIViewController {
        let changePasswordViewController = ChangePasssordViewController()
        changePasswordViewController.delegate = self
        return changePasswordViewController
    }
}
extension ChangePasswordFlowController: ChangePasswordViewControllerFlowDelegate {
    func didTouchBackButton(on viewController: ChangePasssordViewController) {
        navigationController.popViewController(animated: true)
    }
    
    func didChangePasswordFailed(on viewController: ChangePasssordViewController) {
        let alert = UIAlertController(title: "Could not reset the password", message: "Try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        navigationController.present(alert, animated: true, completion: nil)
    }
    
    func didChangePasswordSuccedeed(on viewController: ChangePasssordViewController) {
        let alert = UIAlertController(title: "Password successfully changed", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        navigationController.present(alert, animated: true, completion: nil)
    }
    
    func didTouchForgotPassword(on viewController: ChangePasssordViewController) {
        let passwordRecoveryFlowController = PasswordRecoveryFlowController(from: self)
        passwordRecoveryFlowController.start(presentationStyle: .push)
    }
    
}
