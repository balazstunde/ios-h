//
//  UIView+addDashedBorder.swift
//  Choo Choo
//
//  Created by Halcyon on 13/08/2019.
//

import UIKit

extension UIView {
    func addDashedBorder() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lola.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [2,3]
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: self.frame.height / 2),
                                CGPoint(x: self.frame.size.width, y: self.frame.size.height / 2)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
