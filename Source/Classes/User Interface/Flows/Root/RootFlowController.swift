//
//  RootFlowController.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 5/17/17.
//  Copyright © 2017 HalcyonMobile. All rights reserved.
//

import UIKit

class RootFlowController: FlowController {
    var presentationStyle: FlowPresentationStyle = .push
    
    // root flowcontroller will have always only one child flow
    var childFlows: [FlowController] = []
    private var homeTabBarController: UITabBarController?
    
    var mainViewController: UIViewController? {
        return window?.rootViewController
    }
    
    private var searchViewController: UIViewController?
    private var ticketViewController: UIViewController?
    private var loginViewController: UIViewController?
    
    // MARK: - Lifecycle
    
    init(window: UIWindow?) {
        self.window = window
        setRootViewController()
    }
    
    private func setRootViewController() {
        homeTabBarController = UITabBarController()
        window?.rootViewController = homeTabBarController
    }
    
    func start() {
        let searchFlowController = SearchFlowController(from: self)
        searchFlowController.start()
        let searchViewController = searchFlowController.mainViewController ?? UIViewController()
        let searchTabBarItem = UITabBarItem(title: nil, image: UIImage(asset: Asset.icNavigationHomeDisabled), tag: 1)
        searchTabBarItem.selectedImage = UIImage(asset: Asset.icNavigationHomeEnabled)
        searchViewController.tabBarItem = searchTabBarItem

        let ticketFlowController = TicketFlowController(from: self)
        ticketFlowController.start()
        let ticketViewController = ticketFlowController.mainViewController ?? UIViewController()
        let ticketTabBarItem = UITabBarItem(title: nil, image: UIImage(asset: Asset.icNavigationTicketDisabled), tag: 2)
        ticketTabBarItem.selectedImage = UIImage(asset: Asset.icNavigationTicketEnabled)
        ticketViewController.tabBarItem = ticketTabBarItem
        
        let loginViewController: UIViewController
        
        if KeyStore.shared.currentUser != nil {
            loginViewController = myAccountViewControllerConfiguration()
        } else {
            loginViewController = loginOptionViewControllerConfiguration()
        }
        
        self.searchViewController = searchViewController
        self.ticketViewController = ticketViewController
        self.loginViewController = loginViewController
        
        configureLoginViewController()
        
        configureTabBar(for: [searchViewController,
                              ticketViewController,
                              loginViewController])
    }
    
    private func configureTabBar(for viewControllers: [UIViewController]) {
        homeTabBarController?.viewControllers = viewControllers
        homeTabBarController?.tabBar.tintColor = .shiraz
    }
    
    private func loginOptionViewControllerConfiguration() -> UIViewController {
        let loginOptionFlowController = LoginOptionFlowController(from: self)
        loginOptionFlowController.flowDelegate = self
        loginOptionFlowController.start()
        let loginOptionViewController = loginOptionFlowController.mainViewController ?? UIViewController()
        
        return loginOptionViewController
    }
    
    private func myAccountViewControllerConfiguration() -> UIViewController {
        let myAccountFlowController = MyAccountFlowController(from: self)
        myAccountFlowController.flowDelegate = self
        myAccountFlowController.start()
        let myAccountViewController = myAccountFlowController.mainViewController ?? UIViewController()
        
        return myAccountViewController
    }
    
    private func configureLoginViewController() {
        let loginOptionTabBarItem = UITabBarItem(title: nil, image: UIImage(asset: Asset.icNavigationProfileDisabled), tag: 3)
        loginOptionTabBarItem.selectedImage = UIImage(asset: Asset.icNavigationProfileEnabled)
        loginViewController?.tabBarItem = loginOptionTabBarItem
    }
    
    func finish() {
        // root flowcontroller never finish
    }
    
    private weak var window: UIWindow?
    
    // MARK: - Set root
    
    private var root: UIViewController? {
        return window?.rootViewController
    }
    
    private func setRoot(to viewController: UIViewController?, animated: Bool = false) {
        guard let viewController = viewController else { return }
        
        guard root != viewController else { return }
        
        func changeRoot(to viewController: UIViewController) {
            window?.rootViewController = viewController
        }

        if animated {
            UIView.transition(with: window!, duration: AnimationDuration.normal, options: .transitionCrossDissolve, animations: {
                changeRoot(to: viewController)
            }, completion: nil)
        } else {
            changeRoot(to: viewController)
        }
    }
    
    // MARK: - Show flows
    
    private func show(flow: FlowController, animated: Bool = true) {
        flow.start()
    }
}

extension RootFlowController: LogInOptionFlowControllerDelegate {
    func didLogin(_ loginOptionFlowController: LoginOptionFlowController) {
        loginViewController = myAccountViewControllerConfiguration()
        configureLoginViewController()
        homeTabBarController?.setViewControllers([searchViewController ?? UIViewController(),
                                                  ticketViewController ?? UIViewController(),
                                                  loginViewController ?? UIViewController()],
                                                 animated: true)
        removeChild(flow: loginOptionFlowController)
    }
}

extension RootFlowController: MyAccountFlowControllerDelegate {
    func didLogout(_ myAccountFlowController: MyAccountFlowController) {
        loginViewController = loginOptionViewControllerConfiguration()
        configureLoginViewController()
        homeTabBarController?.setViewControllers([searchViewController ?? UIViewController(),
                                                  ticketViewController ?? UIViewController(),
                                                  loginViewController ?? UIViewController()],
                                                 animated: true)
        removeChild(flow: myAccountFlowController)
    }
}
