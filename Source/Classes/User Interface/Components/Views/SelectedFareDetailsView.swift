//
//  SelectedFareDetailsView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

class SelectedFareDetailsView: UIView {
    private let tickImageView = UIImageView()
    private let fareDetailsView = FareDetailsView()
    private let outboundLabel = UILabel()
    private let shadowView = UIView()
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        shadowView.layer.shadowPath = UIBezierPath(rect: shadowView.bounds).cgPath
    }
    
    private func setupView() {
        addSubview(shadowView)
        addSubview(tickImageView)
        addSubview(outboundLabel)
        addSubview(fareDetailsView)
        
        shadowView.backgroundColor = .white
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOpacity = Style.shadowOpacity
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
        shadowView.layer.shadowRadius = Style.shadowRadius
        shadowView.layer.shouldRasterize = true
        
        outboundLabel.text = "Selected outbound"
        outboundLabel.font = Font.barlowSemiBold(size: .medium)
        
        tickImageView.image = Asset.icNavigationCheck.image
        tickImageView.image?.withRenderingMode(.alwaysTemplate)
        tickImageView.tintColor = .shiraz
        tickImageView.contentMode = .scaleAspectFill
        
        fareDetailsView.changePriceStyle()
    }
    
    private func setupConstraints() {
        tickImageView.translatesAutoresizingMaskIntoConstraints = false
        outboundLabel.translatesAutoresizingMaskIntoConstraints = false
        fareDetailsView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            shadowView.leadingAnchor.constraint(equalTo: leadingAnchor),
            shadowView.topAnchor.constraint(equalTo: topAnchor),
            shadowView.trailingAnchor.constraint(equalTo: trailingAnchor),
            shadowView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            tickImageView.topAnchor.constraint(equalTo: topAnchor, constant: AppStyle.PaddingValues.mediumPadding),
            tickImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: AppStyle.PaddingValues.mediumPadding),
            
            outboundLabel.leadingAnchor.constraint(equalTo: tickImageView.trailingAnchor, constant: Layout.outboundLabelPadding),
            outboundLabel.topAnchor.constraint(equalTo: tickImageView.topAnchor),
            
            fareDetailsView.topAnchor.constraint(equalTo: tickImageView.bottomAnchor),
            fareDetailsView.heightAnchor.constraint(equalToConstant: Layout.fareDetailsHeight),
            fareDetailsView.leadingAnchor.constraint(equalTo: leadingAnchor),
            fareDetailsView.trailingAnchor.constraint(equalTo: trailingAnchor),
            fareDetailsView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func configure(withFare journey: Journey) {
        fareDetailsView.configure(withJourney: journey)
    }
}

extension SelectedFareDetailsView {
    private enum Layout {
        static let outboundLabelPadding: CGFloat = 10
        static let fareDetailsHeight: CGFloat = 164
    }
    
    private enum Style {
        static let shadowOpacity: Float = 0.7
        static let shadowRadius: CGFloat = 10
        static let rowHeight: CGFloat = 30
    }
}
