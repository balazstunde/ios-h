//
//  PurchasedTicketViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 21/08/2019.
//

import Foundation
import UIKit

class PurchasedTicketViewController: BaseViewController {
    private let thanksLabel = UILabel()
    private let enjoyLabel = UILabel()
    private let doneButton = MainButton(title: "Done")
    private let ticketDetailsView: TicketDetailsView!
    private let ticket: Ticket!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupConstraints()
    }
    
    init(ticket: Ticket) {
        self.ticket = ticket
        ticketDetailsView = TicketDetailsView(with: ticket)
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        view.backgroundColor = .alabaster
        navigationItem.title = "Your ticket"
        
        view.addSubview(thanksLabel)
        view.addSubview(enjoyLabel)
        view.addSubview(doneButton)
        view.addSubview(ticketDetailsView)
        
        thanksLabel.text = "Thanks for your order!"
        thanksLabel.font = Font.barlowSemiBold(size: .mediumLarge)
        
        enjoyLabel.text = "Here's your ticket. Have a nice trip!"
        enjoyLabel.font = Font.barlowRegular(size: .mediumLarge)
        
        doneButton.layer.cornerRadius = LayoutValues.cornerRadius
    }
    
    private func setupConstraints() {
        thanksLabel.translatesAutoresizingMaskIntoConstraints = false
        enjoyLabel.translatesAutoresizingMaskIntoConstraints = false
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        ticketDetailsView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            thanksLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: LayoutValues.thanksTopPadding),
            thanksLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            enjoyLabel.topAnchor.constraint(equalTo: thanksLabel.bottomAnchor),
            enjoyLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            ticketDetailsView.topAnchor.constraint(equalTo: enjoyLabel.bottomAnchor, constant: LayoutValues.ticketTopPadding),
            ticketDetailsView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.marginPadding),
            ticketDetailsView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.marginPadding),
            
            doneButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.marginPadding),
            doneButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.marginPadding),
            doneButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -LayoutValues.marginPadding),
            doneButton.heightAnchor.constraint(equalToConstant: LayoutValues.buttonHeight)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension PurchasedTicketViewController {
    private enum LayoutValues {
        static let thanksTopPadding: CGFloat = 46
        static let ticketTopPadding: CGFloat = 56
        static let buttonHeight: CGFloat = 48
        static let marginPadding: CGFloat = 16
        static let cornerRadius: CGFloat = 3
    }
}
