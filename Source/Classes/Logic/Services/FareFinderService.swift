//
//  FareFinderService.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import Foundation

class FareFinderService {
    
    func findFares(_ journeySearch: JourneySearch, completion: @escaping (PaginatedSearchResult) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.Auth.PostJourneySearchRequest(journey: journeySearch), success: { (journey: PaginatedSearchResult) in
            completion(journey)
        }, failure: { error in
            failure(error)
        })
        
    }
}
