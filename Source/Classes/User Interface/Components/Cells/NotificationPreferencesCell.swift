//
//  NotificationPreferencesCell.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

class NotificationPreferencesCell: UITableViewCell {
    private var nameLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(_ rowData: String) {
        nameLabel.text = rowData
    }
    
    private func setupView() {
        nameLabel.font = Font.barlowRegular(size: FontSize.mediumSmall)
        nameLabel.textColor = .black
        
        contentView.addSubview(nameLabel)
    }
    
    private func setupConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
