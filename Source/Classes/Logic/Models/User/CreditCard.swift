//
//  CreditCard.swift
//  Choo Choo
//
//  Created by Halcyon on 13/08/2019.
//

import Foundation

struct CreditCard: Codable {
    var cvv: String
    var holderName: String
    var number: String
    var validUntil: String //yyyy-MM-dd
}
