//
//  EditMyAccountFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

class EditMyAccountFlowController: NavigationFlowController {
    override var firstScreen: UIViewController {
        let editMyAccountViewController = EditMyAccountViewController()
        editMyAccountViewController.delegate = self
        return editMyAccountViewController
    }
}

extension EditMyAccountFlowController: EditMyAccountViewControllerFLowDelegate {
    
    func didTouchBackButton(on viewControllrt: EditMyAccountViewController) {
        navigationController.popViewController(animated: true)
    }
 
    func didTouchEditPassword(on viewController: EditMyAccountViewController) {
        let changePasswordFlowController = ChangePasswordFlowController(from: self)
        changePasswordFlowController.start(presentationStyle: .push)
    }
   
}
