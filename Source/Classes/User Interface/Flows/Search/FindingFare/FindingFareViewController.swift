//
//  FindingFareViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import UIKit

protocol FindingFareViewControllerDelegate: AnyObject {
    func didTouchOnCell(on viewController: FindingFareViewController, journey: Journey)
    func didRequestFail(on viewController: FindingFareViewController)
    func didTapToLabel(on viewController: FindingFareViewController, type: SearchStationType)
    func didTapFromLabel(on viewController: FindingFareViewController, type: SearchStationType)
}

class FindingFareViewController: BaseViewController {
    weak var delegate: FindingFareViewControllerDelegate?
    
    //TODO: add detail screen on top
    private let tableView = UITableView()
    private let cellIdentifier = "SearchResultCell"
    private var tableViewHeader: FindingFareHeaderView
    private let bookingHeaderView = BookingHeaderView()

    var bookingService: BookingService!
    private var fares: [Journey] = []
    private let animation: TrainLoadingAnimationView
    private let noResultView = FindingFareNoResultView()
    private let sortMethodsView = SortMethodViewController()
    private let fareFinderService = FareFinderService()
    private var pageNumber: Int = 0
    private var requestFailed = false
    
    init(withDetails details: BookingService) {
        bookingService = details
        
        let fromLocation = bookingService?.ticketDetails.fromDestination?.name ?? ""
        let toLocation = bookingService?.ticketDetails.toDestination?.name ?? ""
        animation = TrainLoadingAnimationView(from: fromLocation, to: toLocation)
        
        let date = bookingService?.ticketDetails.date ?? Date()
        let dateOutbound = DateFormatter.getDateAsString(inFormat: .nameOfTheDayMonthDay, date: date)
        
        let personOutbounds = bookingService?.getDetailsAsString()
        tableViewHeader = FindingFareHeaderView(withOutbounds: "\(dateOutbound), \(String(describing: personOutbounds))")
        
        bookingHeaderView.setFromLabelText(with: fromLocation)
        bookingHeaderView.setToLabelText(with: toLocation)
        bookingHeaderView.setTitleText(with: "Checking for available trains from")
        
        super.init()
        setupView()
        setupConstraints()
        loadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animation.startAnimation()
    }
    
    private func setupView() {
        hidesBottomBarWhenPushed = true
        view.addSubview(tableView)
        tableView.register(FindingFareTableCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = tableViewHeader
        tableView.separatorStyle = .none
        view.backgroundColor = .white
        tableView.alwaysBounceVertical = false
        
        view.addSubview(animation)
        view.addSubview(noResultView)
        view.addSubview(bookingHeaderView)
        self.noResultView.isHidden = true
        
        animation.didFinishAnimation = {
            if self.requestFailed {
                self.delegate?.didRequestFail(on: self)
            }
            self.view.sendSubviewToBack(self.animation)
            if self.fares.isEmpty {
                self.tableView.isHidden = true
                self.animation.isHidden = true
                self.noResultView.isHidden = false
            } else {
                self.bookingHeaderView.setTitleText(with: "When would you like to leave from")
                self.noResultView.isHidden = true
                self.tableView.isHidden = false
                self.animation.isHidden = false
            }
        }

        navigationItem.title = "Finding Fare"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain,
                                                           target: self, action: #selector(didTouchBackButton))
        
        noResultView.didTouchedSearchAgainButton = {
            self.navigationController?.popViewController(animated: true)
        }
        
        tableViewHeader.didTouchSortMethodsButton = { [weak self] in
            guard let `self` = self else { return }
            self.sortMethodsView.delegate = self
            self.navigationController?.present(self.sortMethodsView, animated: true)
        }
        
        bookingHeaderView.didTapFromLabelClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapFromLabel(on: self, type: .departure)
        }
        
        bookingHeaderView.didTapToLabelClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapToLabel(on: self, type: .destination)
        }
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeader.translatesAutoresizingMaskIntoConstraints = false
        animation.translatesAutoresizingMaskIntoConstraints = false
        noResultView.translatesAutoresizingMaskIntoConstraints = false
        bookingHeaderView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
                bookingHeaderView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                bookingHeaderView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                bookingHeaderView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
                tableView.topAnchor.constraint(equalTo: bookingHeaderView.bottomAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                
                tableViewHeader.widthAnchor.constraint(equalTo: tableView.widthAnchor),
                tableViewHeader.topAnchor.constraint(equalTo: tableView.topAnchor),
                tableViewHeader.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
                
                animation.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                animation.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                animation.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                animation.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                
                noResultView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                noResultView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                noResultView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                noResultView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    struct PageInfo {
        var status: PageFetchStatus
        var pageNumber: Int
    }
    
    enum PageFetchStatus {
        case complete
        case incomplete
    }
    
    private func loadData() {
        guard let endStopId = bookingService?.ticketDetails.toDestination?.id,
            let startStopId = bookingService?.ticketDetails.fromDestination?.id,
            let date = bookingService?.ticketDetails.date else { return }
        let stringDate = DateFormatter.getDateAsString(inFormat: .yearMonthDayWithLine, date: date)
        
        if pageNumber == FetchStatus.incomplete {
            tableView.tableFooterView = FindingFareFooterView(withFaresFound: fares.count, frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 60))
            return
        }
        var desiredTickets: [String: Int] = [:]
        bookingService?.getDetails()?.filter({ $0.value > 0 }).forEach({ desiredTickets[$0.key.uppercased()] = $0.value })
        let journeyGenerator = JourneySearch(date: stringDate, endStationId: endStopId, startStationId: startStopId, desiredTickets: desiredTickets, pagingInfo: PageableSearch(numberOfElements: 5, pageNumber: pageNumber))
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        if pageNumber != 0 {
            spinner.color = .shiraz
            spinner.startAnimating()
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
        }
        pageNumber += 1
        fareFinderService.findFares(journeyGenerator, completion: { (fare) in
            if fare.results.isEmpty || fare.results.count < 5 {
                self.pageNumber = FetchStatus.incomplete
            }
            self.fares += fare.results
            DispatchQueue.main.async {
                spinner.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.didSelectSortMethod(.departureTime)
                self.tableView.reloadData()
            }
        }, failure: { (error) in
            self.requestFailed = true
        })
    }
    
    @objc private func didTouchBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    private func didTouchCell(journey: Journey) {
        delegate?.didTouchOnCell(on: self, journey: journey)
    }
}

extension FindingFareViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fares.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as? FindingFareTableCell else {
            fatalError("Could not dequeue FindingFareCell")
        }
        
        let fare = fares[indexPath.row]
        cell.configure(withFare: fare)
        cell.selectionStyle = .none
        return cell
    }
}

extension FindingFareViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        didTouchCell(journey: fares[indexPath.row])
        cell?.isSelected = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height - UIScreen.main.bounds.width * 0.5) {
            loadData()
        }
    }
}

extension FindingFareViewController {
    private enum PaddingValues {
        static let mediumPadding: CGFloat = 16.0
    }
    private enum FetchStatus {
        static let incomplete: Int = -1
    }
}

extension FindingFareViewController: SortMethodViewControllerDelegate {
    func didSelectSortMethod(_ method: SortMethod) {
        switch method {
        case .price:
            fares.sort { $0.pricingInformation.secondClassPrice.value < $1.pricingInformation.secondClassPrice.value }
        case .departureTime:
            fares.sort { (first, second) -> Bool in
                guard let firstDate = DateFormatter.getStringAsDate(inFormat: DateFormats.hourMinutesSeconds, date: first.arrivalStation.departureTime),
                    let secondDate = DateFormatter.getStringAsDate(inFormat: DateFormats.hourMinutesSeconds, date: second.departureStation.departureTime) else { return false}
                return firstDate.compare(secondDate) == .orderedAscending
            }
        }
        tableViewHeader.sortMethod = method.rawValue
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func didTouchOutside(on viewController: SortMethodViewController) {
        DispatchQueue.main.async {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}
