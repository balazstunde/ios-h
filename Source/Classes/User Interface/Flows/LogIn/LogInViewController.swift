//
//  LogInViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 31/07/2019.
//

import UIKit

protocol LogInViewControllerFlowDelegate: AnyObject {
    func didTouchForgotPasswordButton(from: LogInViewController)
    func didSuccesfullyLogIn(from: LogInViewController)
    func didSelectBackButton(from: LogInViewController)
    func didLoginFail(from: LogInViewController)
}

class LogInViewController: BaseViewController {
    
    enum Paddings {
        static let noPadding: CGFloat = 0
        static let smallestPadding: CGFloat = 1
        static let smallPadding: CGFloat  =  10
        static let mediumSmallPadding: CGFloat = 15
        static let mediumPadding: CGFloat = 20
        static let mediumLargePadding: CGFloat = 30
        static let largePadding: CGFloat = 50
        static let iconHeight: CGFloat = 28
        static let iconWidth: CGFloat = 22
    }
    
    weak var delegate: LogInViewControllerFlowDelegate?
    
    private let authService = AuthenticationService()
    
    private let mainContent = UIView()
    private let underlineEmail = UIView()
    private let underlinePassword = UIView()
    private var isPasswordButtonDown = false
    
    //Labels
    private let emailLabel = UILabel()
    private let passwordLabel = UILabel()
    private let passwordErrorLabel = UILabel()
    
    //Stacks
    private var emailStackView = UIStackView()
    private var passwordStackView = UIStackView()
    
    //Buttons
    private let logInButton = UIButton()
    private let forgotPasswordButton = UIButton()
    private var showPasswordButton = UIButton()
    
    //TextFields
    private var emailTextField: UITextField!
    private var passwordTextField: UITextField!
    
    //Images & Icons
    private let emailIcon = UIImageView(image: #imageLiteral(resourceName: "ic_navigation_email"))
    private let passwordIcon = UIImageView(image: #imageLiteral(resourceName: "ic_navigation_password"))
    private var backArrowIcon = UIImage(named: "ic_navigation_back")
    private let spinner = UIActivityIndicatorView(style: .gray)
    
    private var underlineViewHeightConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initConstraints()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    private func initView() {
        view.backgroundColor = .white
        
        underlineEmail.backgroundColor = .textDescriptionColor
        underlinePassword.backgroundColor = .textDescriptionColor
        
        initLabels()
        initButtons()
        initIcons()
        initNavBar()
        initTextFields()
        initStackViews()
        
        mainContent.addSubview(emailLabel)
        mainContent.addSubview(emailStackView)
        mainContent.addSubview(underlineEmail)
        mainContent.addSubview(passwordLabel)
        mainContent.addSubview(passwordStackView)
        mainContent.addSubview(underlinePassword)
        mainContent.addSubview(passwordErrorLabel)
        mainContent.addSubview(forgotPasswordButton)
        mainContent.addSubview(logInButton)
        mainContent.addSubview(spinner)
        view.addSubview(mainContent)
    }
    
    private func initLabels() {
        emailLabel.text = "Email address"
        emailLabel.textColor = .white
        emailLabel.font = Font.barlowRegular(size: .small)
        
        passwordLabel.text = "Password (6+ characters)"
        passwordLabel.textColor = .white
        passwordLabel.font = Font.barlowRegular(size: .small)
        
        passwordErrorLabel.text = "Check field for any errors"
        passwordErrorLabel.textColor = .white
        passwordErrorLabel.font = Font.barlowRegular(size: .extraSmall)
    }
    
    private func initStackViews() {
        emailStackView = UIStackView(arrangedSubviews: [emailIcon, emailTextField])
        emailStackView.axis = .horizontal
        emailStackView.spacing = 10
        emailStackView.distribution = .fillProportionally
        
        passwordStackView = UIStackView(arrangedSubviews: [passwordIcon, passwordTextField, showPasswordButton])
        passwordStackView.axis = .horizontal
        passwordStackView.spacing = 16
        passwordStackView.distribution = .fillProportionally
    }
    
    private func initTextFields() {
        emailTextField = UITextField()
        emailTextField.setPlaceholderFont(title: "Email address", font: Font.barlowRegular(size: .mediumSmall))
        emailTextField.borderStyle = .none
        emailTextField.textContentType = .emailAddress
        emailTextField.keyboardType = .emailAddress
        emailTextField.autocapitalizationType = .none
        emailTextField.font = Font.barlowRegular(size: .mediumSmall)
        
        passwordTextField = UITextField()
        passwordTextField.setPlaceholderFont(title: "Password (6+ characters)", font: Font.barlowRegular(size: .mediumSmall))
        passwordTextField.textContentType = .password
        passwordTextField.isSecureTextEntry = true
        passwordTextField.borderStyle = .none
        passwordTextField.font = Font.barlowRegular(size: .mediumSmall)
    }
    
    private func initNavBar() {
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backArrowIcon, style: .plain, target: self,
                                                           action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                                   NSAttributedString.Key.font: Font.barlowSemiBold(size: .medium)]
        navigationItem.title = "Log In"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    private func initIcons() {
        emailIcon.image = emailIcon.image?.withRenderingMode(.alwaysTemplate)
        emailIcon.tintColor = .iconColor
        passwordIcon.image = passwordIcon.image?.withRenderingMode(.alwaysTemplate)
        passwordIcon.tintColor = .iconColor
        backArrowIcon = backArrowIcon?.withRenderingMode(.alwaysTemplate)
    }
    
    private func initButtons() {
        logInButton.setTitle("Log In", for: .normal)
        logInButton.titleLabel?.font = Font.barlowBold(size: .mediumSmall)
        logInButton.backgroundColor = .lola
        logInButton.isEnabled = false
        logInButton.layer.cornerRadius = 5
        logInButton.addTarget(self, action: #selector(logInButtonTapped), for: .touchUpInside)
        
        forgotPasswordButton.setTitle("Forgot password", for: .normal)
        forgotPasswordButton.setTitleColor(UIColor(red: 166/255, green: 166/255, blue: 166/255, alpha: 1), for: .normal)
        forgotPasswordButton.backgroundColor = .white
        forgotPasswordButton.titleLabel?.font = Font.barlowBold(size: .small)
        let yourAttributes : [NSAttributedString.Key: Any] = [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Forgot password", attributes: yourAttributes)
        forgotPasswordButton.setAttributedTitle(attributeString, for: .normal)
        forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordButtonTapped), for: .touchUpInside)
        
        showPasswordButton.setImage(Asset.icNavigationShowPassword.image, for: .normal)
        showPasswordButton.tintColor = .lola
        showPasswordButton.addTarget(self, action: #selector(didPressShowPassword), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    private func initConstraints() {
        mainContent.translatesAutoresizingMaskIntoConstraints = false
        emailStackView.translatesAutoresizingMaskIntoConstraints = false
        passwordStackView.translatesAutoresizingMaskIntoConstraints = false
        logInButton.translatesAutoresizingMaskIntoConstraints = false
        forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        underlineEmail.translatesAutoresizingMaskIntoConstraints = false
        underlinePassword.translatesAutoresizingMaskIntoConstraints = false
        emailLabel.translatesAutoresizingMaskIntoConstraints = false
        passwordLabel.translatesAutoresizingMaskIntoConstraints = false
        passwordErrorLabel.translatesAutoresizingMaskIntoConstraints = false
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints = [NSLayoutConstraint]()
        
        //mainContent
        constraints.append(mainContent.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Paddings.mediumPadding))
        constraints.append(mainContent.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Paddings.noPadding))
        constraints.append(mainContent.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: Paddings.noPadding))
        constraints.append(mainContent.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: Paddings.noPadding))
        
        //forgot password
        constraints.append(forgotPasswordButton.topAnchor.constraint(equalTo: underlinePassword.bottomAnchor, constant: Paddings.largePadding))
        constraints.append(forgotPasswordButton.centerXAnchor.constraint(equalTo: mainContent.centerXAnchor, constant: Paddings.noPadding))
        constraints.append(forgotPasswordButton.heightAnchor.constraint(equalToConstant: Paddings.mediumLargePadding))
        
        //logInbutton
        constraints.append(logInButton.centerXAnchor.constraint(equalTo: mainContent.centerXAnchor, constant: Paddings.noPadding))
        constraints.append(logInButton.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Paddings.smallPadding))
        constraints.append(logInButton.topAnchor.constraint(equalTo: forgotPasswordButton.bottomAnchor, constant: Paddings.mediumSmallPadding))
        constraints.append(logInButton.heightAnchor.constraint(equalToConstant: Paddings.largePadding))
        
        //Icons
        underlineViewHeightConstraint = underlineEmail.heightAnchor.constraint(equalToConstant: Paddings.smallestPadding)
        underlineViewHeightConstraint?.isActive = true
        constraints.append(underlinePassword.heightAnchor.constraint(equalToConstant: Paddings.smallestPadding))
        constraints.append(emailIcon.widthAnchor.constraint(equalToConstant: Paddings.iconHeight))
        constraints.append(passwordIcon.widthAnchor.constraint(equalToConstant: Paddings.iconWidth))
        constraints.append(passwordIcon.heightAnchor.constraint(equalToConstant: Paddings.iconHeight))
        constraints.append(showPasswordButton.widthAnchor.constraint(equalToConstant: Paddings.mediumLargePadding))
        
        //spinner
        constraints.append(spinner.centerXAnchor.constraint(equalTo: mainContent.centerXAnchor, constant: Paddings.noPadding))
        constraints.append(spinner.topAnchor.constraint(equalTo: logInButton.bottomAnchor, constant: Paddings.mediumLargePadding))
        
        // email label
        constraints.append(emailLabel.topAnchor.constraint(equalTo: mainContent.topAnchor, constant: Paddings.smallPadding))
        constraints.append(emailLabel.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor))
        constraints.append(emailLabel.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        // email stack view
        constraints.append(emailStackView.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: Paddings.smallPadding))
        constraints.append(emailStackView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Paddings.mediumPadding))
        constraints.append(emailStackView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        // email underline
        constraints.append(underlineEmail.topAnchor.constraint(equalTo: emailStackView.bottomAnchor, constant: Paddings.mediumSmallPadding))
        constraints.append(underlineEmail.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor))
        constraints.append(underlineEmail.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        // password label
        constraints.append(passwordLabel.topAnchor.constraint(equalTo: underlineEmail.bottomAnchor, constant: Paddings.mediumPadding))
        constraints.append(passwordLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor))
        constraints.append(passwordLabel.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        // password stack view
        constraints.append(passwordStackView.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: Paddings.smallPadding))
        constraints.append(passwordStackView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Paddings.mediumPadding))
        constraints.append(passwordStackView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        // password underline
        constraints.append(underlinePassword.topAnchor.constraint(equalTo: passwordStackView.bottomAnchor, constant: Paddings.mediumSmallPadding))
        constraints.append(underlinePassword.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor))
        constraints.append(underlinePassword.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        // password error
        constraints.append(passwordErrorLabel.topAnchor.constraint(equalTo: underlinePassword.bottomAnchor))
        constraints.append(passwordErrorLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor))
        constraints.append(passwordErrorLabel.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Paddings.mediumPadding)))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func didPressShowPassword() {
        showPasswordButton.tintColor = .iconColor
        if isPasswordButtonDown {
            showPasswordButton.setImage(Asset.icNavigationShowPassword.image, for: .normal)
            passwordTextField.isSecureTextEntry = true
        } else {
            showPasswordButton.setImage(Asset.icNavigationHidePassword.image, for: .normal)
            passwordTextField.isSecureTextEntry = false
        }
        isPasswordButtonDown = !isPasswordButtonDown
    }
    
    @objc private func backButtonTapped() {
        delegate?.didSelectBackButton(from: self)
    }
    
    @objc private func logInButtonTapped() {
        spinner.startAnimating()
        guard let userEmail = emailTextField.text,
            let userPassword = passwordTextField.text
            else {
                return
        }
        authService.logIn(userEmail: userEmail, userPassword: userPassword, completion: { (auth: AuthenticationDto) in
            
                DispatchQueue.main.async {
                    self.delegate?.didSuccesfullyLogIn(from: self)
                }
                
        }, failure: { _ in
            DispatchQueue.main.async {
                self.delegate?.didLoginFail(from: self)
            }
        })
        spinner.stopAnimating()
    }

    @objc private func forgotPasswordButtonTapped() {
        delegate?.didTouchForgotPasswordButton(from: self)
    }
}

extension LogInViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == emailTextField {
            guard let userEnteredString = emailTextField.text as NSString?,
                let newString = userEnteredString.replacingCharacters(in: range, with: string) as NSString? else { return false }
            if  newString != "" && passwordTextField.text!.isEmpty == false {
                logInButton.backgroundColor = .primary
                logInButton.isEnabled = true
            } else {
                logInButton.isEnabled = false
                logInButton.backgroundColor = .lola
            }
        } else {
            if textField == passwordTextField {
                 guard let userEnteredString = passwordTextField.text as NSString?,
                    let newString = userEnteredString.replacingCharacters(in: range, with: string) as NSString? else { return false }
                if  newString.length >= 6 {
                    passwordErrorLabel.textColor = .white
                    passwordLabel.textColor = .primary
                    if emailTextField.text!.isEmpty == false {
                        logInButton.backgroundColor = .primary
                        logInButton.isEnabled = true
                    }
                    
                } else {
                    logInButton.isEnabled = false
                    logInButton.backgroundColor = .lola
                    passwordErrorLabel.textColor = .secondary
                    passwordLabel.textColor = .secondary
                }
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            underlineEmail.backgroundColor = .textDescriptionColor
            underlineViewHeightConstraint?.constant = 1
            if textField.text?.isEmpty != false {
                textField.setPlaceholderFont(title: "Email address", font: Font.barlowRegular(size: .mediumSmall))
                emailLabel.textColor = .white
            }
        } else {
            if textField == passwordTextField {
                underlinePassword.backgroundColor = .textDescriptionColor
                underlineViewHeightConstraint?.constant = 1
                if textField.text?.isEmpty != false {
                    textField.setPlaceholderFont(title: "Password (6+ characters)", font: Font.barlowRegular(size: .mediumSmall))
                    passwordLabel.textColor = .white
                }
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            underlineEmail.backgroundColor = .primary
            textField.placeholder = ""
            underlineViewHeightConstraint?.constant = 2
            emailLabel.textColor = .primary
        } else {
            if textField == passwordTextField {
                underlinePassword.backgroundColor = .primary
                textField.placeholder = ""
                underlineViewHeightConstraint?.constant = 2
                passwordLabel.textColor = .primary
            }
        }
    }
}

extension UITextField {
    func setPlaceholderFont(title: String, font: UIFont) {
        self.attributedPlaceholder = NSAttributedString(string: title, attributes: [
            .foregroundColor: UIColor.textDescriptionColor,
            .font: font
            ])
    }
}
