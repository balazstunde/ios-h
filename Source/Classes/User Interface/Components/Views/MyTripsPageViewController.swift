//
//  PageControllerTest.swift
//  Choo Choo
//
//  Created by halcyonmobile on 13/08/2019.
//

import UIKit

protocol MyTripsPageViewControllerDelegate: AnyObject {
    func didTouchPlanLabel()
}

class MyTripsPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    private var canEdit: Bool = true
    private var pages = [UIViewController]()
    private let pageControl = UIPageControl()
    private let upcomingTripsButton = UIButton()
    private let pastTripsButton = UIButton()
    private let upcomingTripsLine = UIView()
    private let pastTripsLine = UIView()
    private var isThisTheFirstPage: Bool = true
    let upcomingTripsPage = UpcomingTripPageViewController()
    let pastTripsPage = PastTripPageViewController()
    weak var flowDelegate: MyTripsPageViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        signUpForNotifications()
    }
    
    private func setupView() {
        self.dataSource = self
        self.delegate = self
        let initialPage = 0
        upcomingTripsPage.delegate = self
        self.pages.append(upcomingTripsPage)
        self.pages.append(pastTripsPage)
        setViewControllers([pages[initialPage]], direction: .forward, animated: false, completion: nil)
        
        self.pageControl.frame = CGRect()
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.numberOfPages = self.pages.count
        self.pageControl.currentPage = initialPage
        self.view.addSubview(self.pageControl)
        
        upcomingTripsButton.backgroundColor = .primary
        upcomingTripsButton.setTitle("Upcoming Trips", for: .normal)
        upcomingTripsButton.titleLabel?.font = Font.barlowBold(size: .medium)
        upcomingTripsButton.isSelected = true
        upcomingTripsButton.setTitleColor(.white, for: .selected)
        upcomingTripsButton.setTitleColor(.gray, for: .normal)
        upcomingTripsLine.backgroundColor = .white
        
        pastTripsButton.backgroundColor = .primary
        pastTripsButton.titleLabel?.font = Font.barlowBold(size: .medium)
        pastTripsButton.setTitle("Past Trips", for: .normal)
        pastTripsButton.isSelected = false
        pastTripsButton.setTitleColor(.white, for: .selected)
        pastTripsButton.setTitleColor(.gray, for: .normal)
        pastTripsLine.backgroundColor = .primary
        
        upcomingTripsButton.addSubview(upcomingTripsLine)
        pastTripsButton.addSubview(pastTripsLine)
        
        pastTripsButton.addTarget(self, action: #selector(didPastTripsButtonPressed), for: .touchUpInside)
        upcomingTripsButton.addTarget(self, action: #selector(didUpcomingTripButtonPressed), for: .touchUpInside)
        view.addSubview(upcomingTripsButton)
        view.addSubview(pastTripsButton)
        
    }
    
    @objc private func didUpcomingTripButtonPressed() {
        if upcomingTripsButton.isTouchInside {
            canEdit = true
            setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
            moveButtons(isFirstPage: true)
        }
    }
    
    @objc private func didPastTripsButtonPressed() {
        if pastTripsButton.isTouchInside {
            canEdit = true
            setViewControllers([pages[1]], direction: .forward, animated: false, completion: nil)
            moveButtons(isFirstPage: false)
        }
    }
    
    func setupConstraints() {
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pastTripsButton.translatesAutoresizingMaskIntoConstraints = false
        upcomingTripsButton.translatesAutoresizingMaskIntoConstraints = false
        upcomingTripsLine.translatesAutoresizingMaskIntoConstraints = false
        pastTripsLine.translatesAutoresizingMaskIntoConstraints = false
        pageControl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -5).isActive = true
        pageControl.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -20).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        NSLayoutConstraint.activate([
            upcomingTripsButton.topAnchor.constraint(equalTo: view.topAnchor),
            upcomingTripsButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            upcomingTripsButton.trailingAnchor.constraint(equalTo: view.centerXAnchor),
            
            pastTripsButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            
            pastTripsButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            pastTripsButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            upcomingTripsLine.topAnchor.constraint(equalTo: upcomingTripsButton.bottomAnchor, constant: -LayoutValues.lineHeight),
            upcomingTripsLine.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Padding.medium),
            upcomingTripsLine.trailingAnchor.constraint(equalTo: pastTripsButton.safeAreaLayoutGuide.leadingAnchor, constant: -Padding.medium),
            upcomingTripsLine.heightAnchor.constraint(equalToConstant: LayoutValues.lineHeight),
            
            pastTripsLine.topAnchor.constraint(equalTo: pastTripsButton.bottomAnchor, constant: -LayoutValues.lineHeight),
            pastTripsLine.leadingAnchor.constraint(equalTo: upcomingTripsLine.trailingAnchor, constant: Padding.medium),
            pastTripsLine.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.medium),
            pastTripsLine.heightAnchor.constraint(equalToConstant: LayoutValues.lineHeight)
            ])
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        canEdit = true
        if let viewControllerIndex = self.pages.firstIndex(of: viewController) {
            moveButtons(isFirstPage: true)
            if viewControllerIndex == 0 {
                return nil
            } else {
                return self.pages[viewControllerIndex - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        canEdit = true
        if let viewControllerIndex = self.pages.firstIndex(of: viewController) {
            moveButtons(isFirstPage: false)
            if viewControllerIndex < self.pages.count - 1 {
                return self.pages[viewControllerIndex + 1]
            } else {
                return nil
            }
        }
        return nil
    }
    
    private func moveButtons(isFirstPage: Bool) {
        if canEdit {
            upcomingTripsButton.isSelected = isFirstPage
            upcomingTripsLine.backgroundColor = isFirstPage ? .white : .primary
            pastTripsButton.isSelected = !isFirstPage
            pastTripsLine.backgroundColor = !isFirstPage ? .white : .primary
            canEdit = false
        }
    }
    
    private func signUpForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLogin), name: .didLogin, object: nil)
    }
    
    @objc private func userDidLogin() {
        DispatchQueue.main.async {
            self.upcomingTripsPage.reloadView()
            self.pastTripsPage.reloadView()
        }
    }
}

private enum LayoutValues {
    static let customTableRowHeight: CGFloat = 150
    static let customTableHeaderHeight: CGFloat = 165
    static let customTableTop: CGFloat = 70
    static let lineHeight: CGFloat = 3
}

extension MyTripsPageViewController: UpcomingTripPageViewControllerDelegate {
    func didTouchPlanLabel() {
        flowDelegate?.didTouchPlanLabel()
    }
}
