//
//  APIConstants.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 08/01/2019.
//

import Foundation

public enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case delete  = "DELETE"
}

public enum PostParamType {
    case body , formData
}

public typealias HTTPHeaders = [String: String]
public typealias JSONDict = [String: Any]

enum API {

#if DEBUG
    static let backendURL = "https://foo.bar/api"
#else
    static let backendURL = "https://foo.bar/api"
#endif
    enum HeaderKeys {
        static let authorization = "authorization"
        static let tokenType = "Bearer"
    }
    
    enum Path {
        static let login                = "/oauth/token"
        static let signup               = "/users"
        static let passwordRecovery     = "/users/password-reset"
        static let password             = "/password"
        static let journeyPricingCategories = "/journeys/pricing-categories"
        static let journeySearch        = "/journeys"
        static let notificationSettings = "/notification-settings"
        static let notifications        = "/notifications"
    }

    enum Param {
        static let id           = "id"
        static let firstName    = "firstName"
        static let email        = "email"
        static let username     = "username"
        static let password     = "password"
        static let grantType    = "grant_type"
        static let refreshToken = "refresh_token"
        static let name         = "firstName"
        static let pushNotificationEnabled      = "pushNotificationEnabled"
        static let oldPassword  = "oldPassword"
        static let newPassword  = "newPassword"
        static let numberOfElements = "numberOfElements"
        static let pageNumber = "pageNumber"
        static let date         = "date"
        static let endStationId = "endStationId"
        static let startStationId = "startStationId"
        static let desiredTickets = "desiredTickets"
        static let pagingInfo   = "pagingInfo"
        static let changedPlatformNotification  = "changedPlatformNotificationEnabled"
        static let marketingNotification        = "marketingNotificationEnabled"
        static let pushNotification             = "pushNotificationEnabled"
        static let trainArrivingNotification    = "trainArrivingIntoStationNotificationEnabled"
        static let tripDelayNotification        = "tripDelayNotificationEnabled"
        static let tripDisruptionNotification   = "tripDisruptionNotificationEnabled"
    }
}
