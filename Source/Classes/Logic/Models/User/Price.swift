//
//  Price.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 09/08/2019.
//

import Foundation

struct Price: Codable {
    let unit: PriceUnitType
    let value: Double
}

enum PriceUnitType: String, Codable {
    case lei = "RON"
}
