//
//  AuthenticationDto.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import Foundation

struct AuthenticationDto: Codable {
    let accessToken: String
    let expiresIn: Int
    let jti: String
    let refreshToken: String
    let scope: String
    let tokenType: String
    let userId: Int
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case jti
        case refreshToken = "refresh_token"
        case scope
        case tokenType = "token_type"
        case userId = "user_id"
    }
}
