//
//  ConfigurePaymentDetailsViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 12/08/2019.
//

import UIKit

protocol ConfigurePaymentViewControllerFlowDelegate: AnyObject {
    func didFailToUpdateCreditCard(on viewController: ConfigurePaymentDetailsViewController)
    func didTouchBackButton(on viewController: ConfigurePaymentDetailsViewController)
    func didSuccessfullyUpdateCard(on viewController: ConfigurePaymentDetailsViewController)
}

enum PaymentType {
    case ticketPayment
    case updatePayment
}

class ConfigurePaymentDetailsViewController: BaseViewController {
    
    private enum LayoutValues {
        static let cornerRadius: CGFloat = 5
        static let mediumBig: CGFloat = 25
        static let big: CGFloat = 30
        static let bigger: CGFloat = 40
        static let reallyBig: CGFloat = 60
        static let buttonHeight: CGFloat = 48
    }
    
    weak var delegate: ConfigurePaymentViewControllerFlowDelegate?
    private let paymentService = PaymentService()
    private let spinner = UIActivityIndicatorView(style: .gray)
    private var currentCreditCard: CreditCard = CreditCard(cvv: "", holderName: "", number: "", validUntil: "")
    private let loadingScreen = LoadingView()
    
    private let expiryDatePicker = MonthYearPickerView()
    private let mainContent = UIView()
    private let saveCardCheckbox = UIImageView()
    private var saveCard: Bool = false
    
    private var nameView = EditView(viewType: .cardHolder)
    private var cardView = EditView(viewType: .card)
    private var dateView = EditView(viewType: .date)
    private var ccvView = EditView(viewType: .ccv)

    private let saveCardLabel = UILabel()
    private let headerLabel = UILabel()

    private let backIcon = UIImageView(image: Asset.icNavigationBack.image)
    
    private var mainStackView = UIStackView()
    private var dateAndCvvStackView = UIStackView()

    private let proceedButton = UIButton()
    
    private let paymentType: PaymentType
    
    init(paymentType: PaymentType) {
        self.paymentType = paymentType
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if KeyStore.shared.currentCreditCard != nil {
            loadingScreen.isHidden = true
            self.currentCreditCard = KeyStore.shared.currentCreditCard ?? CreditCard(cvv: "", holderName: "", number: "", validUntil: "")
            self.setupTextFields()
            self.setupButtons()
            self.loadingScreen.removeFromSuperview()
        } else {
            loadingScreen.isHidden = false
            PaymentService().getCreditCard(userID: KeyStore.shared.userID ?? 0, completion: { card in
                KeyStore.shared.currentCreditCard = card
                self.currentCreditCard = card
                DispatchQueue.main.async {
                    self.setupTextFields()
                    self.setupButtons()
                    self.loadingScreen.removeFromSuperview()
                }
            }, failure: { _ in
                DispatchQueue.main.async {
                    self.loadingScreen.removeFromSuperview()
                }
            })
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        initView()
        initConstraints()
    }
    
    @objc func donedatePicker() {
        self.view.endEditing(true)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func backButtonTapped() {
        delegate?.didTouchBackButton(on: self)
    }
    
    private func initView() {
        setupLabels()
        setupNavBar()
        setupTextFields()
        setupButtons()
        
        nameView.delegate = self
        cardView.delegate = self
        dateView.delegate = self
        ccvView.delegate = self
        view.backgroundColor = .white
        
        dateView.isUserInteractionEnabled = true
        openDatePicker()
        
        mainContent.addSubview(loadingScreen)
        mainContent.addSubview(headerLabel)
        mainContent.addSubview(nameView)
        mainContent.addSubview(cardView)
        mainContent.addSubview(dateView)
        mainContent.addSubview(ccvView)
        mainContent.addSubview(saveCardLabel)
        mainContent.addSubview(saveCardCheckbox)
        mainContent.addSubview(spinner)
        mainContent.addSubview(proceedButton)
        view.addSubview(mainContent)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backIcon.image, style: .plain, target: self, action: #selector(backButtonTapped))
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: Font.barlowSemiBold(size: .medium)]
        navigationItem.title = "Payment details"
    }
    
    private func setupLabels() {
        headerLabel.setLabelDetails(text: "Please fill in the required information", color: .white, font: Font.barlowRegular(size: FontSize.medium))
        headerLabel.backgroundColor = .primary
        headerLabel.textAlignment = .center
        
        switch paymentType {
        case .ticketPayment:
            saveCardLabel.setLabelDetails(text: "Save card for later use", color: .black, font: Font.barlowRegular(size: FontSize.mediumSmall))
        case .updatePayment:
            saveCardLabel.isHidden = true
        }
    }
    
    private func setupTextFields() {
        nameView.text = currentCreditCard.holderName
        cardView.text = currentCreditCard.number
        if currentCreditCard.validUntil != "" {
            dateView.text = currentCreditCard.validUntil.prefix(7).suffix(2) + "/" + currentCreditCard.validUntil.prefix(4).suffix(2)
        }
        ccvView.text = currentCreditCard.cvv
        
        let viewList = [nameView, cardView, dateView, ccvView]
        for oneView in viewList {
            oneView.label.textColor = oneView.text!.isEmpty ? .white : .primary
        }
    }
    
    private func setupButtons() {
        switch paymentType {
        case .ticketPayment:
            proceedButton.setTitle("Proceed to payment", for: .normal)
            proceedButton.addTarget(self, action: #selector(proceedToPaymentButtonTapped), for: .touchUpInside)
            
            saveCardCheckbox.isUserInteractionEnabled = true
            saveCardCheckbox.layer.borderWidth = 1
            saveCardCheckbox.layer.borderColor = UIColor.black.cgColor
            saveCardCheckbox.layer.cornerRadius = LayoutValues.cornerRadius
            saveCardCheckbox.contentMode = .scaleAspectFit
            saveCardCheckbox.image = Asset.checkImage.image
            saveCardCheckbox.tintColor = .white
            saveCardCheckbox.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(saveCardButtonTouched)))
        case .updatePayment:
            proceedButton.setTitle("Save Card", for: .normal)
            proceedButton.addTarget(self, action: #selector(updateCardButtonTapped), for: .touchUpInside)
        }
        
        proceedButton.titleLabel?.font = Font.barlowBold(size: .mediumSmall)
        proceedButton.layer.cornerRadius = LayoutValues.cornerRadius
        if nameView.text!.isEmpty || cardView.text!.isEmpty || dateView.text!.isEmpty || ccvView.text!.isEmpty {
            proceedButton.backgroundColor = .lola
            proceedButton.isEnabled = false
        } else {
            proceedButton.backgroundColor = .primary
            proceedButton.isEnabled = true
        }
    }
    
    private func initConstraints() {
        mainContent.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen.translatesAutoresizingMaskIntoConstraints = false
        proceedButton.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        saveCardLabel.translatesAutoresizingMaskIntoConstraints = false
        saveCardCheckbox.translatesAutoresizingMaskIntoConstraints = false
        spinner.translatesAutoresizingMaskIntoConstraints = false
        nameView.translatesAutoresizingMaskIntoConstraints = false
        cardView.translatesAutoresizingMaskIntoConstraints = false
        dateView.translatesAutoresizingMaskIntoConstraints = false
        ccvView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            mainContent.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainContent.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainContent.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainContent.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            loadingScreen.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            loadingScreen.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            loadingScreen.topAnchor.constraint(equalTo: view.topAnchor),
            loadingScreen.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            headerLabel.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor),
            headerLabel.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor),
            headerLabel.topAnchor.constraint(equalTo: mainContent.topAnchor),
            headerLabel.heightAnchor.constraint(equalToConstant: LayoutValues.reallyBig),
            
            nameView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: LayoutValues.big),
            nameView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.mediumSmall),
            nameView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Padding.mediumSmall)),
            nameView.heightAnchor.constraint(equalToConstant: LayoutValues.reallyBig),
            
            cardView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: Padding.medium),
            cardView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.mediumSmall),
            cardView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Padding.mediumSmall)),
            cardView.heightAnchor.constraint(equalToConstant: LayoutValues.reallyBig),
            
            dateView.topAnchor.constraint(equalTo: cardView.bottomAnchor, constant: Padding.medium),
            dateView.trailingAnchor.constraint(equalTo: mainContent.centerXAnchor),
            dateView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.mediumSmall),
            dateView.heightAnchor.constraint(equalToConstant: LayoutValues.reallyBig),
            
            ccvView.topAnchor.constraint(equalTo: dateView.topAnchor),
            ccvView.leadingAnchor.constraint(equalTo: mainContent.centerXAnchor),
            ccvView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Padding.mediumSmall)),
            ccvView.heightAnchor.constraint(equalToConstant: LayoutValues.reallyBig),

            proceedButton.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.mediumSmall),
            proceedButton.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Padding.mediumSmall)),
            proceedButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -(Padding.mediumSmall)),
            proceedButton.heightAnchor.constraint(equalToConstant: LayoutValues.buttonHeight),
            
            saveCardLabel.topAnchor.constraint(equalTo: dateView.bottomAnchor, constant: LayoutValues.bigger),
            saveCardLabel.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.mediumSmall),
            
            saveCardCheckbox.trailingAnchor.constraint(equalTo: ccvView.trailingAnchor),
            saveCardCheckbox.topAnchor.constraint(equalTo: ccvView.bottomAnchor, constant: LayoutValues.bigger),
            saveCardCheckbox.heightAnchor.constraint(equalToConstant: LayoutValues.mediumBig),
            saveCardCheckbox.widthAnchor.constraint(equalToConstant: LayoutValues.mediumBig),
            
            spinner.topAnchor.constraint(equalTo: saveCardLabel.bottomAnchor, constant: LayoutValues.big),
            spinner.centerXAnchor.constraint(equalTo: mainContent.centerXAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    private func openDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([doneButton,spaceButton], animated: false)
        
        dateView.textField.inputAccessoryView = toolbar
        dateView.textField.inputView = expiryDatePicker
        
        expiryDatePicker.onDateSelected = { [weak self] month, year in
            let date = String(format: "%02d/%d", month, year % 100)
            self?.dateView.text = date
        }
    }
    
    @objc private func proceedToPaymentButtonTapped() {
        spinner.startAnimating()
        if saveCard {
            updateCard()
        } else {
            spinner.stopAnimating()
        }
        //TODO : purchase ticket
    }
    
    private func updateCard() {
        let ccv = ccvView.text ?? ""
        let date = dateView.text ?? ""
        let actualDate = "20" + String(date.suffix(2)) + "-" + String(date.prefix(2)) + "-01" //yyyy-MM-dd
        let name = nameView.text ?? ""
        let cardNr = cardView.text ?? ""
        let card = CreditCard(cvv: ccv, holderName: name, number: cardNr, validUntil: actualDate)
        let id: Int = KeyStore.shared.userID ?? 0
        paymentService.updateCreditCard(creditCard: card, userID: id, completion: {
            if self.paymentType == .updatePayment {
                self.delegate?.didSuccessfullyUpdateCard(on: self)
            }
                
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
            }
        }, failure: { _ in
            self.delegate?.didFailToUpdateCreditCard(on: self)
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
            }
        })
    }
    
    @objc private func updateCardButtonTapped() {
        updateCard()
    }
    
    @objc private func saveCardButtonTouched() {
        let color: UIColor = !saveCard ? .primary : .white
        saveCardCheckbox.tintColor = color
        saveCard = !saveCard
    }
}

extension ConfigurePaymentDetailsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let userEnteredString = textField.text as NSString?,
            let newString = userEnteredString.replacingCharacters(in: range, with: string) as NSString? else { return false }
        if !cardView.text!.isEmpty {
            cardView.isEditAllowed = true
        } else {
            cardView.isEditAllowed = false
        }
        if !cardView.text!.isEmpty && !nameView.text!.isEmpty && !ccvView.text!.isEmpty && newString != "" {
            proceedButton.backgroundColor = .primary
            proceedButton.isEnabled = true
        } else {
            proceedButton.backgroundColor = .lola
            proceedButton.isEnabled = false
        }
        return true
    }
}

extension UILabel {
    func setLabelDetails(text: String, color: UIColor, font: UIFont) {
        self.text = text
        self.textColor = color
        self.font = font
    }
}

extension ConfigurePaymentDetailsViewController: EditViewDelegate {
    func didTouchTextField() {
        guard
            let name = nameView.text, !name.isEmpty,
            let card = cardView.text, !card.isEmpty,
            let date = dateView.text, !date.isEmpty,
            let ccv = ccvView.text, !ccv.isEmpty
            else {
                self.proceedButton.isEnabled = false
                self.proceedButton.backgroundColor = .lola
                
                return
        }
        self.proceedButton.backgroundColor = .primary
        self.proceedButton.isEnabled = true
    }
    
    func didTouchEdit() {
        
    }
}
