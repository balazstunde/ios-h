//
//  NotificationPreferencesHeader.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

class NotificationPreferencesHeaderView: UITableViewHeaderFooterView {
    private let nameLabel = UILabel()
    private let separatorView = UIView()
    
    var sectionName: String? {
        didSet {
            nameLabel.text = sectionName
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupView()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        contentView.backgroundColor = .white
        nameLabel.font = Font.barlowBold(size: .medium)
        separatorView.backgroundColor = .separatorColor
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(separatorView)
    }
    
    private func setUpConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Padding.medium),
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Padding.medium),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Padding.medium),
            separatorView.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
            separatorView.heightAnchor.constraint(equalToConstant: 1)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
