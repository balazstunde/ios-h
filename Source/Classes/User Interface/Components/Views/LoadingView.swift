//
//  LoadingView.swift
//  Choo Choo
//
//  Created by Halcyon on 20/08/2019.
//

import UIKit

class LoadingView: UIView {
    private let spinner = UIActivityIndicatorView()
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        self.backgroundColor = .gray
        self.alpha = 0.2
        spinner.startAnimating()
        self.addSubview(spinner)
    }
    
    func setupConstraints() {
        spinner.translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}
