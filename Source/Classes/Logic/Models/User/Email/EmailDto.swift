//
//  EmailDto.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation

class EmailDto: Codable {
    let email: String
    
    init(email: String) {
        self.email = email
    }
}
