//
//  TravellingClass.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 07/08/2019.
//

import Foundation

enum TravellingClass: String, Codable {
    case first = "1st"
    case second = "2nd"
    case coach = "Coach"
}
