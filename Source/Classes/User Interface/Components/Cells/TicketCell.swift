import UIKit

protocol TicketCellDelegate: AnyObject {
    func didSelectReminderButton(notificationData: NotificationData, isSelected: Bool, on cell: UITableViewCell)
}

class TicketCell: UITableViewCell {
    
    weak var delegate: TicketCellDelegate?
    
    private let departureStackView = UIStackView()
    private let arrivalStackView = UIStackView()
    private let detailsStackView = UIStackView()
    private let reminderStackView = UIStackView()
    private var bellImage = UIImage()
    private let bellButton = UIButton()
    private let dateLabel = UILabel()
    private let typeLabel = UILabel()
    private let departureTimeLabel = UILabel()
    private let departureCityLabel = UILabel()
    private let reminderLabel = UILabel()
    private let arrivalTimeLabel = UILabel()
    private let arrivalCityLabel = UILabel()
    private let trainView = DottedLineView()
    private let hourStackView = UIStackView()
//    static var futureNotifications = [NotificationData]()
    private var reminderSelected = false

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstriants()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        bellButton.setImage(Asset.iconNofication.image, for: .normal)
        bellButton.backgroundColor = .softPeach
        bellButton.layer.cornerRadius = 5
        bellButton.addTarget(self, action: #selector(setReminderButtonTouched), for: .touchUpInside)
        bellImage = bellImage.withRenderingMode(.alwaysTemplate)
        bellButton.tintColor = .secondary
        
        dateLabel.textColor = .secondary

        hourStackView.axis = .horizontal
        hourStackView.alignment = .center
        hourStackView.distribution = .equalCentering

        reminderStackView.addArrangedSubview(reminderLabel)
        reminderStackView.addArrangedSubview(bellButton)
        reminderStackView.axis = .horizontal
        reminderStackView.spacing = 10
        
        detailsStackView.addArrangedSubview(dateLabel)
        detailsStackView.addArrangedSubview(typeLabel)
        detailsStackView.axis = .vertical
        
        departureStackView.addArrangedSubview(departureTimeLabel)
        departureStackView.addArrangedSubview(departureCityLabel)
        departureStackView.axis = .vertical
        
        arrivalStackView.addArrangedSubview(arrivalTimeLabel)
        arrivalStackView.addArrangedSubview(arrivalCityLabel)
        arrivalStackView.axis = .vertical
        arrivalStackView.alignment = .trailing

        hourStackView.addArrangedSubview(departureStackView)
        hourStackView.addArrangedSubview(trainView)
        hourStackView.addArrangedSubview(arrivalStackView)

        dateLabel.font = Font.barlowBold(size: .medium)
        departureTimeLabel.font = Font.barlowBold(size: .mediumExtra)
        departureCityLabel.font = Font.barlowRegular(size: .medium)
        reminderLabel.font = Font.barlowRegular(size: .mediumSmall)
        arrivalTimeLabel.font = Font.barlowBold(size: .mediumExtra)
        arrivalCityLabel.font = Font.barlowRegular(size: .medium)

        contentView.addSubview(hourStackView)
        contentView.addSubview(detailsStackView)
        contentView.addSubview(reminderStackView)
    }
    
    private func setupConstriants() {
        detailsStackView.translatesAutoresizingMaskIntoConstraints = false
        reminderStackView.translatesAutoresizingMaskIntoConstraints = false
        hourStackView.translatesAutoresizingMaskIntoConstraints = false

        var constraints: [NSLayoutConstraint] = []
        constraints.append(detailsStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: AppStyle.Layout.cellIconPadding))
        constraints.append(detailsStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutConstants.customMargin))
        constraints.append(reminderStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: LayoutConstants.customTop))
        constraints.append(reminderStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutConstants.customTop))
        constraints.append(hourStackView.topAnchor.constraint(equalTo: detailsStackView.bottomAnchor, constant: 25))
        constraints.append(hourStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -LayoutConstants.customMargin))
        constraints.append(hourStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutConstants.customMargin))
        constraints.append(hourStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutConstants.customMargin))

        NSLayoutConstraint.activate(constraints)
    }
    
    func configure(_ tickets: Ticket) {
        dateLabel.text = tickets.date
        typeLabel.text = tickets.pricingCategory.title
        departureCityLabel.text = tickets.startTrainStopDto.stopName
        departureTimeLabel.text = tickets.startTrainStopDto.departureTime.getHourMinutes()
        typeLabel.font = Font.barlowRegular(size: .medium)
        reminderLabel.text = "Set reminder"
        arrivalTimeLabel.text = tickets.endTrainStopDto.arrivalTime.getHourMinutes()
        arrivalCityLabel.text = tickets.endTrainStopDto.stopName
        trainView.setTrainNo = tickets.tripIdentifier
    }
    
    @objc private func setReminderButtonTouched() {
        reminderSelected = !reminderSelected
        
        if let startDate = dateLabel.text, let startTime = departureTimeLabel.text, let endTime = arrivalTimeLabel.text, let departure = departureCityLabel.text, let destination = arrivalCityLabel.text {
            let notificationData = NotificationData(startDate: startDate, startTime: startTime, endTime: endTime,
                                                    departure: departure, destination: destination)
            delegate?.didSelectReminderButton(notificationData: notificationData, isSelected: reminderSelected, on: self)
        }
    
        if reminderSelected {
            bellButton.backgroundColor = .secondary
            bellButton.tintColor = .softPeach
        } else {
            bellButton.backgroundColor = .softPeach
            bellButton.tintColor = .secondary
        }
    }
}

extension TicketCell {
    private enum LayoutConstants {
        static let topAnchor: CGFloat = 80
        static let customLeading: CGFloat = 160
        static let customTop: CGFloat = 24
        static let customMargin: CGFloat = 32
        static let customSpace: CGFloat = 50
        static let hourStackTop: CGFloat = 64

    }
}
