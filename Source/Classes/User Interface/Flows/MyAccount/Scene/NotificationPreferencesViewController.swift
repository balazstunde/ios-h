//
//  NotificationPreferencesViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

class SwitchButtonWithRowAndSection : UISwitch {
    var row : Int?
    var section : Int?
}

protocol NotificationPreferencesViewControllerDelegate: AnyObject {
    func close()
    func showAlert(on viewController: NotificationPreferencesViewController)
}

class NotificationPreferencesViewController: UIViewController {
    
    private let notificationCellIdentifier = "NotificationPrefCell"
    private let notificationHeaderIdentifier = "NotificationPrefHeader"
    private let tableView = UITableView()
    private var notificationPreferences: NotificationPreferences?
    private let notificationService = NotificationService()
    private var notificationPreferencesRowSections: [NotificationPreferencesSection] = []
    private let keyStore = KeyStore.shared
    weak var delegate: NotificationPreferencesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setUpConstraints()
        setupLogic()
    }
    
    private func setupLogic() {
        notificationPreferences = NotificationPreferences()
        guard let id = keyStore.userID else { return }
        notificationService.getNotifications(userId: id, completion: {[weak self] notifications in
            DispatchQueue.main.async {
                self?.notificationPreferences = notifications
                self?.tableView.reloadData()}
        }, failure: { [weak self] error in
                DispatchQueue.main.async {
                    self?.delegate?.showAlert(on: self ?? NotificationPreferencesViewController())
                }})
    }
    
    private func setupView() {
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain, target: self, action: #selector(close))
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: Font.barlowSemiBold(size: .mediumLarge)]
        navigationItem.title = "Notifications"
        
        tableView.rowHeight = 50
        tableView.sectionHeaderHeight = 72
        tableView.separatorStyle = .none
        tableView.register(NotificationPreferencesCell.self, forCellReuseIdentifier: notificationCellIdentifier)
        tableView.register(NotificationPreferencesHeaderView.self, forHeaderFooterViewReuseIdentifier: notificationHeaderIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        notificationPreferencesRowSections = NotificationPreferencesDataSource.createSectionData()
    }
    
    private func setUpConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func close() {
        delegate?.close()
    }
}

extension NotificationPreferencesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: notificationHeaderIdentifier) as? NotificationPreferencesHeaderView else {
            fatalError("Could not dequeue NotificationPreferencesHeaderView")
        }
        header.sectionName = notificationPreferencesRowSections[section].sectionTitle
        return header
    }
}

extension NotificationPreferencesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch notificationPreferencesRowSections[section] {
        case .general(let rows): return rows.count
        case .notify(let rows): return rows.count
        case .discounts(let rows): return rows.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: notificationCellIdentifier, for: indexPath) as? NotificationPreferencesCell else {
            fatalError("Could not dequeue NotificationPreferencesCell")
        }
        switch notificationPreferencesRowSections[indexPath.section] {
        case .general(let rows), .notify(let rows), .discounts(let rows):
            cell.configureCell(rows[indexPath.row])
            let switchButton = SwitchButtonWithRowAndSection()
            switchButton.backgroundColor = .switchButtonOffColor
            switchButton.layer.cornerRadius = 16.0
            switchButton.onTintColor = .switchButtonColor
            switchButton.tintColor = .switchButtonOffColor
            switchButton.row = indexPath.row
            switchButton.section = indexPath.section
            
            switch rows[indexPath.row] {
            case "Receive push notifications": switchButton.isOn = notificationPreferences?.pushNotificationEnabled ?? false
            case "There is a  trip disruption": switchButton.isOn = notificationPreferences?.tripDisruptionNotificationEnabled ?? false
            case "There is a  trip delay": switchButton.isOn = notificationPreferences?.tripDelayNotificationEnabled ?? false
            case "Train has arrived in station": switchButton.isOn = notificationPreferences?.trainArrivingIntoStationNotificationEnabled ?? false
            case "Platform has changed": switchButton.isOn = notificationPreferences?.changedPlatformNotificationEnabled ?? false
            case "Get discounts and promotional info": switchButton.isOn = notificationPreferences?.marketingNotificationEnabled ?? false
            default: switchButton.isOn = false
            }
            
            // if the first is in off mode then disable all (the first remain enable)
            if !(notificationPreferences?.pushNotificationEnabled ?? false) && !(switchButton.row == 0 && switchButton.section == 0) {
                switchButton.isEnabled = false
            }
            
            switchButton.thumbTintColor = switchButton.isOn ? .shiraz : .switchButtonColor
            switchButton.addTarget(self, action: #selector(manageTheValueChanges), for: .valueChanged)
            cell.accessoryView = switchButton
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.isSelected = false
    }
    
    @objc private func manageTheValueChanges(button: SwitchButtonWithRowAndSection) {
        // change the color
        if button.isOn {
            // if the first notification mode became to on mode then enable all notifications
            if button.row == 0 && button.section == 0 {
                enableAll()
            }
            button.thumbTintColor = .shiraz
        } else {
            // if the first notification mode became to off mode then disable all other notifications
            if button.row == 0 && button.section == 0 {
                disableAll()
            }
            button.thumbTintColor = .switchButtonColor
        }
        
        switch (button.section, button.row) {
        case (0,0):
            notificationPreferences?.pushNotificationEnabled = button.isOn
        case (1,0):
            notificationPreferences?.tripDisruptionNotificationEnabled = button.isOn
        case (1,1):
            notificationPreferences?.tripDelayNotificationEnabled = button.isOn
        case (1,2):
            notificationPreferences?.trainArrivingIntoStationNotificationEnabled = button.isOn
        case (1,3):
            notificationPreferences?.changedPlatformNotificationEnabled = button.isOn
        case (2,0):
            notificationPreferences?.marketingNotificationEnabled = button.isOn
        default:
            return
        }
        
        guard let id = keyStore.userID else { return }
        notificationService.updateNotificationSettings(userId: id, notificationPreferences: notificationPreferences! ,completion: {
        }, failure: { _ in
            DispatchQueue.main.async {
                self.delegate?.showAlert(on: self)
            }})
    }
    
    func disableAll() {
        for section in 1...tableView.numberOfSections - 1 {
            for row in 0...tableView.numberOfRows(inSection: section) - 1 {
                if let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) {
                    guard let accessoryView=cell.accessoryView as? SwitchButtonWithRowAndSection else {
                        return
                    }
                    accessoryView.isEnabled = false
                    accessoryView.isOn = false
                    accessoryView.thumbTintColor = .switchButtonColor
                    disableNotifications()
                }
            }
        }
    }
    
    private func disableNotifications() {
        notificationPreferences?.pushNotificationEnabled = false
        notificationPreferences?.tripDisruptionNotificationEnabled = false
        notificationPreferences?.tripDelayNotificationEnabled = false
        notificationPreferences?.trainArrivingIntoStationNotificationEnabled = false
        notificationPreferences?.changedPlatformNotificationEnabled = false
        notificationPreferences?.marketingNotificationEnabled = false
    }
    
    func enableAll() {
        for section in 1...tableView.numberOfSections - 1 {
            for row in 0...tableView.numberOfRows(inSection: section) - 1 {
                if let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) {
                    guard let accessoryView=cell.accessoryView as? SwitchButtonWithRowAndSection else {
                        return
                    }
                    accessoryView.isEnabled = true
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return notificationPreferencesRowSections.count
    }
}
