//
//  NotLoggedInViewController.swift
//  Choo Choo
//
//  Created by halcyonmobile on 19/08/2019.
//

import UIKit

// change name remove controller
class NotLoggedInViewController: UIView {

    private let imageView = UIImageView()
    private let notLoggedInLabel  = UILabel()

    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        imageView.image = Asset.icRestrictedAuthentication.image
        notLoggedInLabel.text = "Not logged in"
        addSubview(imageView)
        addSubview(notLoggedInLabel)
    }

    func setupConstraints() {

        imageView.translatesAutoresizingMaskIntoConstraints = false
        notLoggedInLabel.translatesAutoresizingMaskIntoConstraints = false

        var constraints: [NSLayoutConstraint] = []

        constraints.append(imageView.centerXAnchor.constraint(equalTo: centerXAnchor))
        constraints.append(imageView.topAnchor.constraint(equalTo: topAnchor, constant: LayoutConstants.customTop))
        constraints.append(imageView.widthAnchor.constraint(equalToConstant: LayoutConstants.customWidth))
        constraints.append(imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor))
        constraints.append(notLoggedInLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: LayoutConstants.space))
        constraints.append(notLoggedInLabel.centerXAnchor.constraint(equalTo: centerXAnchor))
        NSLayoutConstraint.activate(constraints)
    }
}

private enum LayoutConstants {
    static let customTop: CGFloat = 120
    static let customWidth: CGFloat = 170
    static let space: CGFloat = 27
}
