String cron_string = BRANCH_NAME == "develop" ? "@midnight" : ""

def isTriggeredByTimer = currentBuild.rawBuild.getCause(hudson.triggers.TimerTrigger$TimerTriggerCause) != null
def isTriggeredManually = currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause) != null

pipeline {
  agent { label 'xcode10' }
  // Enables nightly build trigger.
  triggers { cron(cron_string) }

  stages {

    stage('Bundler') {
      steps {
        sh 'Bundler'
      }
    }

    stage('Dependencies') {
      steps {
        sh 'bundle exec fastlane dependencies'
      }
    }

    stage('Tests') {
      steps {
        sh 'bundle exec fastlane test'
      }
    }

    // Send code analysis to SonarQube
    stage('Static Analysis') {
      steps {
        sh 'bundle exec fastlane metrics'
      }
    }

    // Time based build distribution.
    // Triggered by timer @ midnight. See at the top.
    //
    // Sends staging [Fabric] builds.
    stage('Deploy: nightly') {
      when { expression { isTriggeredByTimer } }
      environment {
        CRASHLYTICS_API_TOKEN = credentials('CRASHLYTICS_API_TOKEN')
        CRASHLYTICS_BUILD_SECRET = credentials('CRASHLYTICS_BUILD_SECRET')
        MATCH_PASSWORD = credentials('MATCH_PASSWORD')
        KEYCHAIN_PASSWORD = credentials('KEYCHAIN_PASSWORD')
      }
      steps {
        sh 'bundle exec fastlane distribute_staging nightly:true'
      }
    }

    // Manually triggered build distribution.
    // Can be triggered from develop.
    //
    // Sends staging [Fabric] builds.
    stage('Deploy: staging') {
      when { expression { isTriggeredManually && BRANCH_NAME ==~ /^(develop)$/ } }
      environment {
        CRASHLYTICS_API_TOKEN = credentials('CRASHLYTICS_API_TOKEN')
        CRASHLYTICS_BUILD_SECRET = credentials('CRASHLYTICS_BUILD_SECRET')
        MATCH_PASSWORD = credentials('MATCH_PASSWORD')
        KEYCHAIN_PASSWORD = credentials('KEYCHAIN_PASSWORD')
      }
      steps {
        sh 'bundle exec fastlane distribute_staging'
      }
    }

    // Manually triggered build distribution.
    // Can be triggered from master, develop and release* 
    //
    // Sends either staging [Fabric] or production [iTunesConnect] builds.
    stage('Deploy: production') {
      // TODO: Use user input confirmation for distribution?
      // It shows a yes / no choice question before proceeding. 
      // https://stackoverflow.com/questions/37831386/jenkins-pipeline-input-step-blocks-executor
      when { expression { isTriggeredManually && BRANCH_NAME ==~ /^(master|^release.*)$/ } }
      environment {
        MATCH_PASSWORD = credentials('MATCH_PASSWORD')
      }
      steps {
        sh 'bundle exec fastlane distribute_production'
      }
    }
  }
}