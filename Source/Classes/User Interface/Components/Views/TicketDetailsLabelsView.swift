//
//  TicketDetailsLabelsView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 06/08/2019.
//

import UIKit

class TicketDetailsLabelsView: UIView {
    private let titleLabel = UILabel()
    private let textLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(title: String, text: String) {
        titleLabel.text = title
        textLabel.text = text
    }
    
    private func setupView() {
        addSubview(titleLabel)
        addSubview(textLabel)
        
        titleLabel.textColor = .textDescriptionColor
        titleLabel.font = Font.barlowRegular(size: .mediumSmall)
        textLabel.font = Font.barlowSemiBold(size: .small)
    }
    
    private func setupConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            
            textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            textLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
