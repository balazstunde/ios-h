//
//  AuthRequests.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 08/01/2019.
//

import Foundation

// MARK: - Log in

extension Request.Auth {
    
    struct Login: RequestRepresentable {
        
        let password: String
        let username: String
        
        let suffix: String = API.Path.login + "?client_id=cfr-33c2-11e9-b260-d663bd823d93"
        
        let method: HTTPMethod = .post
        
        let paramType: PostParamType = .formData
        
        var parameters: Parameters? {
            return  [
                API.Param.grantType: GrantType.password.rawValue,
                API.Param.password: password,
                API.Param.username: username
            ]
        }
    }
    
    struct SignUpRequest: RequestRepresentable {
        let paramType: PostParamType = .body
        let user: NewUserAccount

        let suffix: String = API.Path.signup + "?clientId=cfr-33c2-11e9-b260-d663bd823d93"
        let method: HTTPMethod = .post
        var parameters: Parameters? {
            return [
                API.Param.email: user.email,
                API.Param.firstName: user.firstName,
                API.Param.password: user.password,
                API.Param.pushNotificationEnabled: user.pushNotificationEnabled
            ]
        }
    }
    
    struct GetUserInfoRequest: RequestRepresentable {
        let paramType: PostParamType = .body
        var suffix: String
        var method: HTTPMethod = .get
        var parameters: Parameters?
        init(userId: Int) {
            suffix = API.Path.signup + "/\(userId)/information"
        }
    }
    
    struct UpdateInformationRequest: RequestRepresentable {
        let paramType: PostParamType = .body
        
        var suffix: String
        var method: HTTPMethod = .put
        
        var parameters: Parameters?
        
        init(userInfo: UserInformations, userId: Int) {
            suffix = "/users/\(userId)/information"
            parameters = [
                "email": userInfo.email,
                "firstName": userInfo.firstName
            ]
        }
    }
    
    struct ResetPasswordTokenRequest: RequestRepresentable {
        var suffix: String = API.Path.passwordRecovery + "?clientId=cfr-33c2-11e9-b260-d663bd823d93"
        
        var method: HTTPMethod = .put
        
        let paramType: PostParamType = .body
        
        var parameters: Parameters?
        
        init(email: String) {
            self.parameters = ["email": email]
        }
    }
    
    struct UpdatePasswordRequest: RequestRepresentable {
        var suffix: String
        
        var method: HTTPMethod = .put
        
        let paramType: PostParamType = .body
        
        var parameters: Parameters?
        
        init(userId: Int, updatePassword: UpdatePasswordDto) {
            suffix = API.Path.signup + "/\(userId)" + API.Path.password
            self.parameters = [API.Param.newPassword: updatePassword.newPassword,
                               API.Param.oldPassword: updatePassword.oldPassword]
        }
    }
    
    struct PostJourneySearchRequest: RequestRepresentable {
        let paramType: PostParamType = .body
        var suffix: String = API.Path.journeySearch
        
        var method: HTTPMethod = .post
        
        var parameters: Parameters?
        
        init(journey: JourneySearch) {
            self.parameters = [
                API.Param.date : journey.date,
                API.Param.endStationId : journey.endStationId,
                API.Param.startStationId : journey.startStationId,
                API.Param.desiredTickets : journey.desiredTickets,
                API.Param.pagingInfo : journey.pagingInfo.convertToDictionary()
            ]
        }
    }
}
