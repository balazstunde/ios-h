//
//  SeeAllStopsView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

protocol SeeAllStopsViewDelegate: AnyObject {
    func didCollapseSection(tableViewHeight: CGFloat)
}

class SeeAllStopsView: UIView {
    weak var delegate: SeeAllStopsViewDelegate?
    
    private let shadowView = UIView()
    private let tableView = UITableView()
    private let tableViewHeader = SelectedFareDetailsTopAndBottomView(type: .header)
    private let tableViewFooter = SelectedFareDetailsTopAndBottomView(type: .footer)
    private var tableViewHeightConstraint: NSLayoutConstraint?
    private var buttonView: SelectedFareDetailsCollapseButtonView?
    private var journey: Journey
    
    init(withFare journey: Journey) {
        self.journey = journey
        buttonView = SelectedFareDetailsCollapseButtonView(numberOfStops: journey.stops.count)
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        shadowView.layer.shadowPath = UIBezierPath(rect: shadowView.bounds).cgPath
    }
    
    private func setupView() {
        addSubview(shadowView)
        addSubview(tableView)
        addSubview(tableViewHeader)
        addSubview(tableViewFooter)
        addSubview(buttonView!)
        
        shadowView.backgroundColor = .white
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOpacity = Style.shadowOpacity
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
        shadowView.layer.shadowRadius = Style.shadowRadius
        shadowView.layer.shouldRasterize = true
        
        buttonView?.delegate = self
        tableViewFooter.configure(time: journey.arrivalStation.arrivalTime, city: journey.arrivalStation.stopName, platform: journey.arrivalStation.platformNumber ?? 0)
        tableViewHeader.configure(time: journey.departureStation.departureTime, city: journey.departureStation.stopName, platform: journey.departureStation.platformNumber ?? 0)
        
        tableView.register(FareOverviewStationsCell.self, forCellReuseIdentifier: FareOverviewStationsCell.cellIdentifier)
        tableView.allowsSelection = false
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.rowHeight = Style.rowHeight
    }
    
    private func setupConstraints() {
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeader.translatesAutoresizingMaskIntoConstraints = false
        tableViewFooter.translatesAutoresizingMaskIntoConstraints = false
        buttonView?.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        
        let constraints = [
            tableViewHeader.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableViewHeader.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableViewHeader.topAnchor.constraint(equalTo: topAnchor, constant: AppStyle.Layout.padding),
            
            buttonView!.topAnchor.constraint(equalTo: tableViewHeader.bottomAnchor),
            buttonView!.leadingAnchor.constraint(equalTo: leadingAnchor),
            buttonView!.trailingAnchor.constraint(equalTo: trailingAnchor),
            buttonView!.heightAnchor.constraint(equalToConstant: Layout.buttonViewHeight),
            
            tableView.topAnchor.constraint(equalTo: buttonView!.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.tableViewPadding),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableViewHeightConstraint!,
            
            tableViewFooter.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            tableViewFooter.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableViewFooter.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            shadowView.leadingAnchor.constraint(equalTo: leadingAnchor),
            shadowView.topAnchor.constraint(equalTo: topAnchor),
            shadowView.trailingAnchor.constraint(equalTo: trailingAnchor),
            shadowView.bottomAnchor.constraint(equalTo: tableViewFooter.bottomAnchor, constant: AppStyle.Layout.padding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension SeeAllStopsView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return journey.stops.count - 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FareOverviewStationsCell.cellIdentifier, for: indexPath) as? FareOverviewStationsCell else {
            assertionFailure("Error dequeue-ing cell!")
            return UITableViewCell()
        }
        cell.configure(time: journey.stops[indexPath.row + 1].arrivalTime, stopName: journey.stops[indexPath.row + 1].stopName)
        return cell
    }
}

extension SeeAllStopsView: SelectedFareDetailsCollapseButtonViewDelegate {
    func didPressCollapseButton(hideTable: Bool) {
        let oldPath = CGPath(rect: shadowView.bounds, transform: nil)
        
        self.tableViewHeightConstraint?.constant = hideTable ? 0 : CGFloat(journey.stops.count - 2) * tableView.rowHeight
        UIView.animate(withDuration: AnimationDuration.normal, animations: {
            self.layoutIfNeeded()
        }, completion: { _ in
            self.shadowView.layer.shadowPath = CGPath(rect: self.shadowView.bounds, transform: nil)
        })
        
        let newPath = CGPath(rect: shadowView.bounds, transform: nil)
        let shadowAnimation = CABasicAnimation(keyPath: "shadowPath")
        shadowAnimation.duration = AnimationDuration.normal
        shadowAnimation.fromValue = oldPath
        shadowAnimation.toValue = newPath
        shadowAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.shadowView.layer.add(shadowAnimation, forKey: nil)
        delegate?.didCollapseSection(tableViewHeight: self.tableViewHeightConstraint?.constant ?? 0)
    }
}

extension SeeAllStopsView {
    private enum Layout {
        static let buttonViewHeight: CGFloat = 60
        static let tableViewPadding: CGFloat = 10
    }
    
    private enum Style {
        static let shadowOpacity: Float = 0.7
        static let shadowRadius: CGFloat = 10
        static let rowHeight: CGFloat = 30
    }
}
