//
//  NotificationService.swift
//  Choo Choo
//
//  Created by Halcyon on 21/08/2019.
//

import Foundation

class NotificationService {
    func updateNotificationSettings(userId: Int, notificationPreferences: NotificationPreferences, completion: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.NotificationRequest.PutNotificationRequest(
                userId: userId,
                changedPlatform: notificationPreferences.changedPlatformNotificationEnabled,
                marketing: notificationPreferences.marketingNotificationEnabled,
                push: notificationPreferences.pushNotificationEnabled,
                trainArriving: notificationPreferences.trainArrivingIntoStationNotificationEnabled,
                tripDelay: notificationPreferences.tripDelayNotificationEnabled,
                tripDisruption: notificationPreferences.tripDisruptionNotificationEnabled),
                success: { _ in completion() }, failure: failure)
    }
    
    func getNotifications(userId: Int, completion: @escaping (NotificationPreferences) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.NotificationRequest.GetNotificationRequest(userId: userId), success: { (notifications: NotificationPreferences) in
            completion(notifications)
        }, failure: failure)
    }
}
