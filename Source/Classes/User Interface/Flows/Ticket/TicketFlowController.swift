//
//  TicketFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

class TicketFlowController: NavigationFlowController {
    
    override var firstScreen: UIViewController {
        let ticketViewController = MyTripsPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        ticketViewController.flowDelegate = self
        return ticketViewController
    }
}

extension TicketFlowController: MyTripsPageViewControllerDelegate {
    func didTouchPlanLabel() {
        navigationController.tabBarController?.selectedIndex = 0
    }
}
