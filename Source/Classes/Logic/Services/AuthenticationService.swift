//
//  AuthenticationService.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 06/08/2019.
//

import Foundation

enum GrantType: String, Codable {
    case password = "password"
    case refreshToken = "refresh_token"
}

class AuthenticationService {
    
    private let keyStore = KeyStore.shared
    
    func createNewAccount(newUser: NewUserAccount, completion: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.Auth.SignUpRequest(user: newUser), success: { (auth: Authentication) in
            KeyStore.shared.accessToken = auth.accessToken
            KeyStore.shared.userID = auth.userId
            self.getUserInfo(userId: auth.userId, completion: {(userInfo: UserInformations) in
            }, failure: { error in
                failure(error)
            })
        }, failure: { error in
            failure(error)
        })
    }
    
    func getUserInfo(userId: Int, completion: @escaping (UserInformations) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.Auth.GetUserInfoRequest(userId: userId), success: { (userInfo: UserInformations) in
            self.keyStore.currentUser = userInfo
            completion(userInfo)
        }, failure: { error in
            failure(error)
        })
    }
    
    func logIn(userEmail: String, userPassword: String, completion: @escaping(AuthenticationDto) -> Void, failure: @escaping (Error?) -> Void) {
        let request = Request.Auth.Login(password: userPassword, username: userEmail)
        
        BackendManager.execute(request: request, success: { (auth: AuthenticationDto) in
            KeyStore.shared.accessToken = auth.accessToken
            KeyStore.shared.userID = auth.userId
            self.getUserInfo(userId: auth.userId, completion: {(userInfo: UserInformations) in
            }, failure: { error in
                failure(error)
            })
            completion(auth)
        }, failure: { error in
            failure(error)
        })
    }
    
    func updateUser(userInfo: UserInformations, userId: Int, completion: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.Auth.UpdateInformationRequest(userInfo: userInfo, userId: userId),
         success: { _ in
           completion()
        }, failure: { error in
            failure(error)
        })
    }
    
    func recoverPassword(email: EmailDto, completion: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        let request = Request.Auth.ResetPasswordTokenRequest(email: email.email)
        BackendManager.execute(request: request, success: { _ in completion()}, failure: { error in failure(error)
        })
    }
    
    func logOut() {
        keyStore.removeUserData()
    }
    
    func changePassword(userId: Int, updatePassword: UpdatePasswordDto, completion: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.Auth.UpdatePasswordRequest(userId: userId, updatePassword: updatePassword), success: {_ in
           completion()
        }, failure: { error in
            failure(error)
        })
    }
}
