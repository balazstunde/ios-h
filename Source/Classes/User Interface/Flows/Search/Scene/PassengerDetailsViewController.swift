//
//  PassengerDetailsViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

protocol PassengerDetailViewControllerDelegate: AnyObject {
    func didTapConfirm(on viewController: PassengerDetailsViewController)
    func didTapOutOfDetails(on viewController: PassengerDetailsViewController)
}

class PassengerDetailsViewController: BaseViewController {
    weak var delegate: PassengerDetailViewControllerDelegate?
    
    private lazy var backdropView: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return bdView
    }()
    private let keyStore = KeyStore.shared
    private let mainView = UIView()
    private let tableView = UITableView()
    private var fareCategories: [FareCategory] = []
    private var fareTypes: [FareType] = [.standard, .free, .discounted]
    private let confirmButton = UIButton()
    private var isPresenting = false
    private var choosedDetails:[String:Int] = [:]
    private var passengersDetailsService = BookingService()
    
    init(passengersDetailsService: BookingService) {
        self.passengersDetailsService = passengersDetailsService
        super.init()
        modalPresentationStyle = .custom
        transitioningDelegate = self
        
    }
    func loadData() {
            passengersDetailsService.getFares(completion: { (fares: [FareCategory]) in
                self.fareCategories = fares
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }, failure: { error in
                DispatchQueue.main.async {
                let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Click", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        loadData()
        choosedDetails  = keyStore.fareCategoriesMap ?? [:]
        
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            mainView.heightAnchor.constraint(equalToConstant: self.view.layer.bounds.height * 0.7),
            mainView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            mainView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            mainView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            
            tableView.topAnchor.constraint(equalTo: mainView.topAnchor,constant: 40),
            tableView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 0),
            
            confirmButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0),
            confirmButton.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
            confirmButton.trailingAnchor.constraint(equalTo: mainView.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            confirmButton.leadingAnchor.constraint(equalTo: mainView.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            confirmButton.heightAnchor.constraint(equalToConstant: 48),
            confirmButton.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -16)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupView() {
        view.backgroundColor = .clear
        view.addSubview(backdropView)
        mainView.layer.cornerRadius = 10
        mainView.backgroundColor = .white
        confirmButton.setTitle("Confirm", for: .normal)
        confirmButton.backgroundColor = UIColor(red: 176/255, green: 8/255, blue: 58/255, alpha: 1)
        confirmButton.titleLabel?.font = Font.bold(size: FontSize.medium)
        confirmButton.layer.cornerRadius = 5
        confirmButton.addTarget(self, action: #selector(confirmButtonDidTouch), for: .touchUpInside)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        backdropView.addGestureRecognizer(tapGesture)
        
        view.addSubview(mainView)
        
        mainView.addSubview(confirmButton)
        mainView.addSubview(tableView)
        
        mainView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        
        tableView.register(PassengerDetailsCell.self, forCellReuseIdentifier: "PassengerDetailsCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc private func handleTap(_ sender: UITapGestureRecognizer) {
        self.delegate?.didTapOutOfDetails(on: self)
    }
    
    private func fareCategoriesFor(fareType: FareType) -> [FareCategory] {
        return fareCategories.filter { $0.type == fareType }
    }
    
    @objc private func confirmButtonDidTouch() {
        let sections = tableView.numberOfSections
        for sect in 0..<sections {
            for row in 0...tableView.numberOfRows(inSection: sect) - 1 {
                guard let cell = tableView.cellForRow(at: NSIndexPath(row: row, section: sect) as IndexPath) as? PassengerDetailsCell,
                    let celName = cell.getName(), let data = cell.getData(), let number = Int(data) else { return }
                choosedDetails[celName] = Int(number)
                passengersDetailsService.setDetails(passangersDetails: choosedDetails)
            }
        }
        delegate?.didTapConfirm(on: self)
    }
}

extension PassengerDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PassengerDetailsCell", for: indexPath) as? PassengerDetailsCell else {
            fatalError("Could not dequeue PassengerDetailsCell")
        }
        
        let type = fareTypes[indexPath.section]
        let fareCat = fareCategoriesFor(fareType: type)[indexPath.row]
        cell.configureWithFare(fareCat)
        cell.setNumber(number: choosedDetails[fareCat.title] ?? 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let type = fareTypes[section]
        return fareCategoriesFor(fareType: type).count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fareTypes.count
    }
    
}

extension PassengerDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fareTypes[section].typeName
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            headerView.textLabel?.font = Font.bold(size: FontSize.mediumLarge)
            headerView.contentMode = .bottomLeft
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        let title = UILabel()
        title.frame =  CGRect(x: 10, y: 0, width: headerFrame.size.width, height: 40)
        title.font = Font.bold(size: FontSize.mediumLarge)
        title.text = fareTypes[section].typeName
        title.backgroundColor = .white
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: headerFrame.size.width, height: headerFrame.size.height))
        headerView.addSubview(title)
        return headerView
        
    }
    
}

extension PassengerDetailsViewController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            
            mainView.frame.origin.y += self.view.layer.bounds.height * 0.7
            backdropView.alpha = 0
            
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.mainView.frame.origin.y -= self.view.layer.bounds.height * 0.7
                self.backdropView.alpha = 1
            }, completion: { (_) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.mainView.frame.origin.y += self.view.layer.bounds.height * 0.7
                self.backdropView.alpha = 0
            }, completion: { (_) in
                transitionContext.completeTransition(true)
            })
        }
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}
