fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
### podinstall
```
fastlane podinstall
```
Run CocoaPods pod install command for project
### build
```
fastlane build
```
Builds the project to test that it compiles
### update_gems
```
fastlane update_gems
```
Update Ruby Gems
### run_swiftlint
```
fastlane run_swiftlint
```
Run swiftlint

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
