//
//  URLs.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 07/02/2019.
//

import Foundation

enum URLs {

    static let testUrl          = URL(staticString: "https://test.com")
    static let privacyPolicyUrl = URL(staticString: "http://192.168.1.50:7081/cfr/api/v1/policies")
    static let termsUrl         = URL(staticString: "http://192.168.1.50:7081/cfr/api/v1/terms")
    static let stopSuffix       = "/stations/search"
    static let locationSuffix   = "/stations/nearest"
    static let recentsSuffix    = "/stations/"
}

enum App {
    static let iTunesId         = 0
}
