//
//  StyleKit.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 12/03/2017.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import UIKit

struct AppStyle {

    // global appearence of the UINavigationBar (white text, primary color, without shadow)
    static func setupAppearance() {
        // TODO: Set up appearances if needed
//        let switchControlAppearance = UISwitch.appearance()
//        switchControlAppearance.onTintColor = Colors.switchControlOn
        
        // navigation bar style
        UINavigationBar.appearance().tintColor =  .white //#colorLiteral(red: 0.5085967183, green: 0.07502175122, blue: 0.2506508231, alpha: 1)
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().barTintColor = .primary
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: Font.barlowSemiBold(size: .mediumExtra)]

    }
    
    struct Layout {
        static let padding: CGFloat                 = 20.0
        static let tabBarItemPadding: CGFloat       = 17.0
        static let cellIconPadding: CGFloat         = 16.0
        static let height24: CGFloat                = 24.0
    }
    
    struct PaddingValues {
        static let smallPadding: CGFloat = 8
        static let mediumPadding: CGFloat = 16
        static let bigPadding: CGFloat = 24
        static let mediumSmallPadding: CGFloat = 10.0
        static let veryBigPadding: CGFloat = 30.0
    }
}
