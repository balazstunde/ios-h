//
//  SortMethodViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 12/08/2019.
//

import UIKit

protocol SortMethodViewControllerDelegate: AnyObject {
    func didTouchOutside(on viewController: SortMethodViewController)
    func didSelectSortMethod(_ method: SortMethod)
}
enum SortMethod: String, CaseIterable {
    case departureTime = "Departure time"
    case price = "Lowest price"
}

class SortMethodViewController: BaseViewController {
    
    private let tableView = UITableView()
    private var sortMethods: [SortMethod] = SortMethod.allCases
    private var activeMethod: Int = 0
    private let cellIdentifier = "SearchMethodCell"
    private let mainView = UIView()
    weak var delegate: SortMethodViewControllerDelegate?
    private var isPresented = false
    
    override init() {
        super.init()
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        tableView.register(SortMethodCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1))
        tableView.allowsMultipleSelection = false
        mainView.addSubview(tableView)
        
        mainView.backgroundColor = .white
        mainView.layer.cornerRadius = Style.cornerConstant
        view.addSubview(mainView)
    
        let outGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTouchOutside))
        outGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(outGestureRecognizer)
    }
    
    @objc private func didTouchOutside() {
        delegate?.didTouchOutside(on: self)
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        mainView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            mainView.heightAnchor.constraint(equalToConstant: CGFloat(self.sortMethods.count * Style.cellHeight + 30)),
            mainView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            mainView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            
            tableView.topAnchor.constraint(equalTo: mainView.topAnchor,constant: LayoutValues.mediumPadding),
            tableView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -LayoutValues.bigPadding),
            tableView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: LayoutValues.bigPadding),
            tableView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -LayoutValues.mediumPadding)
        ])
    }
}

extension SortMethodViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Style.cellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if activeMethod == indexPath.row {
            delegate?.didTouchOutside(on: self)
            return
        }
        let cell  = tableView.cellForRow(at: indexPath) as? SortMethodCell
        cell?.selectedMethodImage.isHidden = false
        let oldCell = tableView.cellForRow(at: IndexPath(row: activeMethod, section: 0)) as? SortMethodCell
        oldCell?.selectedMethodImage.isHidden = true
        activeMethod = indexPath.row
        delegate?.didSelectSortMethod(sortMethods[activeMethod])
    }
}

extension SortMethodViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SortMethodCell else {
            fatalError("Error dequeue-ing")
        }
        if indexPath.row == activeMethod {
            cell.configure(withSortMethod: sortMethods[indexPath.row].rawValue, isActive: true)
        } else {
            cell.configure(withSortMethod: sortMethods[indexPath.row].rawValue, isActive: false)
        }
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        return cell
    }
}

extension SortMethodViewController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else { return }
        isPresented = !isPresented
        if isPresented {
            containerView.addSubview(toVC.view)
            UIView.animate(withDuration: 0.33, delay: 0, options: [.transitionCrossDissolve], animations: {
                self.mainView.frame.origin.y -= CGFloat(self.sortMethods.count * Style.cellHeight + 30)
            }, completion: { (_) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.33, delay: 0, options: [.curveEaseOut], animations: {
                self.mainView.frame.origin.y += CGFloat(self.sortMethods.count * Style.cellHeight + 30)
            }, completion: { (_) in
                transitionContext.completeTransition(true)
            })
        }
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension SortMethodViewController {
    private enum LayoutValues {
        static let mediumPadding: CGFloat = 16.0
        static let bigPadding: CGFloat = 32.0
    }
    private enum Style {
        static let cornerConstant: CGFloat = 8.0
        static let cellHeight: Int = 60
    }
}
