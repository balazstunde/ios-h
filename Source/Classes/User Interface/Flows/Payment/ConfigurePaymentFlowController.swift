//
//  ConfigurePaymentFlowController.swift
//  Choo Choo
//
//  Created by Halcyon on 19/08/2019.
//

import UIKit

class ConfigurePaymentFlowController: NavigationFlowController {
    private let paymentType: PaymentType
    
    init (from: FlowController, paymentType: PaymentType) {
        self.paymentType = paymentType
        super.init(from: from)
    }
    
    required init(from parent: FlowController?) {
        fatalError("init(from:) has not been implemented")
    }
    
    override var firstScreen: UIViewController {
        let cpdVC = ConfigurePaymentDetailsViewController(paymentType: paymentType)
        cpdVC.delegate = self
        return cpdVC
    }
}

extension ConfigurePaymentFlowController: ConfigurePaymentViewControllerFlowDelegate {
    func didSuccessfullyUpdateCard(on viewController: ConfigurePaymentDetailsViewController) {
        navigationController.presentAlert(customTitle: "Good!", customMessage: "Credit card updated successfully!")
    }
    
    func didFailToUpdateCreditCard(on viewController: ConfigurePaymentDetailsViewController) {
        navigationController.presentAlert(customTitle: "Ups!", customMessage: "Something went wrong! Check your data and try again.")
    }
    
    func didTouchBackButton(on viewController: ConfigurePaymentDetailsViewController) {
        navigationController.popViewController(animated: true)
    }
}
