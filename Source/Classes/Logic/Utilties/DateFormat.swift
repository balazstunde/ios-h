//
//  DateFormat.swift
//  Choo Choo
//
//  Created by Halcyon on 31/07/2019.
//

import Foundation

enum DateFormats: String {
    case nameOfTheDayMonthDayYear = "EEEE, MMM d, yyyy"
    case nameOfTheDayMonthDay = "EEEE, MMM d"
    case dayMonthYearAsNumbers = "dd.MM.yy"
    case yearMonthDay = "yyyy/MM/dd"
    case yearMonthDayWithLine = "yyyy-MM-dd"
    case hourMinutesSeconds = "HH:mm:ss"
    case nameOfTheDayMonthDayTime = "EEEE, MMM d HH:mm"
    case ticketDayAndHourFormat = "E d MMM HH:mm"
    case identifierFormat = "d MMM HH:mm"
}

extension DateFormatter {
    static func getDateAsString(inFormat: DateFormats, date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inFormat.rawValue
        return dateFormatter.string(from: date)
    }
    
    static func getStringAsDate(inFormat: DateFormats, date: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = inFormat.rawValue
        return formatter.date(from: date)
    }
    
    static func getIntervalBetween(startDate: Date, endDate: Date) -> String {
        let interval = startDate.timeIntervalSince(endDate)
        var hours: Int
        var minutes: Int
        if interval < 0 {
            hours = Unit.hourInDay - Int(abs(interval / UnitToSeconds.hour)) - 1
            minutes = Unit.minutesInHour - Int(abs(interval.truncatingRemainder(dividingBy: UnitToSeconds.hour) / UnitToSeconds.minute))
        } else {
            hours = Int(abs(interval / UnitToSeconds.hour))
            minutes = Int(abs(interval.truncatingRemainder(dividingBy: UnitToSeconds.hour) / UnitToSeconds.minute))
        }
        return "\(hours)h:\(minutes) min"
    }
}

extension DateFormatter {
    enum UnitToSeconds {
        static let hour: Double = 3600
        static let minute: Double = 60
    }
    enum Unit {
        static let hourInDay: Int = 24
        static let minutesInHour: Int = 60
    }
}
