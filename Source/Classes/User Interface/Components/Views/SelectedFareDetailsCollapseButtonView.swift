//
//  SelectedFareDetailsCollapseButtonView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 16/08/2019.
//

import UIKit

protocol SelectedFareDetailsCollapseButtonViewDelegate: AnyObject {
    func didPressCollapseButton(hideTable: Bool)
}

class SelectedFareDetailsCollapseButtonView: UIView {
    weak var delegate: SelectedFareDetailsCollapseButtonViewDelegate?
    private let bottomLine = UIView()
    private let leftLine = UIView()
    private let buttonImageView = UIImageView()
    private var flippedImage: UIImage?
    private let titleLabel = UILabel()
    private let numberOfStops: Int
    private var flip = true
    
    init(numberOfStops: Int) {
        self.numberOfStops = numberOfStops - 2
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(bottomLine)
        addSubview(leftLine)
        addSubview(buttonImageView)
        buttonImageView.addSubview(titleLabel)
        
        bottomLine.backgroundColor = .softPeach
        leftLine.backgroundColor = .switchButtonColor
        
        buttonImageView.image = numberOfStops == 0 ? UIImage() : Asset.icNavigationDownArrow.image
        buttonImageView.image?.withRenderingMode(.alwaysTemplate)
        buttonImageView.tintColor = .shiraz
        buttonImageView.contentMode = .left
        buttonImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didPressCollapseButton)))
        buttonImageView.isUserInteractionEnabled = true
        
        titleLabel.text = numberOfStops == 0 ? "No stops" : "\(numberOfStops) stops"
        titleLabel.font = Font.barlowSemiBold(size: .mediumSmall)
        titleLabel.textColor = .shiraz
        
        if let image = buttonImageView.image, let cgImage = image.cgImage {
            flippedImage = UIImage(cgImage: cgImage, scale: image.scale, orientation: .downMirrored)
            flippedImage = flippedImage?.withRenderingMode(.alwaysTemplate)
        }
    }
    
    private func setupConstraints() {
        bottomLine.translatesAutoresizingMaskIntoConstraints = false
        leftLine.translatesAutoresizingMaskIntoConstraints = false
        buttonImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            leftLine.topAnchor.constraint(equalTo: topAnchor),
            leftLine.bottomAnchor.constraint(equalTo: bottomAnchor),
            leftLine.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.linePadding),
            leftLine.widthAnchor.constraint(equalToConstant: Layout.lineWidth),
            
            buttonImageView.leadingAnchor.constraint(equalTo: leftLine.trailingAnchor, constant: AppStyle.Layout.padding),
            buttonImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: buttonImageView.topAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: buttonImageView.bottomAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: buttonImageView.trailingAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: buttonImageView.leadingAnchor, constant: (buttonImageView.image?.size.width ?? 50) + AppStyle.Layout.padding),
            
            bottomLine.bottomAnchor.constraint(equalTo: bottomAnchor),
            bottomLine.leadingAnchor.constraint(equalTo: buttonImageView.leadingAnchor),
            bottomLine.heightAnchor.constraint(equalToConstant: Layout.lineWidth),
            bottomLine.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -AppStyle.Layout.padding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didPressCollapseButton() {
        if let flipped = flippedImage {
            buttonImageView.image = flip ? flipped : Asset.icNavigationDownArrow.image
            buttonImageView.image?.withRenderingMode(.alwaysTemplate)
            buttonImageView.tintColor = .shiraz
            buttonImageView.contentMode = .left
        }
        flip = !flip
        delegate?.didPressCollapseButton(hideTable: flip)
    }
}

extension SelectedFareDetailsCollapseButtonView {
    private enum Layout {
        static let linePadding: CGFloat = 77
        static let lineWidth: CGFloat = 1
    }
}
