import Foundation

enum TravelType: String, Codable {
    case first = "FIRST"
    case second = "SECOND"
    case coach = "COACH"
}
