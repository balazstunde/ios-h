//
//  TravelPreferencesFlowController.swift
//  Choo Choo
//
//  Created by Halcyon on 06/08/2019.
//

import UIKit

class TravelPreferencesFlowController: NavigationFlowController {
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        return .overCurrentContext
    }
    
    override var modalTransitionStyle: UIModalTransitionStyle {
        return .coverVertical
    }
    
    override var firstScreen: UIViewController {
        let tripConfigurationVC = TripConfigurationViewController()
        tripConfigurationVC.delegate = self
        return tripConfigurationVC
    }
}

extension TravelPreferencesFlowController: TripConfigurationViewControllerFlowDelegate {
    func close() {
        finish()
        navigationController.dismiss(animated: true, completion: nil)
    }
    
    func closeWithConfirm(selected: TravelPreferences) {
        finish()
        navigationController.dismiss(animated: true, completion: nil)
    }
    
}
