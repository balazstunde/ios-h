//
//  TravelPreferences.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation

struct TravelPreferences {
    let name: String
    let price: Double
}
