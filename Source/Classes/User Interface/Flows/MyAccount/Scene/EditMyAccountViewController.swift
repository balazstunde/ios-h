//
//  EditMyAccountViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import UIKit

private struct SizeButtonAndView {
    static let buttonHeight: CGFloat = 48
    static let viewHeight: CGFloat = 100
}

protocol EditMyAccountViewControllerFLowDelegate: AnyObject {
    func didTouchEditPassword(on viewController: EditMyAccountViewController)
    func didTouchBackButton(on viewController: EditMyAccountViewController)
}

class EditMyAccountViewController: BaseViewController {
    
    private let keyStore = KeyStore.shared
    weak var delegate: EditMyAccountViewControllerFLowDelegate?
    
    private var firstNameView = EditView(viewType: .name)
    private var emailView = EditView(viewType: .email)
    private var passwordView = EditView(viewType: .password)
    
    private let saveButton = UIButton()
    
    private let updateService = AuthenticationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpContraints()
    }
    
    private func initNavBar() {
        navigationController?.navigationBar.barTintColor = .primary
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.title = "Profile settings"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: Font.barlowSemiBold(size: .mediumLarge)]
    }
    
    private func setUpView() {
        
        firstNameView.delegate = self
        emailView.delegate = self
        passwordView.delegate = self
        passwordView.isEditAllowed = true
        
        view.backgroundColor = .white
        initNavBar()
        initButton()
        
        view.addSubview(firstNameView)
        view.addSubview(emailView)
        view.addSubview(passwordView)
        view.addSubview(saveButton)
        
        loadData()
    }
    
    private func initButton() {
        saveButton.setTitle("Save", for: .normal)
        saveButton.titleLabel?.font = Font.barlowBold(size: FontSize.medium)
        saveButton.backgroundColor = .lola
        saveButton.layer.cornerRadius = 5
        saveButton.layer.shadowColor = #colorLiteral(red: 0.8, green: 0.7529411765, blue: 0.7764705882, alpha: 1)
        saveButton.layer.shadowRadius = 5
        saveButton.layer.shadowOpacity = 0.5
        saveButton.isEnabled = false
        saveButton.addTarget(self, action: #selector(didPressSave), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

    }
    
    private func setUpContraints() {
        firstNameView.translatesAutoresizingMaskIntoConstraints = false
        emailView.translatesAutoresizingMaskIntoConstraints = false
        passwordView.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            firstNameView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Padding.mediumBig),
            firstNameView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            firstNameView.heightAnchor.constraint(equalToConstant: SizeButtonAndView.viewHeight),
            firstNameView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.small),
            
            emailView.topAnchor.constraint(equalTo: firstNameView.bottomAnchor),
            emailView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            emailView.heightAnchor.constraint(equalToConstant: SizeButtonAndView.viewHeight),
            emailView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.small),
            
            passwordView.topAnchor.constraint(equalTo: emailView.bottomAnchor),
            passwordView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            passwordView.heightAnchor.constraint(equalToConstant: SizeButtonAndView.viewHeight),
            passwordView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.small),
            
            saveButton.heightAnchor.constraint(equalToConstant: SizeButtonAndView.buttonHeight),
            saveButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Padding.mediumBig),
            saveButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Padding.mediumBig),
            saveButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.mediumBig)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func backButtonTapped() {
        delegate?.didTouchBackButton(on: self)
    }
    
    @objc private func didPressSave() {
        guard let id = keyStore.userID,
            let currentName = keyStore.currentUser?.firstName,
            let currentEmail = keyStore.currentUser?.email,
            let newName = firstNameView.text,
            let newEmail = emailView.text
            else { return }
        
        if newName != currentName || newEmail != currentEmail {
            let userInfo = UserInformations(email: newEmail, firstName: newName)
            updateService.updateUser(userInfo: userInfo, userId: id, completion: {
                let updatedUser = UserInformations( email: newEmail, firstName: newName)
                self.keyStore.currentUser = updatedUser
                    DispatchQueue.main.async {
                        self.loadData()
                    }
                }, failure: { _ in
                     DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Hopa..", message: "Something went wrong", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.loadData()
                    }})
        }
    }
    private func loadData() {
        let user = keyStore.currentUser
        if let currentUser = user {
            firstNameView.text = currentUser.firstName
            emailView.text = currentUser.email
            passwordView.text = "CurrentPassword"
        }
        saveButton.backgroundColor = .lola
        
    }
}

extension EditMyAccountViewController: EditViewDelegate {
    func didTouchTextField() {
        guard
            let name = firstNameView.text, !name.isEmpty,
            let email = emailView.text, !email.isEmpty,
            let password = passwordView.text, !password.isEmpty
            else {
                self.saveButton.isEnabled = false
                self.saveButton.backgroundColor = .lola
                
                return
        }
        self.saveButton.backgroundColor = .primary
        self.saveButton.isEnabled = true
    }
    
    func didTouchEdit() {
        delegate?.didTouchEditPassword(on: self)
    }
}
