//
//  NotificationPreferencesSection.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

enum NotificationPreferencesSection {
    case general([String])
    case notify([String])
    case discounts([String])
    
    var sectionTitle : String {
        switch self {
        case .general: return "General"
        case .notify: return "Notify me when"
        case .discounts: return "Discounts and seasonal offers"
        }
    }
}
