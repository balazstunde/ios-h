//
//  SearchOverviewViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 30/07/2019.
//

import Foundation
import UIKit

protocol SearchViewControllerFlowDelegate: AnyObject {
    func didTouchLocationField(on viewController: SearchViewController, location: SearchStationType)
    func didTouchSwitchButton(on viewController: SearchViewController)
    func didTapDatePickerView(on viewController: SearchViewController)
    func didTapPersonPickerView(on viewController: SearchViewController)
    func didTapSearchButton(on viewController: SearchViewController)
}

class SearchViewController: BaseViewController {
    // UI Components
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let headerImageView = UIImageView()
    private let titleLabel = UILabel()
    private let fromToView = FromToView()
    private let datePickerView = TripDetailsView(icon: Asset.icNavigationDatePicker.image)
    private let personPickerView = TripDetailsView(icon: Asset.icNavigationTicketDisabled.image)
    private let searchButton = MainButton(title: "Search")
    private var isFilledOutFromInput: Bool = false
    private var isFilledOutDestinationInput: Bool = false
    
    weak var delegate: SearchViewControllerFlowDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        setupListeners()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
        hidesBottomBarWhenPushed = false
    }
    
    private func setupView() {
        edgesForExtendedLayout = []
        view.backgroundColor = .white
        
        // Setup scroll view
        view.addSubview(scrollView)
        
        // Setup content view
        scrollView.addSubview(contentView)
        scrollView.contentInsetAdjustmentBehavior = .never
        
        // Setup header image
        contentView.addSubview(headerImageView)
        headerImageView.image = Asset.header.image
        
        // Setup title label
        contentView.addSubview(titleLabel)
        titleLabel.text = "Hi!\nWhere would you like\nto travel today?"
        titleLabel.textColor = .white
        titleLabel.font = Font.barlowBold(size: .title)
        titleLabel.numberOfLines = 0
        
        // Setup from-to component
        contentView.addSubview(fromToView)
        fromToView.layer.cornerRadius = StyleValues.cornerRadiusValue
        fromToView.layer.shadowColor = UIColor.gray.cgColor
        fromToView.layer.shadowOpacity = StyleValues.shadowOpacity
        fromToView.layer.shadowOffset = CGSize.zero
        fromToView.layer.shadowRadius = StyleValues.cornerRadiusValue
        
        // Setup date picker component
        contentView.addSubview(datePickerView)
        datePickerView.layer.cornerRadius = StyleValues.cornerRadiusValue
        updateDateDetails(with: Date())
        
        // Setup person picker component
        contentView.addSubview(personPickerView)
        personPickerView.layer.cornerRadius = StyleValues.cornerRadiusValue
        personPickerView.setViewText(withText: "1 Adult")
        
        // Setup search button component
        contentView.addSubview(searchButton)
        searchButton.isEnabled = false
        searchButton.layer.cornerRadius = StyleValues.cornerRadiusValue
        searchButton.layer.shadowColor = UIColor.gray.cgColor
        searchButton.layer.shadowOpacity = StyleValues.shadowOpacity
        searchButton.layer.shadowOffset = CGSize.zero
        searchButton.layer.shadowRadius = StyleValues.cornerRadiusValue
        searchButton.isEnabled = false
    }
    
    private func setupConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        headerImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        fromToView.translatesAutoresizingMaskIntoConstraints = false
        datePickerView.translatesAutoresizingMaskIntoConstraints = false
        personPickerView.translatesAutoresizingMaskIntoConstraints = false
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            headerImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            headerImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            headerImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            headerImageView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: LayoutValues.headerImageViewHeightMultiplier),
            
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.titleSidePadding),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -LayoutValues.titleSidePadding),
            titleLabel.bottomAnchor.constraint(equalTo: headerImageView.bottomAnchor, constant: -LayoutValues.titleBottomPadding),
            titleLabel.topAnchor.constraint(lessThanOrEqualTo: contentView.topAnchor, constant: LayoutValues.titleTopPadding),
            
            fromToView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.mediumPadding),
            fromToView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutValues.mediumPadding),
            fromToView.topAnchor.constraint(equalTo: headerImageView.bottomAnchor, constant: -LayoutValues.fromToViewTopPadding),
            
            datePickerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.mediumPadding),
            datePickerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutValues.mediumPadding),
            datePickerView.topAnchor.constraint(equalTo: fromToView.bottomAnchor, constant: LayoutValues.smallPadding),
            datePickerView.heightAnchor.constraint(equalToConstant: LayoutValues.subviewHeight),
            
            personPickerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.mediumPadding),
            personPickerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutValues.mediumPadding),
            personPickerView.topAnchor.constraint(equalTo: datePickerView.bottomAnchor, constant: LayoutValues.smallPadding),
            personPickerView.heightAnchor.constraint(equalTo: datePickerView.heightAnchor),
            
            searchButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.mediumPadding),
            searchButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutValues.mediumPadding),
            searchButton.topAnchor.constraint(equalTo: personPickerView.bottomAnchor, constant: LayoutValues.mediumPadding),
            searchButton.heightAnchor.constraint(equalToConstant: LayoutValues.buttonHeight)
            
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupListeners() {
        fromToView.didTouchFrom = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTouchLocationField(on: self, location: .departure)
        }
        
        fromToView.didTouchTo = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTouchLocationField(on: self, location: .destination)
        }
        
        fromToView.didTouchSwitch = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTouchSwitchButton(on: self)
        }
        
        datePickerView.didTapViewClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapDatePickerView(on: self)
        }
        
        personPickerView.didTapViewClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapPersonPickerView(on: self)
        }
        
        searchButton.didTapButtonClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapSearchButton(on: self)
        }
    }
    
    public func setDestination(destination: String?) {
        if let destination = destination {
            fromToView.toTextField = destination
            setImageStateComplete()
        }
    }
    
    public func setDeparture(departure: String) {
        fromToView.fromTextField = departure
        setImageStateComplete()
    }
    
    private func setImageStateComplete() {
        if fromToView.fromTextField != "" && fromToView.toTextField != "" {
            fromToView.progressImageState = .complete
        }
    }
    
    public func updatePersonDetails(with map:[String:Int]) {
        var details: [String] = []
        for (person, number) in map where number > 0 {
            details.append("\(person) \(number)")
        }
        personPickerView.setViewText(withText: details.joined(separator: ", "))
    }
    
    public func updateDateDetails(with selectedDate: Date) {
        var stringDate: String = ""
        if Calendar.current.isDateInToday(selectedDate) {
            stringDate = "Today"
        } else if Calendar.current.isDateInTomorrow(selectedDate) {
            stringDate = "Tomorrow"
        } else {
            stringDate = DateFormatter.getDateAsString(inFormat: DateFormats.nameOfTheDayMonthDay, date: selectedDate)
        }
        datePickerView.setViewText(withText: stringDate)
    }
    
    func enableSearchButton() {
        searchButton.isEnabled = true
    }
}

extension SearchViewController {
    private enum LayoutValues {
        static let titleSidePadding: CGFloat = 24
        static let titleBottomPadding: CGFloat = 75
        static let titleTopPadding: CGFloat = 100
        static let headerImageViewHeightMultiplier: CGFloat = 0.4
        static let fromToViewTopPadding: CGFloat = 50
        static let smallPadding: CGFloat = 8
        static let mediumPadding: CGFloat = 16
        static let subviewHeight: CGFloat = 48
        static let buttonHeight: CGFloat = 49
    }
    private enum StyleValues {
        static let cornerRadiusValue: CGFloat = 6.0
        static let shadowOpacity: Float = 0.6
    }
}
