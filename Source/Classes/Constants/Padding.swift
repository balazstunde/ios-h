//
//  LayoutValues.swift
//  Choo Choo
//
//  Created by halcyonmobile on 30/07/2019.
//

import Foundation
import UIKit

struct Padding {
    static let small: CGFloat = 8
    static let mediumSmall: CGFloat = 10
    static let medium: CGFloat = 16
    static let mediumBig: CGFloat = 20
    static let big: CGFloat = 24
    static let extraLarge: CGFloat = 32
    static let large: CGFloat = 36
    static let custom: CGFloat = 56
}
