//
//  TicketDetails.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 06/08/2019.
//

import UIKit

class TicketDetailsView: UIView {
    
    private let trainNumberLabel = UILabel()
    private let ticketTypeLabel = UILabel()
    private let barCodeImage = UIImageView()
    private let travelTimeLabel = UILabel()
    private let icon = UIImageView()
    private let backgroundImage = UIImageView()
    private let departureCityLabel = UILabel()
    private let destinationCityLabel = UILabel()
    private let departureTimeLabel = UILabel()
    private let destinationTimeLabel = UILabel()
    
    private let contentView = UIView()
    
    private let dateView = TicketDetailsLabelsView()
    private let platformView = TicketDetailsLabelsView()
    private let classView = TicketDetailsLabelsView()
    private let boardingView = TicketDetailsLabelsView()
    
    private let destinationStackView = UIStackView()
    private let departureStackView = UIStackView()
    private let topStackView = UIStackView()
    private let bottomStackView = UIStackView()
    private let ticket: Ticket
    
    private enum LayoutValues {
        static let stackViewSpacing: CGFloat = 30.0
        static let padding25: CGFloat = 25.0
        static let padding20: CGFloat = 20.0
        static let padding60: CGFloat = 60.0
    }
    
    init(with ticket: Ticket) {
        self.ticket = ticket
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(backgroundImage)
        addSubview(contentView)
        contentView.addSubview(trainNumberLabel)
        contentView.addSubview(ticketTypeLabel)
        contentView.addSubview(barCodeImage)
        contentView.addSubview(icon)
        contentView.addSubview(topStackView)
        contentView.addSubview(bottomStackView)
        
        backgroundImage.image = Asset.icBigTicketBackground.image
        trainNumberLabel.text = ticket.tripIdentifier
        trainNumberLabel.font = Font.barlowRegular(size: .medium)
        ticketTypeLabel.text = ticket.pricingCategory.title
        ticketTypeLabel.font = Font.barlowRegular(size: .medium)
        barCodeImage.image = Asset.icBarcode.image
        barCodeImage.contentMode = .scaleAspectFit
        travelTimeLabel.text = getTravelTime(departureTime: ticket.startTrainStopDto.departureTime, arrivalTime: ticket.endTrainStopDto.arrivalTime)
        travelTimeLabel.font = Font.barlowRegular(size: .medium)
        travelTimeLabel.textColor = UIColor.textDescriptionColor
        icon.image = Asset.icPinnedDestination.image
        icon.contentMode = .scaleAspectFit
        
        departureCityLabel.text = ticket.startTrainStopDto.stopName
        departureCityLabel.font = Font.barlowRegular(size: .mediumLarge)
        departureTimeLabel.text = ticket.startTrainStopDto.departureTime.getHourMinutes()
        departureTimeLabel.font = Font.barlowBold(size: .mediumLarge)
        
        destinationCityLabel.text = ticket.endTrainStopDto.stopName
        destinationCityLabel.font = Font.barlowRegular(size: .mediumLarge)
        destinationTimeLabel.text = ticket.endTrainStopDto.arrivalTime.getHourMinutes()
        destinationTimeLabel.font = Font.barlowBold(size: .mediumLarge)
        
        dateView.configure(title: "Date", text: ticket.date)
        classView.configure(title: "Class", text: ticket.travellingClass.rawValue)
        boardingView.configure(title: "Boarding", text: ticket.startTrainStopDto.arrivalTime.getHourMinutes())
        platformView.configure(title: "Platform", text: String(ticket.startTrainStopDto.platformNumber ?? 0))
        
        departureStackView.addArrangedSubview(departureCityLabel)
        departureStackView.addArrangedSubview(departureTimeLabel)
        departureStackView.axis = .horizontal
        departureStackView.spacing = LayoutValues.stackViewSpacing
        departureStackView.distribution = .fillEqually
        
        destinationStackView.addArrangedSubview(destinationCityLabel)
        destinationStackView.addArrangedSubview(destinationTimeLabel)
        destinationStackView.axis = .horizontal
        destinationStackView.spacing = LayoutValues.stackViewSpacing
        destinationStackView.distribution = .fillEqually
        
        bottomStackView.addArrangedSubview(dateView)
        bottomStackView.addArrangedSubview(platformView)
        bottomStackView.addArrangedSubview(classView)
        bottomStackView.addArrangedSubview(boardingView)
        bottomStackView.axis = .horizontal
        bottomStackView.distribution = .fillEqually
        
        topStackView.addArrangedSubview(departureStackView)
        topStackView.addArrangedSubview(travelTimeLabel)
        topStackView.addArrangedSubview(destinationStackView)
        topStackView.axis = .vertical
        topStackView.distribution = .fillEqually
    }
    
    private func setupConstraints() {
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        trainNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        ticketTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        barCodeImage.translatesAutoresizingMaskIntoConstraints = false
        travelTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        icon.translatesAutoresizingMaskIntoConstraints = false
        dateView.translatesAutoresizingMaskIntoConstraints = false
        platformView.translatesAutoresizingMaskIntoConstraints = false
        classView.translatesAutoresizingMaskIntoConstraints = false
        boardingView.translatesAutoresizingMaskIntoConstraints = false
        topStackView.translatesAutoresizingMaskIntoConstraints = false
        bottomStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            backgroundImage.topAnchor.constraint(equalTo: topAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            contentView.topAnchor.constraint(equalTo: topAnchor, constant: LayoutValues.padding25),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -LayoutValues.padding25),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.padding25),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.padding25),
            
            barCodeImage.topAnchor.constraint(equalTo: contentView.topAnchor),
            barCodeImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            barCodeImage.bottomAnchor.constraint(equalTo: topStackView.bottomAnchor),
            
            trainNumberLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            trainNumberLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            
            ticketTypeLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            ticketTypeLabel.leadingAnchor.constraint(equalTo: trainNumberLabel.trailingAnchor, constant: LayoutValues.padding60),
            
            icon.topAnchor.constraint(greaterThanOrEqualTo: trainNumberLabel.bottomAnchor, constant: LayoutValues.padding20),
            icon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            
            topStackView.topAnchor.constraint(equalTo: icon.topAnchor),
            topStackView.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: LayoutValues.padding20),
            topStackView.heightAnchor.constraint(equalTo: icon.heightAnchor),
            
            bottomStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            bottomStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            bottomStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    private func getTravelTime(departureTime: String, arrivalTime: String) -> String {
        let splitDeparture = departureTime.split(separator: ":")
        let splitarrival = arrivalTime.split(separator: ":")
        
        guard let departureHours = Int(splitDeparture[0]), let departureMinutes = Int(splitDeparture[1]), let arrivalHours = Int(splitarrival[0]), let arrivalMinutes = Int(splitarrival[1]) else { return "" }
        
        if arrivalMinutes - departureMinutes < 0 {
            if arrivalHours - departureHours < 0 {
                return "\(arrivalHours + 12 - departureHours) h: \(arrivalMinutes + 60 - departureMinutes) min"
            } else {
                return "\(arrivalHours - departureHours) h: \(arrivalMinutes + 60 - departureMinutes) min"
            }
        } else {
            if arrivalHours - departureHours < 0 {
                return "\(arrivalHours + 12 - departureHours) h: \(arrivalMinutes - departureMinutes) min"
            } else {
                return "\(arrivalHours - departureHours) h: \(arrivalMinutes - departureMinutes) min"
            }
        }
    }
}
