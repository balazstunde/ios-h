//
//  PricingCategory.swift
//  Choo Choo
//
//  Created by halcyonmobile on 20/08/2019.
//

import Foundation
struct PricingCategory: Codable {
    let description: String
    let discountPercentage: Double
    let name: String
    let title: String
}
