//
//  MyAccountViewController.swift
//  Choo Choo
//
//  Created by halcyonmobile on 29/07/2019.
//

import UIKit

struct Identifiers {
    static let cellIdentifier: String = "MyAccountTableCell"
    static let headerIdentifier: String = "MyAccountSectionHeaderView"
}

protocol MyAccountViewControllerFLowDelegate: AnyObject {
    func didTouchProfileSettings(on viewController: MyAccountViewController)
    func didTouchPaymentMethod(on viewController: MyAccountViewController)
    func didTouchNotifications(on viewController: MyAccountViewController)
    func didTouchPrivacyPolicy(on viewController: MyAccountViewController)
    func didTouchTermsAndConditions(on viewController: MyAccountViewController)
    func didTouchCustomerSupport(on viewController: MyAccountViewController)
    func didTouchLogOut(on viewController: MyAccountViewController)
}

class MyAccountViewController: UIViewController {
    private var welcomeStackView = UIStackView()
    private let firstWelcomeLabel = UILabel()
    private let secondWelcomeLabel = UILabel()
    private let tableView = UITableView()
    private var myAccountRowSections: [MyAccountSection] = []
    private let authService = AuthenticationService()
    weak var delegate: MyAccountViewControllerFLowDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        setUpConstraints()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let userFirstName = KeyStore.shared.currentUser?.firstName else { return }
        
        firstWelcomeLabel.text = "Welcome \(userFirstName)!"
    }
    
    private func initView() {
        view.backgroundColor = .white
        navigationItem.title = "My Account"
        navigationItem.hidesBackButton = true
        
        firstWelcomeLabel.font = Font.barlowBold(size: .large)
        secondWelcomeLabel.text = "Good to have you here."
        secondWelcomeLabel.font = Font.barlowMedium(size: .large)
        
        welcomeStackView = UIStackView(arrangedSubviews: [firstWelcomeLabel, secondWelcomeLabel])
        welcomeStackView.alignment = .leading
        welcomeStackView.axis = .vertical
        welcomeStackView.spacing = Padding.small
        
        view.addSubview(welcomeStackView)
        view.addSubview(tableView)
        setUpTableView()
    }
    
    private func setUpTableView() {
        tableView.register(MyAccountTableCell.self, forCellReuseIdentifier: Identifiers.cellIdentifier)
        tableView.register(MyAccountSectionHeaderView.self, forHeaderFooterViewReuseIdentifier: Identifiers.headerIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 70
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
    
    private func loadData() {
        myAccountRowSections = MyAccountDataSource.createSectionData()
    }
    
    private func setUpConstraints() {
        var constraints = [NSLayoutConstraint] ()
        
        welcomeStackView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(welcomeStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Padding.extraLarge))
        constraints.append(welcomeStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Padding.medium))
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(tableView.topAnchor.constraint(equalTo: welcomeStackView.bottomAnchor, constant: Padding.big))
        constraints.append(tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor))
        constraints.append(tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor))
        constraints.append(tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor))
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension MyAccountViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return myAccountRowSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch myAccountRowSections[section] {
        case .general(let rows): return rows.count
        case .support(let rows): return rows.count
        case .logout(let rows): return rows.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.cellIdentifier, for: indexPath) as? MyAccountTableCell else {
            assertionFailure("Could not dequeue")
            return UITableViewCell()
        }
        switch myAccountRowSections[indexPath.section] {
        case .general(let rows), .support(let rows):
            cell.configureCell(rows[indexPath.row])
            cell.accessoryView = UIImageView(image: Asset.iconNext.image)
            
        case .logout(let rows):
            cell.configureCell(rows[indexPath.row])
            cell.accessoryView = nil
        }
        return cell
    }
}

extension MyAccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: Identifiers.headerIdentifier) as? MyAccountSectionHeaderView else {
            assertionFailure("Could not get header footer")
            return UITableViewHeaderFooterView()
        }
        header.sectionName = myAccountRowSections[section].sectionTitle
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 72.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.isSelected = false
        switch indexPath {
        case [0,0]:
            delegate?.didTouchProfileSettings(on: self)
        case [0,1]:
            delegate?.didTouchPaymentMethod(on: self)
        case [0,2]:
            delegate?.didTouchNotifications(on: self)
        case [1,0]:
            delegate?.didTouchPrivacyPolicy(on: self)
        case [1,1]:
            delegate?.didTouchTermsAndConditions(on: self)
        case [1,2]:
            delegate?.didTouchCustomerSupport(on: self)
        case [2,0]:
            authService.logOut()
            delegate?.didTouchLogOut(on: self)
        default:
            return
        }
    }
}
