//
//  Stop.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 31/07/2019.
//

import Foundation

struct StopResponse: Decodable {
    let results: [Station]
    let pageNumber: Int
    let pageSize: Int
}
