//
//  TrainStationFinderService.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 31/07/2019.
//

import Foundation

struct GetStopsRequest: RequestRepresentable {
    let paramType: PostParamType = .body
    var suffix: String = URLs.stopSuffix
    var method: HTTPMethod = .get
    var parameters: Parameters?
    
    init(numberOfElements: Int, pageNumber: Int, prefix: String) {
        self.parameters = ["numberOfElements": numberOfElements,
                           "pageNumber": pageNumber,
                           "prefix": prefix]
    }
}

struct GetRecentStopsRequest: RequestRepresentable {
    var suffix: String = URLs.recentsSuffix
    var method: HTTPMethod = .get
    var parameters: Parameters?
    var paramType: PostParamType = .body
    
    init(recentIds: [Int]) {
        suffix += "?"
        if recentIds.count > 1 {
            for index in 0..<(recentIds.count-1) {
                suffix += "stationIds=\(recentIds[index])&"
            }
            suffix += "stationIds=\(recentIds[recentIds.count-1])"
        } else {
            suffix += "stationIds=\(recentIds[recentIds.count-1])"
        }
    }
}

class StationFinderService {
    
    var requestedPage = 0
    var canLoadMore = false
    
    func getStops(loadNextPage: Bool, elementsPerPage: Int, prefix: String, completion: @escaping (StopResponse) -> Void, failure: @escaping (Error?) -> Void) {
        requestedPage = loadNextPage ? requestedPage + 1 : 0
        BackendManager.execute(request: GetStopsRequest(numberOfElements: elementsPerPage, pageNumber: requestedPage, prefix: prefix ), success: { [weak self] (stopResponse: StopResponse) in
            guard let self = self else { return }
            self.canLoadMore = stopResponse.results.count == elementsPerPage
            completion(stopResponse)
        }, failure: failure)
    }
    
    func getRecentStops(recentIds: [Int], completion: @escaping ([Station]) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: GetRecentStopsRequest(recentIds: recentIds), success: completion, failure: failure)
    }
}
