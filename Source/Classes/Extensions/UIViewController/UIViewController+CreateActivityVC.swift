//
//  UIViewController+createActivityVCWithString.swift
//  Choo Choo
//
//  Created by Halcyon on 12/08/2019.
//

import UIKit

extension UIViewController {
    func createActivityVC(departureStation: String, destinationStation: String, trainNumber: String, departureTime: String) -> UIActivityViewController {
        let activityController = UIActivityViewController(activityItems: ["I'm travelling from \(departureStation) to \(destinationStation) with \(trainNumber) on \(departureTime)"], applicationActivities: nil)
        return activityController
    }
}
