//
//  ChangePasssordViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 5/09/2019

//

import UIKit

protocol ChangePasswordViewControllerFlowDelegate: AnyObject {
     func didTouchForgotPassword(on viewController: ChangePasssordViewController)
     func didChangePasswordFailed(on viewController: ChangePasssordViewController)
     func didTouchBackButton(on viewController: ChangePasssordViewController)
     func didChangePasswordSuccedeed(on viewController: ChangePasssordViewController)
}

class ChangePasssordViewController: BaseViewController, UITextFieldDelegate {
    
    weak var delegate: ChangePasswordViewControllerFlowDelegate?
    private let oldPasswordTextField = UITextField()
    private let newPasswordTextField = UITextField()
    private let confirmPasswordTextField = UITextField()
    
    private let underlineOldPassword = UIView()
    private let underlineNewPassword = UIView()
    private let underlineConfirmPassword = UIView()
    
    private let saveButton = UIButton()
    
    private let passwordIcon = UIImageView()
    private let passwordIcon2 = UIImageView()
    private let passwordIcon3 = UIImageView()
    private let backArrowIcon = UIImageView()
    
    private let showOldPasswordButton = UIButton()
    private let showNewPasswordButton = UIButton()
    private let showConfirmPasswordButton = UIButton()
    
    private let forgotPasswordButton = UIButton()
    
    private let changePasswordStack = UIStackView()
    private let oldPasswordStack = UIStackView()
    private let newPasswordStack = UIStackView()
    private let confirmPasswordStack = UIStackView()
    
    private var isFirstButtonDown = false
    private var isSecondButtonDown = false
    private var isThirdButtonDown = false
    
    private let oldPasswordLabel = UILabel()
    private let newPasswordLabel = UILabel()
    private let confirmPasswordLabel = UILabel()
    private let passwordDoesNotMatchLabel = UILabel()
    
    private let keyStore = KeyStore.shared
    private let authService = AuthenticationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpContraints()
        setupAddTargetIsCorrect()
    }
    
    private func initnavBar() {
        backArrowIcon.image = Asset.icNavigationBack.image
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "Change your password"
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: Font.barlowSemiBold(size: .mediumLarge)]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backArrowIcon.image, style: .plain, target: self,
                                                           action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = .white
    }
    
    private func setUpView() {
        
        view.backgroundColor = .white
        initnavBar()
        initTextFields()
        initIcons()
        initButtons()
        initLabels()
        oldPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        setUpStack()
        view.addSubview(forgotPasswordButton)
        view.addSubview(changePasswordStack)
    }
    
    private func initTextFields() {
        underlineOldPassword.backgroundColor = .paleSlate
        underlineNewPassword.backgroundColor = .paleSlate
        underlineConfirmPassword.backgroundColor = .paleSlate
        
        oldPasswordTextField.placeholder = "Old password"
        oldPasswordTextField.font = Font.barlowRegular(size: FontSize.medium)
        oldPasswordTextField.isSecureTextEntry = true
        
        newPasswordTextField.placeholder = "New Password"
        newPasswordTextField.font = Font.barlowRegular(size: FontSize.medium)
        newPasswordTextField.isSecureTextEntry = true
        
        confirmPasswordTextField.placeholder = "Confirm password"
        confirmPasswordTextField.font = Font.barlowRegular(size: FontSize.medium)
        confirmPasswordTextField.isSecureTextEntry = true
    }
    
    private func initIcons() {
        passwordIcon.image = Asset.icNavigationPassword.image
        passwordIcon.image = passwordIcon.image?.withRenderingMode(.alwaysTemplate)
        passwordIcon.tintColor = .iconColor
        
        passwordIcon2.image = Asset.icNavigationPassword.image
        passwordIcon2.image = passwordIcon.image?.withRenderingMode(.alwaysTemplate)
        passwordIcon2.tintColor = .iconColor
        
        passwordIcon3.image = Asset.icNavigationPassword.image
        passwordIcon3.image = passwordIcon.image?.withRenderingMode(.alwaysTemplate)
        passwordIcon3.tintColor = .iconColor
        
    }
    
    private func initButtons() {
        saveButton.setTitle("Save changes", for: .normal)
        saveButton.titleLabel!.font = Font.barlowBold(size: FontSize.medium)
        saveButton.backgroundColor = .lola
        saveButton.layer.cornerRadius = 5
        saveButton.layer.shadowColor = #colorLiteral(red: 0.8, green: 0.7529411765, blue: 0.7764705882, alpha: 1)
        saveButton.layer.shadowRadius = 5
        saveButton.layer.shadowOpacity = 0.5
        saveButton.isEnabled = false
        saveButton.addTarget(self, action: #selector(didPressSaveButton), for: .touchUpInside)
        
        showOldPasswordButton.setImage(Asset.icNavigationShowPassword.image, for: .normal)
        showOldPasswordButton.tintColor = .iconColor
        showOldPasswordButton.addTarget(self, action: #selector(didPressShowOldPassword), for: .touchUpInside)
        
        showNewPasswordButton.setImage(Asset.icNavigationShowPassword.image, for: .normal)
        showNewPasswordButton.tintColor = .iconColor
        showNewPasswordButton.addTarget(self, action: #selector(didPressShowNewPassword), for: .touchUpInside)
        
        showConfirmPasswordButton.setImage(Asset.icNavigationShowPassword.image, for: .normal)
        showConfirmPasswordButton.tintColor = .iconColor
        showConfirmPasswordButton.addTarget(self, action: #selector(didPressShowConfirmPassword), for: .touchUpInside)
        
        forgotPasswordButton.setTitleColor(.black, for: .normal)
        let yourAttributes : [NSAttributedString.Key: Any] = [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Forgot password", attributes: yourAttributes)
        forgotPasswordButton.setAttributedTitle(attributeString, for: .normal)
        forgotPasswordButton.titleLabel!.font = Font.barlowRegular(size: FontSize.mediumSmall)
        forgotPasswordButton.addTarget(self, action: #selector(didPressForgotPassword), for: .touchUpInside)
    }
    
    private func initLabels() {
        oldPasswordLabel.textColor = .white
        newPasswordLabel.textColor = .white
        confirmPasswordLabel.textColor = .white
        
        oldPasswordLabel.text = "Old password"
        newPasswordLabel.text = "New password"
        confirmPasswordLabel.text = "Confirm password"
        
        oldPasswordLabel.font = Font.barlowRegular(size: .mediumSmall)
        newPasswordLabel.font = Font.barlowRegular(size: .mediumSmall)
        confirmPasswordLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        passwordDoesNotMatchLabel.textColor = .white
        passwordDoesNotMatchLabel.font = Font.barlowRegular(size: .mediumSmall)
        passwordDoesNotMatchLabel.numberOfLines = 0
    }
    
    private func setUpStack() {
        oldPasswordStack.addArrangedSubview(passwordIcon)
        oldPasswordStack.addArrangedSubview(oldPasswordTextField)
        oldPasswordStack.addArrangedSubview(showOldPasswordButton)
        
        newPasswordStack.addArrangedSubview(passwordIcon2)
        newPasswordStack.addArrangedSubview(newPasswordTextField)
        newPasswordStack.addArrangedSubview(showNewPasswordButton)
        
        confirmPasswordStack.addArrangedSubview(passwordIcon3)
        confirmPasswordStack.addArrangedSubview(confirmPasswordTextField)
        confirmPasswordStack.addArrangedSubview(showConfirmPasswordButton)
        
        changePasswordStack.addArrangedSubview(oldPasswordLabel)
        changePasswordStack.addArrangedSubview(oldPasswordStack)
        changePasswordStack.addArrangedSubview(underlineOldPassword)
        
        changePasswordStack.addArrangedSubview(newPasswordLabel)
        changePasswordStack.addArrangedSubview(newPasswordStack)
        changePasswordStack.addArrangedSubview(underlineNewPassword)
        
        changePasswordStack.addArrangedSubview(confirmPasswordLabel)
        changePasswordStack.addArrangedSubview(confirmPasswordStack)
        changePasswordStack.addArrangedSubview(underlineConfirmPassword)
        changePasswordStack.addArrangedSubview(passwordDoesNotMatchLabel)
        
        changePasswordStack.addArrangedSubview(saveButton)
        
        oldPasswordStack.axis = .horizontal
        oldPasswordStack.spacing = 50
        oldPasswordStack.distribution = .fillProportionally
        
        newPasswordStack.axis = .horizontal
        newPasswordStack.spacing = 40
        newPasswordStack.distribution = .fillProportionally
        
        confirmPasswordStack.axis = .horizontal
        confirmPasswordStack.spacing = 40
        confirmPasswordStack.distribution = .fillProportionally
        
        changePasswordStack.axis = .vertical
        changePasswordStack.spacing = 10
        changePasswordStack.distribution = .fill
    }
    
    private func setUpContraints() {
        changePasswordStack.translatesAutoresizingMaskIntoConstraints = false
        underlineConfirmPassword.translatesAutoresizingMaskIntoConstraints = false
        underlineNewPassword.translatesAutoresizingMaskIntoConstraints = false
        underlineOldPassword.translatesAutoresizingMaskIntoConstraints = false
        passwordIcon.translatesAutoresizingMaskIntoConstraints = false
        passwordIcon2.translatesAutoresizingMaskIntoConstraints = false
        passwordIcon3.translatesAutoresizingMaskIntoConstraints = false
        showOldPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        showNewPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        showConfirmPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            forgotPasswordButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            forgotPasswordButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            
            changePasswordStack.topAnchor.constraint(equalTo: forgotPasswordButton.bottomAnchor, constant: -10),
            changePasswordStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            changePasswordStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            passwordIcon.widthAnchor.constraint(equalToConstant: 22),
            passwordIcon.heightAnchor.constraint(equalToConstant: 28),
            passwordIcon2.widthAnchor.constraint(equalToConstant: 22),
            passwordIcon2.heightAnchor.constraint(equalToConstant: 28),
            passwordIcon3.widthAnchor.constraint(equalToConstant: 22),
            passwordIcon3.heightAnchor.constraint(equalToConstant: 28),
            showOldPasswordButton.widthAnchor.constraint(equalToConstant: 30),
            showNewPasswordButton.widthAnchor.constraint(equalToConstant: 30),
            showConfirmPasswordButton.widthAnchor.constraint(equalToConstant: 30),
            
            underlineNewPassword.heightAnchor.constraint(equalToConstant: 1),
            underlineOldPassword.heightAnchor.constraint(equalToConstant: 1),
            underlineConfirmPassword.heightAnchor.constraint(equalToConstant: 1),
            
            saveButton.heightAnchor.constraint(equalToConstant: 48)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didPressShowOldPassword() {
        isFirstButtonDown = !isFirstButtonDown
        let image = isFirstButtonDown ? Asset.icNavigationHidePassword.image : Asset.icNavigationShowPassword.image
        showOldPasswordButton.setImage(image, for: .normal)
        oldPasswordTextField.isSecureTextEntry = !oldPasswordTextField.isSecureTextEntry
    }
    
    @objc private func didPressShowNewPassword() {
            isSecondButtonDown = !isSecondButtonDown
            let image = isSecondButtonDown ? Asset.icNavigationHidePassword.image : Asset.icNavigationShowPassword.image
            showNewPasswordButton.setImage(image, for: .normal)
            newPasswordTextField.isSecureTextEntry = !newPasswordTextField.isSecureTextEntry
    }
    
    @objc private func didPressShowConfirmPassword() {
        isThirdButtonDown = !isThirdButtonDown
        let image = isThirdButtonDown ? Asset.icNavigationHidePassword.image : Asset.icNavigationShowPassword.image
        showConfirmPasswordButton.setImage(image, for: .normal)
        confirmPasswordTextField.isSecureTextEntry = !confirmPasswordTextField.isSecureTextEntry
    }
    
    @objc private func didPressForgotPassword() {
        delegate?.didTouchForgotPassword(on: self)
    }
    
    @objc private func backButtonTapped() {
        delegate?.didTouchBackButton(on: self)
    }
    
    @objc private func didPressSaveButton() {
        guard let oldPassword = oldPasswordTextField.text, let newPassword = newPasswordTextField.text, let userId = keyStore.userID else { return }
        let updatePassword = UpdatePasswordDto(newPassword: newPassword, oldPassword: oldPassword)
        authService.changePassword(userId: userId, updatePassword: updatePassword, completion: {
            DispatchQueue.main.async {
                self.delegate?.didChangePasswordSuccedeed(on: self)
            }
        }, failure: { _ in
            DispatchQueue.main.async {
                 self.delegate?.didChangePasswordFailed(on: self)
            }
        })
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == oldPasswordTextField {
            underlineOldPassword.backgroundColor = .primary
            textField.placeholder = ""
            oldPasswordLabel.textColor = .primary
        } else {
            if textField == newPasswordTextField {
                underlineNewPassword.backgroundColor = .primary
                textField.placeholder = ""
                newPasswordLabel.textColor = .primary
                
            } else {
                if textField == confirmPasswordTextField {
                    underlineConfirmPassword.backgroundColor = .primary
                    textField.placeholder = ""
                    confirmPasswordLabel.textColor = .primary
                    if !checkPassword() {
                        confirmPasswordLabel.textColor = .primary
                    } else {
                        confirmPasswordLabel.textColor = .white
                    }
                }
            }
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == oldPasswordTextField {
             underlineOldPassword.backgroundColor = .paleSlate
            if textField.text?.isEmpty == false {
            } else {
                textField.placeholder = "Old password"
                oldPasswordLabel.textColor = .white
            }
        } else {
            if textField == newPasswordTextField {
                underlineNewPassword.backgroundColor = .paleSlate
                if textField.text?.isEmpty == false {
                } else {
                    textField.placeholder = "New password"
                    newPasswordLabel.textColor = .white
                }
            } else {
                if textField == confirmPasswordTextField {
                    underlineConfirmPassword.backgroundColor = .paleSlate
                    if textField.text?.isEmpty == false {
                        
                    } else {
                        textField.placeholder = "Confirm password"
                        confirmPasswordLabel.textColor = .white
                    }
                }
            }
        }
    }
    
    func setupAddTargetIsCorrect() {
        saveButton.isEnabled = false
        saveButton.backgroundColor = .lola
        oldPasswordTextField.addTarget(self, action: #selector(textFieldsCorrect),
                              for: .editingChanged)
        newPasswordTextField.addTarget(self, action: #selector(textFieldsCorrect),
                              for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(textFieldsCorrect),
                                  for: .editingChanged)
    }
    
    @objc func textFieldsCorrect(sender: UITextField) {
        
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        guard
            let old = oldPasswordTextField.text, !old.isEmpty,
            let new = newPasswordTextField.text, !new.isEmpty,
            let confirm = confirmPasswordTextField.text, !confirm.isEmpty,
            checkPassword()
            else {
                self.saveButton.isEnabled = false
                saveButton.backgroundColor = .lola
                
                return
        }
        saveButton.backgroundColor = .primary
        saveButton.isEnabled = true
    }
    
    private func checkPassword() -> Bool {
        guard let newPassword = newPasswordTextField.text, let confirmPass = confirmPasswordTextField.text, newPassword == confirmPass
            else { passwordDoesNotMatchLabel.textColor = .primary
                   passwordDoesNotMatchLabel.text = "Password does not match"
                   return false
                }
        if 6 <= newPassword.count && newPassword.matches("[A-Z]") && newPassword.matches("[a-z]") && newPassword.matches("[0-9]") {
            passwordDoesNotMatchLabel.textColor = .white
            return true
            
        }
        passwordDoesNotMatchLabel.textColor = .primary
        passwordDoesNotMatchLabel.text = "The password should contain one big letter, one small,\n one digit and at least 6 characters"
        return false
    }
    
}
extension String {
    func didMatch(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
