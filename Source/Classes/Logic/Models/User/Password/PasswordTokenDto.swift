//
//  PasswordTokenDto.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation

class PasswordTokenDto: Codable {
    let token: String
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
    }
}

