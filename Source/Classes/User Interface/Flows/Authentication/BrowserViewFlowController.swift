//
//  PrivacyPolicyFlowController.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

class BrowserViewFlowController: NavigationFlowController {
    
    private let browserType: BrowserType
    
    init(browserType: BrowserType, from: FlowController) {
        self.browserType = browserType
        super.init(from: from)
    }
    
    required init(from parent: FlowController?) {
        fatalError("init(from:) has not been implemented")
    }
    
    override var firstScreen: UIViewController {
        let browserViewController = BrowserViewController(browserType: browserType)
        browserViewController.delegate = self
        return browserViewController
    }
}

extension BrowserViewFlowController: BrowserViewControllerDelegate {
    func close() {
        navigationController.popViewController(animated: true)
    }
}
