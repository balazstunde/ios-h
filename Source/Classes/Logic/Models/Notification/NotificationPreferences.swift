//
//  NotificationPreferences.swift
//  Choo Choo
//
//  Created by Halcyon on 21/08/2019.
//

import Foundation
class NotificationPreferences: Codable {
    var changedPlatformNotificationEnabled = false
    var marketingNotificationEnabled = false
    var pushNotificationEnabled = false
    var trainArrivingIntoStationNotificationEnabled = false
    var tripDelayNotificationEnabled = false
    var tripDisruptionNotificationEnabled = false
}
