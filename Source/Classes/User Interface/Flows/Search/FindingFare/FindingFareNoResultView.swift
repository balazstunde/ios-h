//
//  FindingFareNoResultView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 08/08/2019.
//

import UIKit

class FindingFareNoResultView: UIView {
    
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let searchLabel = UILabel()
    
    var didTouchedSearchAgainButton: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        imageView.image = Asset.noFare.image
        
        titleLabel.text = "We are sorry but we found no results"
        titleLabel.font = Font.barlowRegular(size: .mediumLarge)
        
        searchLabel.text = "Search for something else"
        searchLabel.font = Font.barlowSemiBold(size: .mediumExtra)
        searchLabel.textColor = .shiraz
        
        let searchLabelTouched = UITapGestureRecognizer(target: self, action: #selector(didTouchSearchLabel))
        searchLabel.isUserInteractionEnabled = true
        searchLabel.addGestureRecognizer(searchLabelTouched)

        addSubview(searchLabel)
        addSubview(imageView)
        addSubview(titleLabel)
    }
    
    private func setupConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        searchLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -PaddingValues.bigPadding),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Style.imageHeight),
            
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: PaddingValues.veryBigPadding),
            
            searchLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            searchLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: PaddingValues.smallPadding)
        ])
    }
    
    @objc private func didTouchSearchLabel() {
        didTouchedSearchAgainButton?()
    }
}

extension FindingFareNoResultView {
    private enum PaddingValues {
        static let smallPadding: CGFloat = 6.0
        static let bigPadding: CGFloat = 24.0
        static let veryBigPadding: CGFloat = 32.0
    }
    private enum Style {
        static let imageHeight: CGFloat = 160
    }
}

