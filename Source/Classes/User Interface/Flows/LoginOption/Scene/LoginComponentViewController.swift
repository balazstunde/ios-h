//
//  BaseLoginViewController.swift
//  Choo Choo
//
//  Created by halcyonmobile on 13/08/2019.
//

import UIKit

protocol LoginComponentViewControllerFlowDelegate: AnyObject {
    func didTouchLogInButton(on viewController: LoginComponentViewController)
    func didTouchCreateAccountButton(on viewController: LoginComponentViewController)
    func didTouchTermButton(on viewController: LoginComponentViewController)
    func didTouchPrivacyButton(on viewController: LoginComponentViewController)
}

class LoginComponentViewController: UIViewController {
    private let loginButton = UIButton()
    private let createAccountStackView = UIStackView()
    private let dontHaveAccountLabel = UILabel()
    private let createAccountButton = UIButton()
    private let byCreatingAccountLabel = UILabel()
    private let termsPrivacyStackView = UIStackView()
    private let andLabel = UILabel()
    private let privacyButton = UIButton()
    private let termsButton = UIButton()
    weak var delegate: LoginComponentViewControllerFlowDelegate?
    private let createAccountButtonAttributes = [
        NSAttributedString.Key.underlineStyle: 1,
        NSAttributedString.Key.foregroundColor: UIColor.shiraz,
        NSAttributedString.Key.font: Font.barlowBold(size: .mediumSmall)
        ] as [NSAttributedString.Key : Any]
    private let termsAndPolicyButtonAttributes = [
        NSAttributedString.Key.foregroundColor: UIColor.shiraz,
        NSAttributedString.Key.font: Font.barlowRegular(size: .small)
        ] as [NSAttributedString.Key : Any]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setUpConstraints()
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        navigationItem.title = "My Account"
        
        loginButton.setTitle("Log In", for: .normal)
        loginButton.titleLabel?.font = Font.barlowBold(size: .mediumSmall)
        loginButton.backgroundColor = .shiraz
        loginButton.layer.cornerRadius = 3
        loginButton.addTarget(self, action: #selector(loginButtonTouched), for: .touchUpInside)
        
        dontHaveAccountLabel.text = "Don't have an account?"
        dontHaveAccountLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        let createAccountButtonText = NSMutableAttributedString(string: "Create Account", attributes: createAccountButtonAttributes)
        createAccountButton.setAttributedTitle(createAccountButtonText, for: .normal)
        createAccountButton.addTarget(self, action: #selector(createAccountButtonTouched), for: .touchUpInside)
        
        createAccountStackView.addArrangedSubview(dontHaveAccountLabel)
        createAccountStackView.addArrangedSubview(createAccountButton)
        createAccountStackView.alignment = .center
        createAccountStackView.axis = .horizontal
        createAccountStackView.spacing = 1
        
        byCreatingAccountLabel.text = "By creating an account you agree to our"
        byCreatingAccountLabel.font = Font.barlowRegular(size: .small)
        
        let privacyPolicyButtonText = NSMutableAttributedString(string: "Privacy Policy", attributes: termsAndPolicyButtonAttributes)
        let termsButtonText = NSMutableAttributedString(string: "Terms of Use", attributes: termsAndPolicyButtonAttributes)
        
        termsButton.setAttributedTitle(termsButtonText, for: .normal)
        termsButton.addTarget(self, action: #selector(termButtonTouched), for: .touchUpInside)
        privacyButton.setAttributedTitle(privacyPolicyButtonText, for: .normal)
        privacyButton.addTarget(self, action: #selector(privacyButtonTouched), for: .touchUpInside)
        
        andLabel.text = "and"
        andLabel.font = Font.barlowRegular(size: .small)
        
        termsPrivacyStackView.addArrangedSubview(termsButton)
        termsPrivacyStackView.addArrangedSubview(andLabel)
        termsPrivacyStackView.addArrangedSubview(privacyButton)
        termsPrivacyStackView.alignment = .center
        termsPrivacyStackView.axis = .horizontal
        termsPrivacyStackView.spacing = 1
        
        view.addSubview(loginButton)
        view.addSubview(createAccountStackView)
        view.addSubview(byCreatingAccountLabel)
        view.addSubview(termsPrivacyStackView)
    }
    
    @objc func loginButtonTouched() {
        delegate?.didTouchLogInButton(on: self)
    }
    
    @objc func createAccountButtonTouched() {
        delegate?.didTouchCreateAccountButton(on: self)
    }
    
    @objc func termButtonTouched() {
        delegate?.didTouchTermButton(on: self)
    }
    
    @objc func privacyButtonTouched() {
        delegate?.didTouchPrivacyButton(on: self)
    }
    
    private func setUpConstraints() {
        var constraints = [NSLayoutConstraint] ()
        
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(loginButton.topAnchor.constraint(lessThanOrEqualTo: view.topAnchor))
        constraints.append(loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        constraints.append(loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor))
        constraints.append(loginButton.heightAnchor.constraint(equalToConstant: 50))
        
        createAccountStackView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(createAccountStackView.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: Padding.big))
        constraints.append(createAccountStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        byCreatingAccountLabel.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(byCreatingAccountLabel.topAnchor.constraint(equalTo: createAccountStackView.bottomAnchor, constant: Padding.extraLarge))
        constraints.append(byCreatingAccountLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        termsPrivacyStackView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(termsPrivacyStackView.topAnchor.constraint(equalTo: byCreatingAccountLabel.bottomAnchor))
        constraints.append(termsPrivacyStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        constraints.append(termsPrivacyStackView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor))
        
        NSLayoutConstraint.activate(constraints)
    }
}
