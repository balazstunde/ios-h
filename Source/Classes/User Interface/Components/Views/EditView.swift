//
//  EditView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

protocol EditViewDelegate: AnyObject {
    func didTouchTextField()
    func didTouchEdit()
}

enum EditViewType {
    case password
    case email
    case name
    case card
    case date
    case ccv
    case cardHolder
}

private struct SizeImage {
    static let imageSizeSmall: CGFloat = 22
    static let imageSizeBig: CGFloat = 28
}

class EditView: UIView {
    let label = UILabel()
    let textField = UITextField()
    private let image = UIImageView()
    private let underline = UIView()
    private var horizontalStack = UIStackView()
    private var verticalStack = UIStackView()
    private var button = UIButton()
    private var viewType: EditViewType
    weak var delegate: EditViewDelegate?
    
    var text: String? {
        set {
            textField.text = newValue
        }
        get {
            return textField.text
        }
    }
    
    var isEditAllowed: Bool = false {
        didSet {
            button.isHidden = !isEditAllowed
        }
    }
    
    init(viewType: EditViewType) {
        self.viewType = viewType
        super.init(frame: .zero)
        setUpView()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpView() {
        button.backgroundColor = .white
        button.setTitle("Edit", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = Font.barlowMedium(size: .medium)
        button.addTarget(self, action: #selector(didTouchEditButton), for: .touchUpInside)
        button.isHidden = true
        underline.backgroundColor = .paleSlate
        textField.font = Font.barlowRegular(size: FontSize.medium)
        textField.textColor = .black
        textField.text = text
        image.image = image.image?.withRenderingMode(.alwaysTemplate)
        image.tintColor = .iconColor
        if textField.text != "" {
            label.textColor = .selected
        } else {
            label.textColor = .white
        }
        label.contentMode = .scaleAspectFit
        
        label.font = Font.barlowRegular(size: .mediumSmall)
        
        switch viewType {
        case .name:
            image.image = Asset.icNavigationProfileDisabled.image
            label.text = "First name"
            button.isEnabled = false
        case .email:
            image.image = Asset.icNavigationEmail.image
            label.text = "Email"
            button.isEnabled = false
            textField.autocapitalizationType = .none
        case .password:
            image.image = Asset.icNavigationPassword.image
            label.text = "Password"
            textField.isSecureTextEntry = true
            textField.isEnabled = false
        case .card:
            image.image = Asset.iconPayment.image
            label.text = "Card number"
            textField.placeholder = "Card number"
            textField.keyboardType = .numberPad
            textField.textContentType = .creditCardNumber
            button.setTitle("", for: .normal)
            button.setImage(Asset.visaImage.image, for: .normal)
            button.isEnabled = false
        case .date:
            image.image = Asset.icNavigationDatePicker.image
            label.text = "Valid until"
            textField.placeholder = "Valid until"
            button.isEnabled = false
        case .ccv:
            image.image = Asset.icNavigationPassword.image
            label.text = "CVV"
            textField.placeholder = "CVV"
            textField.isSecureTextEntry = true
            textField.keyboardType = .numberPad
            textField.textContentType = .password
            button.isEnabled = false
        case .cardHolder:
            image.image = Asset.icNavigationProfileDisabled.image
            label.text = "Cardholder name"
            textField.placeholder = "Cardholder name"
            button.isEnabled = false
        }
        
        horizontalStack.setupStackView(subviews: [image, textField, button], axis: .horizontal, distribution: .fillProportionally, spacing: 25)
        
        verticalStack.setupStackView(subviews: [label, horizontalStack], axis: .vertical, distribution: .fill, spacing: 10)
        
        textField.addTarget(self, action: #selector(onTap), for: .editingChanged)
        textField.delegate = self
        addSubview(verticalStack)
        addSubview(underline)
    }
    
    private func setUpConstraints() {
        verticalStack.translatesAutoresizingMaskIntoConstraints = false
        image.translatesAutoresizingMaskIntoConstraints = false
        underline.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        textField.translatesAutoresizingMaskIntoConstraints = false
        var constraints:[NSLayoutConstraint] = [
            underline.heightAnchor.constraint(equalToConstant: 1),
            image.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Padding.mediumBig),
            underline.topAnchor.constraint(equalTo: verticalStack.bottomAnchor, constant: Padding.mediumSmall),
            underline.bottomAnchor.constraint(equalTo: bottomAnchor),
            underline.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: Padding.mediumBig),
            underline.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Padding.mediumBig),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Padding.mediumBig)
        ]
        
        switch viewType {
        case .name, .cardHolder:
            constraints.append(image.widthAnchor.constraint(equalToConstant: SizeImage.imageSizeSmall))
            constraints.append(image.heightAnchor.constraint(equalToConstant: SizeImage.imageSizeBig))
        case .email, .card, .date:
            constraints.append(image.widthAnchor.constraint(equalToConstant: SizeImage.imageSizeBig))
            constraints.append(image.heightAnchor.constraint(equalToConstant: SizeImage.imageSizeSmall))
        case .password, .ccv:
            constraints.append(image.heightAnchor.constraint(equalToConstant: SizeImage.imageSizeBig))
            constraints.append(image.widthAnchor.constraint(equalToConstant: SizeImage.imageSizeSmall))
        }
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc
    private func onTap() {
        delegate?.didTouchTextField()
    }
    
    @objc
    private func didTouchEditButton() {
        delegate?.didTouchEdit()
    }
}
extension EditView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        underline.backgroundColor = .primary
        underline.frame.size.height = 2.0
        textField.placeholder = ""
        label.textColor = .primary
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        underline.backgroundColor = .paleSlate
        underline.frame.size.height = 1
        if textField.text?.isEmpty == true {
            textField.placeholder = label.text
            label.textColor = .white
        }
    }
}
