//
//  TicketService.swift
//  Choo Choo
//
//  Created by halcyonmobile on 07/08/2019.
//

enum Flag {
    case UPCOMING
    case PREVIOUS
}

struct GetTicketRequest: RequestRepresentable {
    let paramType: PostParamType = .body

    var suffix: String = "/tickets"

    var method: HTTPMethod = .get

    var parameters: Parameters?

    init(flag: Flag) {
        self.parameters = ["flag": flag]
    }
}

class TicketService {
    func getTickets(flag: Flag, completion: @escaping ([Ticket]) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: GetTicketRequest(flag: flag), success: { (tickets: [Ticket]) in
            completion(tickets)
        }, failure: { error in
            failure(error)

        })
    }
}
