//
//  PrivacyPolicyController.swift
//  Choo Choo
//
//  Created by Halcyon on 31/07/2019.
//

import UIKit

protocol BrowserViewControllerDelegate: AnyObject {
    func close()
}

enum BrowserType {
    case terms
    case privacy
    
    var title: String {
        switch self {
        case .terms:
            return "Terms and Conditions"
        case .privacy:
            return "Privacy and Policy"
        }
    }
    
    var url: URL {
        switch self {
        case .terms:
            return URLs.termsUrl
        case .privacy:
            return URLs.privacyPolicyUrl
        }
    }
}

class BrowserViewController: UIViewController {
    
    private var privacyContent: UIWebView!
    private let browserType: BrowserType
    weak var delegate: BrowserViewControllerDelegate?
    
    init(browserType: BrowserType) {
        self.browserType = browserType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        initConstraints()
    }
    
    private func initUI() {
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain, target: self, action: #selector(close))
        navigationItem.title = browserType.title
        
        privacyContent = UIWebView()
        privacyContent.loadRequest(URLRequest(url: browserType.url))
        
        view.addSubview(privacyContent)
    }
    
    private func initConstraints() {
        privacyContent.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            privacyContent.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            privacyContent.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            privacyContent.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            privacyContent.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func close() {
        navigationController?.popViewController(animated: true)
    }
}
