//
//  FareOverviewViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

class FareOverviewViewController: BaseViewController {
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let selectedFareDetailsView = SelectedFareDetailsView()
    private var seeAllStopsView: SeeAllStopsView?
    private let journey: Journey
    private var contentViewHeightConstraint: NSLayoutConstraint?
    private var initialSize: CGFloat = 0
    
    init(journey: Journey) {
        self.journey = journey
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    private func setupView() {
        seeAllStopsView = SeeAllStopsView(withFare: journey)
        seeAllStopsView?.delegate = self
        selectedFareDetailsView.configure(withFare: journey)
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(selectedFareDetailsView)
        contentView.addSubview(seeAllStopsView!)
        
        view.backgroundColor = .white
        navigationItem.title = "Connections"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain, target: self, action: #selector(didPressBackButton))
    }
    
    private func setupConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        selectedFareDetailsView.translatesAutoresizingMaskIntoConstraints = false
        seeAllStopsView?.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        contentViewHeightConstraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        contentViewHeightConstraint?.priority = UILayoutPriority(rawValue: 500)
        
        var constraints = [
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            selectedFareDetailsView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: AppStyle.Layout.padding),
            selectedFareDetailsView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: AppStyle.Layout.padding),
            selectedFareDetailsView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -AppStyle.Layout.padding),
            
            seeAllStopsView!.topAnchor.constraint(equalTo: selectedFareDetailsView.bottomAnchor, constant: Layout.viewPadding),
            seeAllStopsView!.bottomAnchor.constraint(greaterThanOrEqualTo: contentView.bottomAnchor, constant: -AppStyle.Layout.padding),
            seeAllStopsView!.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: AppStyle.Layout.padding),
            seeAllStopsView!.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -AppStyle.Layout.padding)
        ]
        constraints.append(contentViewHeightConstraint!)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didPressBackButton() {
        navigationController?.popViewController(animated: true)
    }
}

extension FareOverviewViewController: SeeAllStopsViewDelegate {
    func didCollapseSection(tableViewHeight: CGFloat) {
        let offset = tableViewHeight + (seeAllStopsView?.frame.origin.y ?? 0) + 200 - contentView.frame.height
        contentViewHeightConstraint?.constant = offset > 0 ? offset : 0
        view.layoutIfNeeded()
    }
}

extension FareOverviewViewController {
    enum Layout {
        static let viewPadding: CGFloat = 10
    }
}
