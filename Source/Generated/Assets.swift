// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let header = ImageAsset(name: "Header")
  internal static let iconLogout = ImageAsset(name: "icon_logout")
  internal static let iconNext = ImageAsset(name: "icon_next")
  internal static let iconNofication = ImageAsset(name: "icon_nofication")
  internal static let iconPayment = ImageAsset(name: "icon_payment")
  internal static let iconPrivacy = ImageAsset(name: "icon_privacy")
  internal static let iconSettings = ImageAsset(name: "icon_settings")
  internal static let iconSupport = ImageAsset(name: "icon_support")
  internal static let iconTerms = ImageAsset(name: "icon_terms")
  internal static let noFare = ImageAsset(name: "NoFare")
  internal static let splash = ImageAsset(name: "Splash")
  internal static let visaImage = ImageAsset(name: "VisaImage")
  internal static let checkImage = ImageAsset(name: "checkImage")
  internal static let icBarcode = ImageAsset(name: "ic-barcode")
  internal static let icBigTicketBackground = ImageAsset(name: "ic-big-ticket-background")
  internal static let icPinnedDestination = ImageAsset(name: "ic-pinned-destination")
  internal static let icNoPastTripsEnvelope = ImageAsset(name: "ic_NoPastTripsEnvelope")
  internal static let icChecked = ImageAsset(name: "ic_checked")
  internal static let icDots = ImageAsset(name: "ic_dots")
  internal static let icLandingPageWithDestination = ImageAsset(name: "ic_landing-page_with_destination")
  internal static let icLandingPageWithoutDestination = ImageAsset(name: "ic_landing-page_without_destination")
  internal static let icLoginoption = ImageAsset(name: "ic_loginoption")
  internal static let icNavigationBack = ImageAsset(name: "ic_navigation_back")
  internal static let icNavigationBike = ImageAsset(name: "ic_navigation_bike")
  internal static let icNavigationCheck = ImageAsset(name: "ic_navigation_check")
  internal static let icNavigationCheckMark = ImageAsset(name: "ic_navigation_check_mark")
  internal static let icNavigationClass = ImageAsset(name: "ic_navigation_class")
  internal static let icNavigationCloseEnabled = ImageAsset(name: "ic_navigation_close-enabled")
  internal static let icNavigationCompassEnabled = ImageAsset(name: "ic_navigation_compass-enabled")
  internal static let icNavigationDatePicker = ImageAsset(name: "ic_navigation_date_picker")
  internal static let icNavigationDownArrow = ImageAsset(name: "ic_navigation_down_arrow")
  internal static let icNavigationEmail = ImageAsset(name: "ic_navigation_email")
  internal static let icNavigationHidePassword = ImageAsset(name: "ic_navigation_hide_password")
  internal static let icNavigationHomeDisabled = ImageAsset(name: "ic_navigation_home_disabled")
  internal static let icNavigationHomeEnabled = ImageAsset(name: "ic_navigation_home_enabled")
  internal static let icNavigationLocationEnabled = ImageAsset(name: "ic_navigation_location-enabled")
  internal static let icNavigationPassword = ImageAsset(name: "ic_navigation_password")
  internal static let icNavigationPinFill = ImageAsset(name: "ic_navigation_pin_fill")
  internal static let icNavigationPower = ImageAsset(name: "ic_navigation_power")
  internal static let icNavigationProfileDisabled = ImageAsset(name: "ic_navigation_profile_disabled")
  internal static let icNavigationProfileEnabled = ImageAsset(name: "ic_navigation_profile_enabled")
  internal static let icNavigationRestaurant = ImageAsset(name: "ic_navigation_restaurant")
  internal static let icNavigationShowPassword = ImageAsset(name: "ic_navigation_show_password")
  internal static let icNavigationSleeping = ImageAsset(name: "ic_navigation_sleeping")
  internal static let icNavigationTicketDisabled = ImageAsset(name: "ic_navigation_ticket_disabled")
  internal static let icNavigationTicketEnabled = ImageAsset(name: "ic_navigation_ticket_enabled")
  internal static let icNavigationTrain = ImageAsset(name: "ic_navigation_train")
  internal static let icNavigationUpArrow = ImageAsset(name: "ic_navigation_up_arrow")
  internal static let icNavigationWifi = ImageAsset(name: "ic_navigation_wifi")
  internal static let icNoUpcomingTickets = ImageAsset(name: "ic_noUpcomingTickets")
  internal static let icRestrictedAuthentication = ImageAsset(name: "ic_restricted_authentication")
  internal static let icSwapButton = ImageAsset(name: "ic_swap_button")
  internal static let icTicketDetailsBike = ImageAsset(name: "ic_ticket-details_bike")
  internal static let icTicketDetailsBus = ImageAsset(name: "ic_ticket-details_bus")
  internal static let icTicketDetailsTaxi = ImageAsset(name: "ic_ticket-details_taxi")
  internal static let icTicketDetailsTram = ImageAsset(name: "ic_ticket-details_tram")
  internal static let icTicketDetailsWalk = ImageAsset(name: "ic_ticket-details_walk")
  internal static let icTicketBackground = ImageAsset(name: "ic_ticketBackground")
  internal static let iconClose = ImageAsset(name: "icon_close")
  internal static let iconNavigation = ImageAsset(name: "icon_navigation")
  internal static let lock = ImageAsset(name: "lock")
  internal static let ticketBackground = ImageAsset(name: "ticket_background")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct DataAsset {
  internal fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  internal var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
