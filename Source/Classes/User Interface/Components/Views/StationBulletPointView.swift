//
//  StationBulletPointView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

class StationBulletPointView: UIView {
    private let circleView = UIView()
    private let lineView = UIView()
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(circleView)
        addSubview(lineView)
        
        circleView.backgroundColor = .switchButtonColor
        circleView.layer.cornerRadius = Style.cornerRadius
        lineView.backgroundColor = .switchButtonColor
    }
    
    private func setupConstraints() {
        circleView.translatesAutoresizingMaskIntoConstraints = false
        lineView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            lineView.topAnchor.constraint(equalTo: topAnchor),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.centerXAnchor.constraint(equalTo: centerXAnchor),
            lineView.widthAnchor.constraint(equalToConstant: Layout.lineWidth),
            
            circleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            circleView.centerYAnchor.constraint(equalTo: centerYAnchor),
            circleView.heightAnchor.constraint(equalToConstant: Layout.circleHeight),
            circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor),
            circleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            circleView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension StationBulletPointView {
    enum Layout {
        static let circleHeight: CGFloat = 7
        static let lineWidth: CGFloat = 1
    }
    
    enum Style {
        static let cornerRadius: CGFloat = 3.5
    }
}
