//
//  Authentication.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 20/08/2019.
//

import Foundation

struct Authentication: Codable {
    let accessToken: String
    let expiresIn: Int
    let refreshToken: String
    let scope: String
    let tokenType: String
    let userId: Int
}
