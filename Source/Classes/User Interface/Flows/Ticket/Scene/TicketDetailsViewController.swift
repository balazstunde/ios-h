//
//  TicketDetailsViewController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 09/08/2019.
//

import UIKit

protocol TicketDetailsViewControllerDelegate: AnyObject {
    func didPressBackButton(on viewController: TicketDetailsViewController)
}

class TicketDetailsViewController: BaseViewController {
    weak var delegate: TicketDetailsViewControllerDelegate?
    private let ticketDetailsView: TicketDetailsView
    private let destinationFacilitiesView: DestinationFacilitiesView
    private let destinationFacilitiesLabel = UILabel()
    private let scrollView = UIScrollView()
    private enum LayoutValues {
        static let padding10: CGFloat = 10.0
        static let padding15: CGFloat = 15.0
        static let padding20: CGFloat = 20.0
        static let padding30: CGFloat = 30.0
    }
    
    init(ticket: Ticket) {
        ticketDetailsView = TicketDetailsView(with: ticket)
        destinationFacilitiesView = DestinationFacilitiesView(destination: ticket.endTrainStopDto.stopName)
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavBar()
        setupConstraints()
    }
    
    private func setupView() {
        view.addSubview(scrollView)
        scrollView.addSubview(ticketDetailsView)
        scrollView.addSubview(destinationFacilitiesView)
        scrollView.addSubview(destinationFacilitiesLabel)
        view.backgroundColor = .white
        
        destinationFacilitiesLabel.text = "Destination facilities"
        destinationFacilitiesLabel.font = Font.barlowBold(size: .mediumExtra)
    }
    
    private func setupNavBar() {
        let backButtonImage = Asset.icNavigationBack.image
        backButtonImage.withRenderingMode(.alwaysTemplate)
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action: #selector(didPressBackButton))
        
        navigationController?.navigationBar.barTintColor = .primary
        navigationItem.title = "Ticket Details"
        navigationItem.leftBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
    }
    
    private func setupConstraints() {
        ticketDetailsView.translatesAutoresizingMaskIntoConstraints = false
        destinationFacilitiesLabel.translatesAutoresizingMaskIntoConstraints = false
        destinationFacilitiesView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            ticketDetailsView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            ticketDetailsView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            ticketDetailsView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            
            destinationFacilitiesLabel.topAnchor.constraint(equalTo: ticketDetailsView.bottomAnchor, constant: AppStyle.PaddingValues.mediumSmallPadding),
            destinationFacilitiesLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: AppStyle.PaddingValues.mediumPadding),    //era 15
            
            destinationFacilitiesView.topAnchor.constraint(equalTo: destinationFacilitiesLabel.bottomAnchor, constant: AppStyle.Layout.padding),
            destinationFacilitiesView.leadingAnchor.constraint(equalTo: ticketDetailsView.leadingAnchor, constant: AppStyle.PaddingValues.mediumPadding),
            destinationFacilitiesView.trailingAnchor.constraint(equalTo: ticketDetailsView.trailingAnchor, constant: -AppStyle.PaddingValues.mediumPadding),
            destinationFacilitiesView.bottomAnchor.constraint(greaterThanOrEqualTo: scrollView.bottomAnchor, constant: -AppStyle.Layout.padding),
            destinationFacilitiesView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -AppStyle.PaddingValues.veryBigPadding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didPressBackButton() {
        delegate?.didPressBackButton(on: self)
    }
}
