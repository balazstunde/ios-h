//
//  Notifications.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 07/02/2019.
//

import Foundation

extension Notification.Name {
    // TODO: Remove
    static let didUpdateSomething = Notification.Name("didUpdateSomething")
    static let didLogin = NSNotification.Name("userDidLogin")
}
