//
//  BaseViewController.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 5/17/17.
//  Copyright © 2017 HalcyonMobile. All rights reserved.
//

import UIKit

// NOTE: Subclass each view controller from this class

class BaseViewController: UIViewController {
    
    // MARK: - Lifecycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setNavigationBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setNavigationBar() {
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.leftBarButtonItem?.tintColor = .white
    }
}
