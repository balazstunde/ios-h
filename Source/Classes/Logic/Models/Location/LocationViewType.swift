//
//  LocationType.swift
//  Choo Choo
//
//  Created by Halcyon on 12/08/2019.
//

import Foundation

enum LocationViewType {
    case from
    case destination
    
    var title: String {
        switch self {
        case .from:
            return "From"
        case .destination:
            return "To"
        }
    }
    
    var placeholder: String {
        switch self {
        case .from:
            return "Your current location.."
        case .destination:
            return "Enter destination.."
        }
    }
}
