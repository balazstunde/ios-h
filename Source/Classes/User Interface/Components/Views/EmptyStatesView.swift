import UIKit
import AVFoundation

class EmptyStateView: UIView {

    enum `Type` {
        case upcomingTrip, previousTrip, notLoggedIn

        var title: String {
            switch self {
            case .upcomingTrip:
                return "You have no tickets yet"
            case .previousTrip:
                return "Your tickets will be stored here"
            case .notLoggedIn:
                return "You are not logged in"
            }
        }

        var shouldShowButton: Bool {
            return self == .upcomingTrip
        }
    }

    private let noTicketsLabel = UILabel()
    private let imageView = UIImageView()
    private var planeTripButton = UIButton()
    public var type: Type = .notLoggedIn

    var didTouchPlanLabel: (() -> Void)?
    
    init(tripType: Type) {
        type = tripType
        super.init(frame: .zero)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        noTicketsLabel.text = type.title
        planeTripButton.isHidden = !type.shouldShowButton
        if type == .upcomingTrip {
            imageView.image = Asset.icNoUpcomingTickets.image
        } else if type == .previousTrip {
            imageView.image = Asset.icNoPastTripsEnvelope.image
        } else {
            imageView.image = Asset.icRestrictedAuthentication.image
        }
        planeTripButton.setTitle("Why not plan a trip?", for: .normal)
        planeTripButton.setTitleColor(.secondary, for: .normal)
        planeTripButton.titleLabel?.font = Font.barlowBold(size: .medium)
        planeTripButton.addTarget(self, action: #selector(didPlanTripPressed), for: .touchUpInside)
        addSubview(imageView)
        addSubview(noTicketsLabel)
        addSubview(planeTripButton)
    }
    
    @objc private func didPlanTripPressed() {
        didTouchPlanLabel?()
    }
    
    private func setupConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        noTicketsLabel.translatesAutoresizingMaskIntoConstraints = false
        planeTripButton.translatesAutoresizingMaskIntoConstraints = false
        var constraints: [NSLayoutConstraint] = []

        constraints.append(imageView.centerXAnchor.constraint(equalTo: centerXAnchor))
        constraints.append(imageView.topAnchor.constraint(equalTo: topAnchor, constant: LayoutConstants.customTop))
        constraints.append(imageView.widthAnchor.constraint(equalToConstant: LayoutConstants.customWidth))
        constraints.append(imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor))
        constraints.append(noTicketsLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: LayoutConstants.space))
        constraints.append(noTicketsLabel.centerXAnchor.constraint(equalTo: centerXAnchor))
        constraints.append(planeTripButton.topAnchor.constraint(equalTo: noTicketsLabel.safeAreaLayoutGuide.bottomAnchor, constant: LayoutConstants.customSpace))
        constraints.append(planeTripButton.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor))
        constraints.append(planeTripButton.centerXAnchor.constraint(equalTo: centerXAnchor))
        NSLayoutConstraint.activate(constraints)
    }
}

private enum LayoutConstants {
    static let customTop: CGFloat = 120
    static let customWidth: CGFloat = 170
    static let customSpace: CGFloat = 12
    static let space: CGFloat = 27
    static let customNoPreviousTrips: CGFloat = 80
    static let customLeading: CGFloat = 118
    static let customPlanTripLabel: CGFloat = 130
}
