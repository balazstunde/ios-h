//
//  TicketDetailsView.swift
//  Choo Choo
//
//  Created by Halcyon on 12/08/2019.
//

import Foundation
import UIKit

class TicketDetailsHeaderView: UIView {
    private let widthSize: CGFloat
    private let ticketTopDetailsView = TicketTopDetailsView()
    private var dashedView: DashedView!
    private let ticketBottomDetailsView = TicketBottomDetailsView()
    private let ticketPriceView = TicketPriceView()
    
    var didTapSeeStopsButton: (() -> Void)?
    
    init(widthSize: CGFloat) {
        self.widthSize = widthSize
        super.init(frame: .zero)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTicketTopDetailsView(trainName: String, totalTime: String, date: String, time: String, fromTo: String) {
        ticketTopDetailsView.setDetails(trainName: trainName, totalTime: totalTime, date: date, time: time, fromTo: fromTo)
    }
    
    func setupTicketBottomDetailsView(trainClass: String) {
        ticketBottomDetailsView.setDetails(trainClass: trainClass)
    }
    
    func setupTicketPriceView(passenger: String, price: String, totalPrice: String) {
        ticketPriceView.setDetails(passenger: passenger, price: price, totalPrice: totalPrice)

    }
    
    private func setupView() {
        dashedView = DashedView()
        
        backgroundColor = .alabaster
        addSubview(ticketTopDetailsView)
        addSubview(dashedView)
        addSubview(ticketBottomDetailsView)
        addSubview(ticketPriceView)
        
        ticketTopDetailsView.didTapSeeStopsButtonClosure = {
            self.didTapSeeStopsButton?()
        }
    }
    
    private func setupConstraints() {
        ticketTopDetailsView.translatesAutoresizingMaskIntoConstraints = false
        dashedView.translatesAutoresizingMaskIntoConstraints = false
        ticketBottomDetailsView.translatesAutoresizingMaskIntoConstraints = false
        ticketPriceView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            ticketTopDetailsView.topAnchor.constraint(equalTo: topAnchor, constant: LayoutValues.ticketTopPadding),
            ticketTopDetailsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            ticketTopDetailsView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            
            dashedView.topAnchor.constraint(equalTo: ticketTopDetailsView.bottomAnchor),
            dashedView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            dashedView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            dashedView.heightAnchor.constraint(equalToConstant: LayoutValues.dashedViewHeight),
            
            ticketBottomDetailsView.topAnchor.constraint(equalTo: dashedView.bottomAnchor),
            ticketBottomDetailsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            ticketBottomDetailsView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            
            ticketPriceView.topAnchor.constraint(equalTo: ticketBottomDetailsView.bottomAnchor, constant: LayoutValues.priceViewTopPadding),
            ticketPriceView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            ticketPriceView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            ticketPriceView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -LayoutValues.priceViewBottomPadding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func getDateTimeLabelText() -> String {
        return ticketTopDetailsView.getDateTimeLabelText()
    }
    
    public func getTrainNumberLabelText() -> String {
        return ticketTopDetailsView.getTrainNumberLabelText()
    }
    
    func updateDashedView() {
        dashedView.updateView()
    }
}

extension TicketDetailsHeaderView {
    private enum LayoutValues {
        static let marginPadding: CGFloat = 16
        static let ticketTopPadding: CGFloat = 24
        static let priceViewTopPadding: CGFloat = 10
        static let priceViewBottomPadding: CGFloat = 32
        static let dashedViewHeight: CGFloat = 20
    }
}
