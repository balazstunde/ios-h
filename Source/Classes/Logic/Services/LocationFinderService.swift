//
//  LocationFinderService.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 09/08/2019.
//

import Foundation

struct GetLocationRequest: RequestRepresentable {
    var suffix: String = URLs.locationSuffix
    var method: HTTPMethod = .get
    var parameters: Parameters?
    var paramType: PostParamType = .body
    
    init(latitude: Double, longitude: Double) {
        parameters = [
            "latitude": latitude,
            "longitude": longitude
        ]
    }
}

class LocationFinderService {
    func getClosestStation(latitude: Double, longitude: Double, completion: @escaping ([Station]) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: GetLocationRequest(latitude: latitude, longitude: longitude), success: completion, failure: failure)
    }
}
