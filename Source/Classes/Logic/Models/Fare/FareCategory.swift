//
//  FareCategory.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import Foundation

struct FareCategory: Codable {
    let description: String
    let name: FareUser
    let discountPercentage: Double
    let title: String
    
}

extension FareCategory {
    var type: FareType {
        switch name {
        case .adults:
            return .standard
        case .students, .infants:
            return .free
        case .child, .youth, .seniors:
            return .discounted
        }
    }
}
