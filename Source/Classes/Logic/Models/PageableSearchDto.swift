//
//  PageableSearch.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 15/08/2019.
//

import Foundation

struct PageableSearch: Codable {
    let numberOfElements: Int
    let pageNumber: Int
}
