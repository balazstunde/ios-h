//
//  UserDTO.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import Foundation

struct UserDTO: Codable {
    
    let changedPlatformNotificationsEnabled: Bool
    let email: String
    let firstName: String
    let marketingNotificationsEnabled: Bool
    let trainArrivingIntoStationNotificationsEnabled: Bool
    let tripDelayNotificationsEnabled: Bool
    let tripDisruptionNotificationsEnabled: Bool
    
}
