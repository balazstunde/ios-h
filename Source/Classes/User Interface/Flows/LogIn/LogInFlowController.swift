//
//  LogInFlowController.swift
//  Choo Choo
//
//  Created by Halcyon on 09/08/2019.
//

import UIKit

protocol LogInFlowControllerDelegate: AnyObject {
    func didLogin(_ loginFlowController: LogInFlowController)
}

class LogInFlowController: NavigationFlowController {
    override var firstScreen: UIViewController {
        let loginViewController = LogInViewController()
        loginViewController.delegate = self
        return loginViewController
    }
    
    weak var flowDelegate: LogInFlowControllerDelegate?
}

extension LogInFlowController: LogInViewControllerFlowDelegate {
    func didLoginFail(from: LogInViewController) {
        navigationController.presentAlert(customTitle: "Invalid email or password", customMessage: "Try again!")
    }
    
    func didSelectBackButton(from: LogInViewController) {
        navigationController.popViewController(animated: true)
    }
    
    func didSuccesfullyLogIn(from: LogInViewController) {
        flowDelegate?.didLogin(self)
    }
    
    func didTouchForgotPasswordButton(from: LogInViewController) {
        let passRecoveryFC = PasswordRecoveryFlowController(from: self)
        passRecoveryFC.start(presentationStyle: .push)
    }
}
