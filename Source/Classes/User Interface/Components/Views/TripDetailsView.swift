//
//  TripDetailsView.swift
//  Choo Choo
//
//  Created by Halcyon on 30/07/2019.
//

import Foundation
import UIKit

class TripDetailsView: UIView {
    var didTapViewClosure: (() -> Void)?
    
    private let icon: UIImage
    private let iconImage = UIImageView()
    private var labelView = UILabel()
    
    init(icon: UIImage) {
        self.icon = icon
        super.init(frame: .zero)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        backgroundColor = .white
        layer.borderWidth = LayoutValues.borderWidth
        layer.borderColor = UIColor.lola.cgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        addGestureRecognizer(tap)
        
        setupIcon()
        setupLabel()
    }
    
    private func setupIcon() {
        addSubview(iconImage)
        
        iconImage.image = icon
        iconImage.image = iconImage.image?.withRenderingMode(.alwaysTemplate)
        iconImage.tintColor = .darkGray
    }
    
    private func setupLabel() {
        addSubview(labelView)
        
        labelView.font = Font.barlowMedium(size: .medium)
    }
    
    private func setupConstraints() {
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        labelView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            iconImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.padding),
            iconImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImage.heightAnchor.constraint(equalTo: iconImage.widthAnchor),
            
            labelView.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: LayoutValues.padding),
            labelView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -LayoutValues.padding),
            labelView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func setViewText(withText: String) {
        labelView.text = withText
    }
    
    @objc func didTapView() {
        didTapViewClosure?()
    }
}

extension TripDetailsView {
    private enum LayoutValues {
        static let padding: CGFloat = 16
        static let borderWidth: CGFloat = 1.0
    }
}
