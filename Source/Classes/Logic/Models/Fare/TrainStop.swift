//
//  TrainStop.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 15/08/2019.
//

import Foundation

struct TrainStop: Codable {
    let arrivalTime: String
    let departureTime: String
    let platformNumber: Int?
    let stopName: String
    let trainStopId: Int
}
