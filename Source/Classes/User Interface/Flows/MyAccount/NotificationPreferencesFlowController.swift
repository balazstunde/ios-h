//
//  NotificationPreferencesFlowController.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import UIKit

class NotificationPreferencesFlowController: NavigationFlowController {
    override var firstScreen: UIViewController {
        let notificationPreferencesViewController = NotificationPreferencesViewController()
        notificationPreferencesViewController.delegate = self
        return notificationPreferencesViewController
    }
}

extension NotificationPreferencesFlowController: NotificationPreferencesViewControllerDelegate {
    func showAlert(on viewController: NotificationPreferencesViewController) {
        navigationController.presentAlert(customTitle: "Hopa..", customMessage: "Something went wrong")
    }
    
    func close() {
        navigationController.popViewController(animated: true)
    }
}
