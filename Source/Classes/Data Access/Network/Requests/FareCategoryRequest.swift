//
//  FareCategoryRequests.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 08/01/2019.
//

import Foundation

extension Request.FareRequest {
    struct FareCategRequest: RequestRepresentable {
        var paramType: PostParamType = .body
 
        var suffix: String = API.Path.journeyPricingCategories
        
        var method: HTTPMethod = .get
        
        var parameters: Parameters?
        
    }
    
}
