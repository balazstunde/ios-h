//
//  User.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 12/03/2017.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import Foundation

struct NewUserAccount: Codable {

    let email: String
    let firstName: String
    let password: String
    let pushNotificationEnabled: Bool
}
