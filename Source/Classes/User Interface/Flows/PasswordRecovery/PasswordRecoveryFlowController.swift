//
//  PasswordRecoveryFlowController.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

class PasswordRecoveryFlowController: NavigationFlowController {
    override var firstScreen: UIViewController {
        let passwordRecoveryViewController = PasswordRecoveryViewController()
        passwordRecoveryViewController.delegate = self
        return passwordRecoveryViewController
    }
}

extension PasswordRecoveryFlowController: PasswordRecoveryViewControllerFlowDelegate {
    func didTouchSendLinkButton(on viewController: PasswordRecoveryViewController, email: String) {
        viewController.navigationController?.pushViewController(SecondPasswordRecoveryViewController(email: email), animated: true)
    }
}
