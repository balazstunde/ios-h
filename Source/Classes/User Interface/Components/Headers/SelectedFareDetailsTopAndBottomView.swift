//
//  SelectedFareDetailsTopAndBottomView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 13/08/2019.
//

import UIKit

enum ViewType: String {
    case header
    case footer
}

class SelectedFareDetailsTopAndBottomView: UIView {
    private let timeLabel = UILabel()
    private let iconImageView = UIImageView()
    private let cityNameLabel = UILabel()
    private let platformLabel = UILabel()
    private let lineView = UIView()
    private var type: ViewType
    private var lineViewTopConstraint: NSLayoutConstraint?
    private var iconImageViewHeightConstraint: NSLayoutConstraint?
    private var iconImageViewWidthConstraint: NSLayoutConstraint?
    
    init(type: ViewType) {
        self.type = type
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(timeLabel)
        addSubview(iconImageView)
        addSubview(cityNameLabel)
        addSubview(platformLabel)
        addSubview(lineView)
        
        timeLabel.font = Font.barlowBold(size: .mediumSmall)
        cityNameLabel.font = Font.barlowSemiBold(size: .mediumSmall)
        platformLabel.font = Font.barlowSemiBold(size: .mediumSmall)
        platformLabel.textAlignment = .right
        lineView.backgroundColor = .softPeach
    }
    
    private func setupConstraints() {
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        cityNameLabel.translatesAutoresizingMaskIntoConstraints = false
        platformLabel.translatesAutoresizingMaskIntoConstraints = false
        lineView.translatesAutoresizingMaskIntoConstraints = false
        setLineViewConstraint()
        setImageViewConstraints()
        
        var constraints = [
            timeLabel.topAnchor.constraint(equalTo: topAnchor, constant: Layout.timeLabelPadding),
            timeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.timeLabelPadding),
            timeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: AppStyle.Layout.padding),
            timeLabel.widthAnchor.constraint(equalToConstant: Layout.timeLabelWidth),
            
            iconImageView.leadingAnchor.constraint(equalTo: timeLabel.trailingAnchor),
            iconImageView.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor),
            
            cityNameLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: Layout.cityNamePadding),
            cityNameLabel.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor),

            platformLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -AppStyle.Layout.padding),
            platformLabel.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor),
            
            lineView.heightAnchor.constraint(equalToConstant: Layout.lineViewHeight),
            lineView.leadingAnchor.constraint(equalTo: cityNameLabel.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: platformLabel.trailingAnchor),
            lineViewTopConstraint!
        ]
        
        if let imageHeightConstraint = iconImageViewHeightConstraint, let imageWidthConstraint = iconImageViewWidthConstraint {
            constraints.append(imageHeightConstraint)
            constraints.append(imageWidthConstraint)
        }
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func configure(time: String, city: String, platform: Int) {
        timeLabel.text = String(time.prefix(5))
        cityNameLabel.text = city
        platformLabel.text = "Pl. \(platform)"
        setImageView(type: type)
    }
    
    private func setImageView(type: ViewType) {
        switch type {
        case .footer:
            iconImageView.image = Asset.icNavigationPinFill.image
            iconImageView.tintColor = .shiraz
            iconImageView.contentMode = .scaleAspectFit
        case .header:
            iconImageView.layer.borderColor = UIColor.switchButtonColor.cgColor
            iconImageView.layer.cornerRadius = Style.cornerRadius
            iconImageView.layer.borderWidth = Style.borderWidth
        }
    }
    
    private func setLineViewConstraint() {
        switch type {
        case .footer:
            lineViewTopConstraint = lineView.topAnchor.constraint(equalTo: topAnchor)
        case .header:
            lineViewTopConstraint = lineView.bottomAnchor.constraint(equalTo: bottomAnchor)
        }
    }
    
    private func setImageViewConstraints() {
        guard type == .header else { return }
        iconImageViewHeightConstraint = iconImageView.heightAnchor.constraint(equalToConstant: 15)
        iconImageViewWidthConstraint = iconImageView.widthAnchor.constraint(equalToConstant: 15)
    }
}

extension SelectedFareDetailsTopAndBottomView {
    private enum Layout {
        static let timeLabelPadding: CGFloat = 10
        static let timeLabelWidth: CGFloat = 50
        static let cityNamePadding: CGFloat = 14
        static let lineViewHeight: CGFloat = 1
    }
    
    private enum Style {
        static let cornerRadius: CGFloat = 7.5
        static let borderWidth: CGFloat = 3
    }
}
