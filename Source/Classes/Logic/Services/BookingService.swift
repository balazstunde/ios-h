//
//  PassengerDetailsService.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import Foundation

struct TicketDetails {
    var fromDestination: Station?
    var toDestination: Station?
    var date = Date()
}

final class BookingService {
    
    var ticketDetails: TicketDetails = TicketDetails()
    
    let keyStore = KeyStore.shared
    
    private var map:[String:Int] = ["Adult":1, "Student": 0, "Infant": 0, "Child": 0, "Youth": 0, "Senior": 0]
    private var priceMap: [String: Double] = [:]
    
    func getDetails() -> [String:Int]? {
        return map
    }
    
    func getFares(completion: @escaping ([FareCategory]) -> Void, failure: @escaping (Error?) -> Void) {
        BackendManager.execute(request: Request.FareRequest.FareCategRequest(), success: { (fares: [FareCategory]) in
            for fareCategory in fares {
                self.priceMap[fareCategory.title] = fareCategory.discountPercentage
            }
            self.keyStore.fareCategoriesPriceMap = self.priceMap
            completion(fares)
        }, failure: { error in
            print(error.localizedDescription)
            failure(error)
        })
    }
    
    func setDetails(passangersDetails: [String:Int]) {
        self.map = passangersDetails
        keyStore.fareCategoriesMap = passangersDetails
    }
    
    func getDetailsAsString() -> String {
        var details: [String] = []
        for (person, number) in map where number > 0 {
            details.append("\(person) \(number)")
        }
        return details.joined(separator: ", ")
    }
}
