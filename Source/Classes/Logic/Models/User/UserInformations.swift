//
//  UserInformations.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 20/08/2019.
//

import Foundation

struct UserInformations: Codable {
    let email: String
    let firstName: String
}
