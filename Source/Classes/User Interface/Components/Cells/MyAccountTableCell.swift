//
//  MyAccountCell.swift
//  Choo Choo
//
//  Created by halcyonmobile on 29/07/2019.
//

import UIKit

class MyAccountTableCell: UITableViewCell {
    private var titleAndDetailsStackView = UIStackView()
    private let tableTitleLabel = UILabel()
    private let tableDescriptionLabel = UILabel()
    private let imageRowView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initView()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(_ rowData: MyAccountSection.MyAccountRowData) {
        tableTitleLabel.text = rowData.title
        tableDescriptionLabel.text = rowData.description
        imageRowView.image = rowData.leftImage.image
        imageRowView.image = imageRowView.image?.withRenderingMode(.alwaysTemplate)
        imageRowView.tintColor = .iconColor
    }
    
    private func initView() {
        tableTitleLabel.font = Font.barlowBold(size: .medium)
        tableDescriptionLabel.font = Font.barlowRegular(size: .medium)
        tableDescriptionLabel.textColor = .textDescriptionColor
        
        titleAndDetailsStackView = UIStackView(arrangedSubviews: [tableTitleLabel, tableDescriptionLabel])
        titleAndDetailsStackView.alignment = .leading
        titleAndDetailsStackView.axis = .vertical
        titleAndDetailsStackView.spacing = Padding.small
        
        contentView.addSubview(titleAndDetailsStackView)
        contentView.addSubview(imageRowView)
    }
    
    private func setUpConstraints() {
        var constraints = [NSLayoutConstraint] ()
        
        titleAndDetailsStackView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(titleAndDetailsStackView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Padding.custom))
        constraints.append(titleAndDetailsStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Padding.small))
        constraints.append(titleAndDetailsStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Padding.small))
        
        imageRowView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(imageRowView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Padding.medium))
        constraints.append(imageRowView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor))
        NSLayoutConstraint.activate(constraints)
    }
}
