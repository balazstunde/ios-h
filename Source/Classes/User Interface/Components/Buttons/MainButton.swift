//
//  MainButton.swift
//  Choo Choo
//
//  Created by Halcyon on 31/07/2019.
//

import Foundation
import UIKit

final class MainButton: UIButton {
    var didTapButtonClosure: (() -> Void)?
    
    private let title: String
    
    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        isEnabled = true
        setTitle(title, for: .normal)
        titleLabel?.font = Font.barlowBold(size: .medium)
        addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    override var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? UIColor.shiraz : UIColor.lola
        }
    }
    
    @objc private func didTapButton() {
        didTapButtonClosure?()
    }
}
