//
//  FromToView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import UIKit

enum ProgressImageStatus {
    case complete, incomplete
}

class FromToView: UIView {
    
    private lazy var horizontalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.spacing = AppStyle.PaddingValues.bigPadding
        return stack
    }()
    
    private lazy var verticalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 5
        return stack
    }()
    
    private lazy var progressImage = UIImageView()
    private lazy var swapLocationsButton = UIButton()
    private lazy var fromView = LocationView(locationViewType: .from)
    private lazy var destinationView = LocationView(locationViewType: .destination)
    private lazy var lineView = UIView()
    
    var fromTextField:  String? {
        get { return fromView.inputLocation }
        set { fromView.inputLocation = newValue ?? ""}
    }
    
    var toTextField:  String? {
        get { return destinationView.inputLocation }
        set { destinationView.inputLocation = newValue ?? ""}
    }

    var progressImageState: ProgressImageStatus = .incomplete {
        didSet {
            switch progressImageState {
            case .complete:
                progressImage.image = Asset.icLandingPageWithDestination.image
            case .incomplete:
                progressImage.image = Asset.icLandingPageWithoutDestination.image
            }
        }
    }
    
    var didTouchTo: (() -> Void)?
    var didTouchFrom: (() -> Void)?
    var didTouchSwitch: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        addSubview(horizontalStackView)
        horizontalStackView.addArrangedSubview(progressImage)
        horizontalStackView.addArrangedSubview(verticalStackView)
        horizontalStackView.addArrangedSubview(swapLocationsButton)
        
        verticalStackView.addArrangedSubview(fromView)
        verticalStackView.addArrangedSubview(lineView)
        verticalStackView.addArrangedSubview(destinationView)
        
        lineView.backgroundColor = .lola
        setupProgressImage()
        setupSwapButton()
        
        destinationView.didTouchTextField = {
            self.didTouchTo?()
        }
        fromView.didTouchTextField = {
            self.didTouchFrom?()
        }
        
    }
    
    private func setupConstraints() {
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        var constraints: [NSLayoutConstraint] = []
        
        constraints.append(horizontalStackView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor))
        constraints.append(horizontalStackView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor))
        constraints.append(horizontalStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: AppStyle.Layout.padding))
        constraints.append(horizontalStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -AppStyle.Layout.padding))
    
        lineView.translatesAutoresizingMaskIntoConstraints = false
        
        constraints.append(lineView.heightAnchor.constraint(equalToConstant: 1))
        constraints.append(lineView.centerYAnchor.constraint(equalTo: centerYAnchor))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupProgressImage() {
        progressImage.image = Asset.icLandingPageWithoutDestination.image
        progressImage.contentMode = .scaleAspectFit
    }
    
    private func setupSwapButton() {
        let imageButton = Asset.icSwapButton.image.withRenderingMode(.alwaysTemplate)
        swapLocationsButton.setImage(imageButton, for: .normal)
        swapLocationsButton.tintColor = UIColor.shiraz
        swapLocationsButton.addTarget(self, action: #selector(didTouchedSwapButton), for: .touchUpInside)
    }
    
    @objc private func didTouchedSwapButton() {
        guard !fromView.inputLocation.isEmpty, !destinationView.inputLocation.isEmpty else { return }
 
        let fromLocation = fromView.inputLocation
        fromView.inputLocation = destinationView.inputLocation
        destinationView.inputLocation = fromLocation
        
        didTouchSwitch?()
    }

}
