//
//  BookingHeaderView.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation
import UIKit

class BookingHeaderView: UIView {
    private let fromLabel = CityLabel()
    private let toLabel = CityLabel()
    private let betweenLabel = UILabel()
    private let titleLabel = UILabel()
    private let horizontalStack = UIStackView()
    
    var didTapFromLabelClosure: (() -> Void)?
    var didTapToLabelClosure: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .primary
        addSubview(titleLabel)
        addSubview(horizontalStack)
        
        horizontalStack.axis = .horizontal
        horizontalStack.alignment = .fill
        horizontalStack.spacing = LayoutValues.padding
        
        horizontalStack.addArrangedSubview(fromLabel)
        horizontalStack.addArrangedSubview(betweenLabel)
        horizontalStack.addArrangedSubview(toLabel)
        
        betweenLabel.text = "to"
        betweenLabel.font = Font.barlowRegular(size: .medium)
        betweenLabel.textColor = .white
        
        titleLabel.font = Font.barlowRegular(size: .medium)
        titleLabel.textColor = .white
        
        fromLabel.didTapLabelClosure = {
            self.didTapFromLabelClosure?()
        }
        
        toLabel.didTapLabelClosure = {
            self.didTapToLabelClosure?()
        }
    }
    
    private func setupConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        horizontalStack.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            heightAnchor.constraint(equalToConstant: LayoutValues.height),
            
            horizontalStack.topAnchor.constraint(equalTo: centerYAnchor),
            horizontalStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            titleLabel.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -LayoutValues.padding),
            titleLabel.centerXAnchor.constraint(equalTo: horizontalStack.centerXAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func setToLabelText(with text: String) {
        toLabel.text = text
    }
    
    public func setFromLabelText(with text: String) {
        fromLabel.text = text
    }
    
    public func getToLabelText() -> String {
        return toLabel.text ?? ""
    }
    
    public func getFromLabelText() -> String {
        return fromLabel.text ?? ""
    }
    
    public func setTitleText(with text: String) {
        titleLabel.text = text
    }
}

extension BookingHeaderView {
    private enum LayoutValues {
        static let padding: CGFloat = 4
        static let height: CGFloat = 100
    }
}
