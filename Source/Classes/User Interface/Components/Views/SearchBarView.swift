//
//  SearchBarView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import UIKit

protocol SearchBarViewDelegate: AnyObject {
    func textDidChange(newValue: String)
}
class SearchBarView: UIView {
    
    weak var delegate: SearchBarViewDelegate?
    
    private let searchBar = UISearchBar()
    private let clearButton = UIButton()
    private let icon = UIImageView()
    
    private enum LayoutValues {
        static let padding32: CGFloat = 32.0
        static let padding39: CGFloat = 39.0
        static let padding14: CGFloat = 14.0
        static let padding10: CGFloat = 10.0
        static let height24: CGFloat = 24.0
    }
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setupView() {
        backgroundColor = #colorLiteral(red: 0.5085967183, green: 0.07502175122, blue: 0.2506508231, alpha: 1)
        icon.image = UIImage(named: "ic_navigation_account-enabled")
        icon.image = icon.image?.withRenderingMode(.alwaysTemplate)
        icon.tintColor = .white
        icon.contentMode = .scaleAspectFit
        
        addSubview(icon)
        addSubview(searchBar)
        addSubview(clearButton)
        
        clearButton.addTarget(self, action: #selector(didTapClearButton), for: .touchUpInside)
        clearButton.setTitle("CLEAR", for: .normal)
        clearButton.titleColor(for: .normal)
        clearButton.isHidden = true
        clearButton.contentMode = .center
        clearButton.titleLabel?.font = Font.barlowSemiBold(size: .mediumSmall)
        
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.setImage(UIImage(), for: .clear, state: .normal)
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        searchBar.becomeFirstResponder()
        
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField {
            searchTextField.textColor = .white
            searchTextField.borderStyle = .none
            searchTextField.attributedPlaceholder = NSAttributedString(string: "City, station", attributes: [.foregroundColor: UIColor.white])
            searchTextField.font = Font.barlowRegular(size: .medium)
        }
    }
    
    private func setupConstraints() {
        icon.translatesAutoresizingMaskIntoConstraints = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        clearButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: AppStyle.Layout.cellIconPadding),
            icon.widthAnchor.constraint(equalToConstant: LayoutValues.height24),
            icon.heightAnchor.constraint(equalTo: icon.widthAnchor),
            icon.topAnchor.constraint(equalTo: topAnchor, constant: LayoutValues.padding14),
            icon.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -LayoutValues.padding14),
            
            searchBar.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: AppStyle.Layout.cellIconPadding),
            searchBar.heightAnchor.constraint(equalTo: icon.heightAnchor),
            searchBar.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            clearButton.topAnchor.constraint(equalTo: topAnchor, constant: LayoutValues.padding10),
            clearButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -AppStyle.Layout.cellIconPadding),
            clearButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -LayoutValues.padding10),
            clearButton.leadingAnchor.constraint(equalTo: searchBar.trailingAnchor, constant: AppStyle.Layout.cellIconPadding)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didTapClearButton() {
        setSearchBarText(with: "")
        clearButton.isHidden = true
        clearButton.isEnabled = false
    }
    
    public func setSearchBarText(with text: String) {
        searchBar.text = text
        delegate?.textDidChange(newValue: text)
    }
}

extension SearchBarView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        delegate?.textDidChange(newValue: searchText)
        
        searchBar.showsCancelButton = false
        clearButton.isHidden = searchText.isEmpty
        clearButton.isEnabled = !searchText.isEmpty
    }
}
