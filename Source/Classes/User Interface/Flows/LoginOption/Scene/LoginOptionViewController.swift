//
//  LoginOptionViewController.swift
//  Choo Choo
//
//  Created by halcyonmobile on 01/08/2019.
//

import UIKit

protocol LogInOptionViewControllerFlowDelegate: AnyObject {
    func didTouchLogInButton(on viewController: LoginOptionViewController)
    func didTouchCreateAccountButton(on viewController: LoginOptionViewController)
    func didTouchTermButton(on viewController: LoginOptionViewController)
    func didTouchPrivacyButton(on viewController: LoginOptionViewController)
}

enum LoginType {
    case restricted
    case normal
}

class LoginOptionViewController: BaseViewController {
    private let loginImageView = UIImageView()
    private let messageLabel = UILabel()
    private let loginType : LoginType
    private let loginComponentViewController = LoginComponentViewController()
    weak var delegate : LogInOptionViewControllerFlowDelegate?
    
    init(loginType: LoginType) {
        self.loginType = loginType
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if loginType == LoginType.normal {
            initNormalLoginUI()
            setUpConstraintsNormalLogin()
        } else {
            initRestrictedLogin()
            setUpConstraintsRestrictedLogin()
        }
    }
    
    private func initNormalLoginUI() {
        view.backgroundColor = .white
        navigationItem.title = "My Account"
        
        loginImageView.image = Asset.icLoginoption.image
        
        messageLabel.text = "Take advantage of an account\nfor faster booking, travel updates\nand more."
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0
        messageLabel.font = Font.barlowMedium(size: .mediumSmall)
        
        view.addSubview(loginImageView)
        view.addSubview(messageLabel)
        loginComponentViewController.delegate = self
        self.addChild(loginComponentViewController)
        self.view.addSubview(loginComponentViewController.view)
    }
    
    private func initRestrictedLogin() {
        view.backgroundColor = .white
        navigationItem.title = "Authentication"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain, target: self, action: #selector(didPressBackButton))
        
        loginImageView.image = Asset.icRestrictedAuthentication.image
        
        messageLabel.text = "You need yo be signed in\nto book a train ticket."
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = Font.barlowRegular(size: .medium)
        
        view.addSubview(loginImageView)
        view.addSubview(messageLabel)
        loginComponentViewController.delegate = self
        self.addChild(loginComponentViewController)
        self.view.addSubview(loginComponentViewController.view)
    }
    
    @objc func didPressBackButton() {
        navigationController?.popViewController(animated: true)
    }

    private func setUpConstraintsNormalLogin() {
        var constraints = [NSLayoutConstraint] ()
        loginImageView.translatesAutoresizingMaskIntoConstraints = false
        
        constraints.append(loginImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -1))
        constraints.append(loginImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor))
        constraints.append(loginImageView.topAnchor.constraint(equalTo: view.topAnchor))
        
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(messageLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Padding.extraLarge))
        constraints.append(messageLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Padding.extraLarge))
        
        loginComponentViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        constraints.append(loginComponentViewController.view.topAnchor.constraint(equalTo: loginImageView.bottomAnchor, constant: Padding.custom))
        constraints.append(loginComponentViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Padding.medium))
        constraints.append(loginComponentViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Padding.medium))
        constraints.append(loginComponentViewController.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Padding.big))
       
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setUpConstraintsRestrictedLogin() {
        var constraints = [NSLayoutConstraint] ()
        loginImageView.translatesAutoresizingMaskIntoConstraints = false
        
        constraints.append(loginImageView.heightAnchor.constraint(equalToConstant: view.bounds.height / 5))
        constraints.append(loginImageView.widthAnchor.constraint(equalToConstant: view.bounds.height / 5))
        constraints.append(loginImageView.topAnchor.constraint(lessThanOrEqualTo : view.safeAreaLayoutGuide.topAnchor, constant: Padding.custom))
        constraints.append(loginImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(messageLabel.topAnchor.constraint(equalTo: loginImageView.bottomAnchor, constant: Padding.extraLarge))
        constraints.append(messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        loginComponentViewController.view.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(loginComponentViewController.view.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Padding.custom))
        constraints.append(loginComponentViewController.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Padding.medium))
        constraints.append(loginComponentViewController.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.medium))
        constraints.append(loginComponentViewController.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Padding.big))
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension LoginOptionViewController: LoginComponentViewControllerFlowDelegate {
    func didTouchLogInButton(on viewController: LoginComponentViewController) {
        delegate?.didTouchLogInButton(on: self)
    }
    
    func didTouchCreateAccountButton(on viewController: LoginComponentViewController) {
        delegate?.didTouchCreateAccountButton(on: self)
    }
    
    func didTouchTermButton(on viewController: LoginComponentViewController) {
        delegate?.didTouchTermButton(on: self)
    }
    
    func didTouchPrivacyButton(on viewController: LoginComponentViewController) {
        delegate?.didTouchPrivacyButton(on: self)
    }
}
