//
//  SortMethodCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 12/08/2019.
//

import UIKit

class SortMethodCell: UITableViewCell {
    
    private let methodNameLabel = UILabel()
    let selectedMethodImage = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        methodNameLabel.font = Font.barlowRegular(size: FontSize.medium)
        selectedMethodImage.image = Asset.icChecked.image
        selectedMethodImage.tintColor = .shiraz
        selectedMethodImage.isHidden = true
        
        addSubview(methodNameLabel)
        addSubview(selectedMethodImage)
    }
    
    private func setupConstraints() {
        methodNameLabel.translatesAutoresizingMaskIntoConstraints = false
        selectedMethodImage.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            methodNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            methodNameLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.centerXAnchor),
            methodNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: LayoutValues.mediumPadding),
            methodNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -LayoutValues.mediumPadding),
            
            selectedMethodImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.smallPadding),
            selectedMethodImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    public func configure(withSortMethod method: String, isActive active: Bool) {
        methodNameLabel.text = method
        selectedMethodImage.isHidden = !active
    }
}

extension SortMethodCell {
    private enum LayoutValues {
        static let verySmallPadding: CGFloat = 4.0
        static let smallPadding: CGFloat = 8.0
        static let mediumPadding: CGFloat = 16.0
    }
}
