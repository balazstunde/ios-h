//
//  FindingFareFooterView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 16/08/2019.
//

import UIKit

class FindingFareFooterView: UIView {
    private let footerLabel = UILabel()
    
    init(withFaresFound faresCount: Int, frame: CGRect) {
        footerLabel.text = "Found \(faresCount) trains."
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(footerLabel)
        footerLabel.font = Font.barlowRegular(size: .medium)
    }
    
    private func setupConstraints() {
        footerLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            footerLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            footerLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
}
