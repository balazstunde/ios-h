//
//  MyAccountFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

protocol MyAccountFlowControllerDelegate: AnyObject {
    func didLogout(_ myAccountFlowController: MyAccountFlowController)
}

class MyAccountFlowController: NavigationFlowController {
    
    weak var flowDelegate: MyAccountFlowControllerDelegate?
    
    override var firstScreen: UIViewController {
        let myAccountViewController = MyAccountViewController()
        myAccountViewController.delegate = self
        return myAccountViewController
    }
}

extension MyAccountFlowController: MyAccountViewControllerFLowDelegate {
    func didTouchProfileSettings(on viewController: MyAccountViewController) {
        let editMyAccountFlowController = EditMyAccountFlowController(from: self)
        editMyAccountFlowController.start(presentationStyle: .push)
    }
    
    func didTouchPaymentMethod(on viewController: MyAccountViewController) {
        let paymentFlowController = ConfigurePaymentFlowController(from: self, paymentType: .updatePayment)
        paymentFlowController.start()
    }
    
    func didTouchNotifications(on viewController: MyAccountViewController) {
        let notificationPreferencesFlowController = NotificationPreferencesFlowController(from: self)
        notificationPreferencesFlowController.start(presentationStyle: .push)
    }
    
    func didTouchPrivacyPolicy(on viewController: MyAccountViewController) {
        let browserFlowController = BrowserViewFlowController(browserType: .privacy, from: self)
        browserFlowController.start(presentationStyle: FlowPresentationStyle.push)
    }
    
    func didTouchTermsAndConditions(on viewController: MyAccountViewController) {
        let browserFlowController = BrowserViewFlowController(browserType: .terms, from: self)
        browserFlowController.start(presentationStyle: FlowPresentationStyle.push)
    }
    
    func didTouchCustomerSupport(on viewController: MyAccountViewController) {
        print("Touched Customer")
    }
    
    func didTouchLogOut(on viewController: MyAccountViewController) {
        flowDelegate?.didLogout(self)
    }
}
