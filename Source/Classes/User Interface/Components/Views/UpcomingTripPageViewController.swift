//
//  UpcomingTripsPage.swift
//  Choo Choo
//
//  Created by halcyonmobile on 13/08/2019.
//

import UIKit
import UserNotifications

protocol UpcomingTripPageViewControllerDelegate: AnyObject {
    func didTouchPlanLabel()
}

class UpcomingTripPageViewController: UIViewController {
    
    private var color: UIColor = .white
    private let tableView =  UITableView()
    private var tickets: [Ticket] = []
    private let ticketService = TicketService()
    private let ticketCell = "TicketCell"
    private let notLoggedIn = NotLoggedInViewController()
    var emptyDataView: EmptyStateView?
    weak var delegate: UpcomingTripPageViewControllerDelegate?
    private let localNotificationService = LocalNotificationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadView()
    }
    func reloadView() {
        guard KeyStore.shared.userID != 0 else {
            emptyDataView = EmptyStateView(tripType: .notLoggedIn)
            tickets = []
            tableView.backgroundView = emptyDataView
            tableView.reloadData()
            return
        }
        emptyDataView = EmptyStateView(tripType: .upcomingTrip)
        emptyDataView?.didTouchPlanLabel = {
            self.delegate?.didTouchPlanLabel()
        }
        tableView.backgroundView = emptyDataView
        loadUpcomingData()
    }
    
    func loadUpcomingData() {
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        spinner.color = .shiraz
        spinner.startAnimating()
        self.tableView.tableFooterView = spinner
        self.tableView.tableFooterView?.isHidden = false
        ticketService.getTickets(flag: .UPCOMING, completion: { [weak self] (ticket: [Ticket]) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if ticket.count == self.tickets.count {
                    spinner.stopAnimating()
                    self.tableView.tableFooterView?.isHidden = true
                    return
                }
                self.tickets = ticket
                spinner.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.tableView.reloadData()
            }
            }, failure: { [weak self] _ in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    spinner.stopAnimating()
                    self.tableView.tableFooterView?.isHidden = true
                }
        })
    }
    
    func setupView() {
        view.addSubview(tableView)
        notLoggedIn.isHidden = true
        view.backgroundColor = .white
        tableView.delegate = self
        tableView.register(TicketCell.self, forCellReuseIdentifier: ticketCell)
        tableView.dataSource = self
        tableView.reloadData()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        loadUpcomingData()

    }
    
    func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Padding.large),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
}

extension UpcomingTripPageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return LayoutValues.customTableRowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return LayoutValues.customTableHeaderHeight
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height - UIScreen.main.bounds.width * 0.5) {
            loadUpcomingData()
        }
    }
}

extension UpcomingTripPageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tickets.isEmpty {
            tableView.backgroundView = emptyDataView
        } else {
            tableView.backgroundView = nil
        }
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ticketCell, for: indexPath) as? TicketCell else {
            fatalError("Error dequeue-ing")
        }
        let backgroundImageView = UIImageView()
        backgroundImageView.image = Asset.icTicketBackground.image
        cell.configure(tickets[indexPath.row])
        cell.backgroundView = backgroundImageView
        cell.delegate = self
        return cell
    }
}

private enum LayoutValues {
    static let customTableRowHeight: CGFloat = 150
    static let customTableHeaderHeight: CGFloat = 165
    static let customTableTop: CGFloat = 70
    static let lineHeight: CGFloat = 3
}

extension UpcomingTripPageViewController: TicketCellDelegate {
    func didSelectReminderButton(notificationData: NotificationData, isSelected: Bool, on cell: UITableViewCell) {
        if isSelected {
            localNotificationService.setUpOneDayNotification(notificationDetails: notificationData)
            localNotificationService.setUpTwoHoursNotification(notificationDetails: notificationData)
        } else {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers:
                ["twoHours \(notificationData.startDate + notificationData.startTime)",
                    "oneDay \(notificationData.startDate + notificationData.startTime)"])
        }
    }
}
