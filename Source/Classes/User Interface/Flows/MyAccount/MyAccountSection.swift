//
//  MyAccountSection.swift
//  Choo Choo
//
//  Created by halcyonmobile on 29/07/2019.
//

import Foundation
import UIKit

enum MyAccountSection {
    case general([MyAccountRowData])
    case support([MyAccountRowData])
    case logout([MyAccountRowData])
    
    var sectionTitle : String {
        switch self {
        case .general: return "General"
        case .support: return "Support"
        case .logout: return ""
        }
    }
    struct MyAccountRowData {
        let title: String
        let description: String
        let leftImage: ImageAsset
    }
}
