//
//  CreateNewAccountFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 07/08/2019.
//

import UIKit

class CreateNewAccountFlowController: NavigationFlowController {
    override var firstScreen: UIViewController {
        let createNewAccountViewController = CreateNewAccountViewController()
        createNewAccountViewController.delegate = self
        return createNewAccountViewController
    }
}
extension CreateNewAccountFlowController: CreateNewAccountViewControllerFlowDelegate {
    
  func didTouchPrivacyAndPolicy(on viewController: CreateNewAccountViewController) {
        let browserViewController = BrowserViewFlowController(browserType: .privacy, from: self)
        browserViewController.start(presentationStyle: FlowPresentationStyle.push)
    }
    
}
