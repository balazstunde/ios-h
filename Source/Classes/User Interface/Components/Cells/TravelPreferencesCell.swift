//
//  TravelPreferencesCell.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import UIKit
import DLRadioButton

protocol TravelPreferencesCellDelegate: AnyObject {
    func radioButtonTapped(_ radioButton: DLRadioButton, on cell: TravelPreferencesCell)
}

class TravelPreferencesCell: UITableViewCell {
    var radioButton: DLRadioButton!
    var classTypeLabel: UILabel!
    var priceLabel: UILabel!
    weak var delegate: TravelPreferencesCellDelegate?
 
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initView() {
        radioButton = DLRadioButton()
        radioButton.tintColor = .shiraz
        radioButton.iconColor = .shiraz
        radioButton.indicatorColor = .shiraz
        radioButton.addTarget(self, action: #selector(radioButtonTapped), for: .touchUpInside)
        
        classTypeLabel = UILabel()
        classTypeLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        priceLabel = UILabel()
        priceLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        contentView.addSubview(radioButton)
        contentView.addSubview(classTypeLabel)
        contentView.addSubview(priceLabel)
    }
    
    func setContent(travelPreference: TravelPreferences) {
        if travelPreference.name == "2nd Class" {
            radioButton.isSelected = true
            priceLabel.textColor = .shiraz
            priceLabel.text = "Best Price"
        } else {
            priceLabel.text = "+\(travelPreference.price) Lei"
        }
        classTypeLabel.text = travelPreference.name
    }
    
    private func setupConstraints() {
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        classTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            radioButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            radioButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            priceLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            classTypeLabel.leadingAnchor.constraint(equalTo: radioButton.trailingAnchor, constant: 28),
            classTypeLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            classTypeLabel.trailingAnchor.constraint(lessThanOrEqualTo: priceLabel.leadingAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func radioButtonTapped(_ radioButton: DLRadioButton) {
        delegate?.radioButtonTapped(radioButton, on: self)
    }
}
