//
//  PaymentService.swift
//  Choo Choo
//
//  Created by Halcyon on 13/08/2019.
//

import Foundation

struct UpdateCreditCardRequest: RequestRepresentable {
    var suffix: String
    
    var method: HTTPMethod = .put
    
    var parameters: Parameters?
    
    var paramType: PostParamType = .body
    
    init(creditCard: CreditCard, id: Int) {
        self.parameters = [
            "cvv": creditCard.cvv,
            "holderName": creditCard.holderName,
            "number": creditCard.number,
            "validUntil": creditCard.validUntil
        ]
        self.suffix = "/users/\(id)/credit-card"
    }
    
}

struct GetCreditCardRequest: RequestRepresentable {
    var suffix: String
    var paramType: PostParamType = .body
    var method: HTTPMethod = .get
    var parameters: Parameters?
    
    init(id: Int) {
        self.suffix = "/users/\(id)/credit-card"
    }
}

class PaymentService {
    
    func updateCreditCard(creditCard: CreditCard, userID: Int, completion: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        let request = UpdateCreditCardRequest(creditCard: creditCard, id: userID)
        BackendManager.execute(request: request, success: { _ in
            self.getCreditCard(userID: KeyStore.shared.userID ?? 0, completion: { card in
                KeyStore.shared.currentCreditCard = card
            }, failure: failure)
            completion()
        }, failure: failure)
    }
    
    func getCreditCard(userID: Int, completion: @escaping (CreditCard) -> Void, failure: @escaping (Error?) -> Void) {
        let request = GetCreditCardRequest(id: userID)
        BackendManager.execute(request: request, success: { creditCard in
            completion(creditCard)
        }, failure: failure)
    }
}
