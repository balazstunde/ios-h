//
//  DestinationFacilitiesView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 08/08/2019.
//

import UIKit

class DestinationFacilitiesView: UIView {
    
    private let destinationLabel = UILabel()
    private let connectionsLabel = UILabel()
    private let tableView = UITableView()
    private let backgroundview = UIView()
    private let destinationName: String
    private let facilities = [
        Facility(type: .bus, connections: ["3", "35", "21"], walkingTime: 5),
        Facility(type: .tram, connections: ["1L", "3R"], walkingTime: 8),
        Facility(type: .taxi, connections: ["Taxi station"], walkingTime: 5),
        Facility(type: .bike, connections: ["Bike docks"], walkingTime: 9)
    ]
    
    var tableHeightConstraint: NSLayoutConstraint!
    
    init(destination: String) {
        self.destinationName = destination
        super.init(frame: .zero)
        setupView()
        setupConstraints()
        
        tableView.layoutIfNeeded()
        tableView.setNeedsLayout()
        tableHeightConstraint.constant = tableView.contentSize.height - 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundview.layer.shadowPath = UIBezierPath(rect: backgroundview.bounds).cgPath
    }
    
    private func setupView() {
        addSubview(backgroundview)
        addSubview(destinationLabel)
        addSubview(connectionsLabel)
        addSubview(tableView)
        
        backgroundview.backgroundColor = .white
        backgroundview.layer.shadowColor = UIColor.lightGray.cgColor
        backgroundview.layer.shadowOpacity = 0.4
        backgroundview.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundview.layer.shadowRadius = 10
        backgroundview.layer.shouldRasterize = true
        
        connectionsLabel.text = "Connection with"
        connectionsLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        destinationLabel.text = destinationName
        destinationLabel.font = Font.barlowMedium(size: .medium)
        
        tableView.register(DestinationFacilitiesCell.self, forCellReuseIdentifier: DestinationFacilitiesCell.cellIdentifier)
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.isUserInteractionEnabled = false
        tableView.tableFooterView = UIView() // remove unnecessary lines
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func setupConstraints() {
        destinationLabel.translatesAutoresizingMaskIntoConstraints = false
        connectionsLabel.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        backgroundview.translatesAutoresizingMaskIntoConstraints = false
        
        tableHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        let constraints = [
            backgroundview.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundview.topAnchor.constraint(equalTo: topAnchor),
            backgroundview.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundview.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            destinationLabel.topAnchor.constraint(equalTo: topAnchor, constant: AppStyle.Layout.padding),
            destinationLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: AppStyle.Layout.padding),
            
            connectionsLabel.topAnchor.constraint(equalTo: destinationLabel.bottomAnchor, constant: AppStyle.PaddingValues.mediumSmallPadding),
            connectionsLabel.leadingAnchor.constraint(equalTo: destinationLabel.leadingAnchor),
            
            tableView.topAnchor.constraint(equalTo: connectionsLabel.bottomAnchor, constant: AppStyle.Layout.padding),
            tableView.leadingAnchor.constraint(equalTo: destinationLabel.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -AppStyle.Layout.padding),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableHeightConstraint!
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension DestinationFacilitiesView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facilities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DestinationFacilitiesCell.cellIdentifier, for: indexPath) as? DestinationFacilitiesCell else {
            assertionFailure("Error retrieving cell!")
            return UITableViewCell()
        }
        
        cell.updateUI(facility: facilities[indexPath.row])
        return cell
    }
}
