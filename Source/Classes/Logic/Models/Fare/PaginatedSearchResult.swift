//
//  PaginatedSearchResult.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 15/08/2019.
//

import Foundation

struct PaginatedSearchResult: Codable {
    let pageNumber: Int
    let pageSize: Int
    let results: [Journey]
}
