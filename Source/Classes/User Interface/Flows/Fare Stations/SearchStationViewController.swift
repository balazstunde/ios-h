//
//  SearchTableView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit
import MapKit
import CoreLocation

enum SearchStationType: AnyObject {
    case departure
    case destination
}

extension SearchStationType {
    var text: String {
        switch self {
        case .departure: return "Departure"
        case .destination: return "Destination"
        }
    }
}

protocol SearchLocationDelegate: AnyObject {
    func didSelectLocation(on viewController: SearchStationViewController, location: Station, viewControllerType: SearchStationType)
    func didPressBackButton(on viewController: SearchStationViewController)
}

class SearchStationViewController: BaseViewController {
    
    weak var delegate: SearchLocationDelegate?
    
    // Table View variables
    private let tableView = UITableView()
    private var recentlySearchedStations: [Station] = []
    private let maxSizeRecents = 5
    private var requestedStations: [Station] = []
    private let tableViewHeader = SearchStationTableViewHeader()
    private let placeholderView = SearchStationPlaceholderView()
    private let spinnerView = SearchStationTableViewFooterView()
    
    // NavBar variables
    private var closeButton: UIBarButtonItem?
    
    // Search Bar
    private let searchBarView = SearchBarView()
    private var searchedText: String?
    
    // Padding values
    private enum LayoutValues {
        static let padding39: CGFloat = 39.0
        static let rowHeight: CGFloat = 70.0
    }
    
    // View type
    private var type: SearchStationType
    
    // Service
    private let stationFinderService = StationFinderService()
    private let locationFinderService = LocationFinderService()
    private var isSearching = false
    private var timer: Timer?
    private let elementsPerPage = 20
    private let locationManager = CLLocationManager()
    
    // Key Store
    private let keyStore = KeyStore.shared
    
    init(type: SearchStationType) {
        self.type = type
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        setupInitialData()
    }
    
    private func setupInitialData() {
        loadRecentStations()
        requestedStations = recentlySearchedStations
    }
    
    private func setupView() {
        view.addSubview(searchBarView)
        view.addSubview(tableView)
        tableView.register(SearchLocationCell.self, forCellReuseIdentifier: SearchLocationCell.cellIdentfier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        
        tableViewHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapCurrentLocation)))
        tableView.tableHeaderView = tableViewHeader
        
        searchBarView.delegate = self
        setupNavBar()
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        searchBarView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeader.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            searchBarView.topAnchor.constraint(equalTo: view.topAnchor),
            searchBarView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchBarView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            tableView.topAnchor.constraint(equalTo: searchBarView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            tableViewHeader.widthAnchor.constraint(equalTo: tableView.widthAnchor),
            tableViewHeader.topAnchor.constraint(equalTo: tableView.topAnchor),
            tableViewHeader.centerXAnchor.constraint(equalTo: tableView.centerXAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        tableView.tableHeaderView?.layoutIfNeeded()
        tableView.tableHeaderView = tableView.tableHeaderView
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.5085967183, green: 0.07502175122, blue: 0.2506508231, alpha: 1)
        navigationController?.navigationBar.shadowImage = UIImage() //removes the border between the navbar and view
        
        closeButton = UIBarButtonItem(image: Asset.icNavigationCloseEnabled.image, style: .done, target: self, action: #selector(didPressBackButton))
        closeButton!.tintColor = .white
        
        let navBarTitleLabel = UILabel()
        navBarTitleLabel.text = type.text
        navBarTitleLabel.contentMode = .left
        navBarTitleLabel.textColor = .white
        navBarTitleLabel.font = Font.barlowSemiBold(size: .large)
        
        navigationItem.leftBarButtonItem = closeButton
        navigationItem.titleView = navBarTitleLabel
    }
    
    private func updateTableView() {
        self.tableView.backgroundView = requestedStations.isEmpty ? self.placeholderView : nil
        self.tableView.separatorStyle = requestedStations.isEmpty ? .none : .singleLine
        self.tableView.tableHeaderView = requestedStations.isEmpty ? nil : self.tableViewHeader
    }
    
    private func createTimer(execute executionBlock: @escaping (Timer) -> Void) -> Timer {
        return Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: executionBlock)
    }
    
    private func loadStops(newValue: String, loadNextPage: Bool, elementsPerPage: Int) {
        timer = createTimer { [weak self] _ in
            guard let self = self else { return }
            self.stationFinderService.getStops(loadNextPage: loadNextPage, elementsPerPage: elementsPerPage, prefix: newValue, completion: { [weak self] stopResponse in
                
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.tableView.tableFooterView = self.spinnerView
                    self.spinnerView.startAnimation()
                }
                
                if loadNextPage {
                    stopResponse.results.forEach({ self.requestedStations.append($0) })
                } else {
                    self.requestedStations = stopResponse.results
                }
                
                DispatchQueue.main.async {
                    self.tableView.tableFooterView = nil
                    self.spinnerView.stopAnimation()
                    self.updateTableView()
                    self.tableView.reloadData()
                }
                
            }, failure: { [weak self] error in
                self?.presentAlert(customTitle: "Error!", customMessage: "Cannot search for stations! Check your internet connection")
            })
        }
    }
    
    private func loadRecentStations() {
        if !keyStore.recentSearches.isEmpty {
            stationFinderService.getRecentStops(recentIds: keyStore.recentSearches, completion: { [weak self] stations in
                guard let self = self else { return }
                self.recentlySearchedStations = stations
                self.requestedStations = stations
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                }, failure: { [weak self ] error in
                    self?.presentAlert(customTitle: "Error!", customMessage: "Cannot retrieve the recent stations! Check your internet connection")
            })
        }
    }
    
    @objc private func didTapCurrentLocation() {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc private func didPressBackButton() {
        delegate?.didPressBackButton(on: self)
    }
}

extension SearchStationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if recentlySearchedStations.contains(requestedStations[indexPath.row]) {
            recentlySearchedStations.removeAll(where: { $0 == requestedStations[indexPath.row] })
        } else if recentlySearchedStations.count >= maxSizeRecents {
            recentlySearchedStations.removeLast()
        }
        recentlySearchedStations.insert(requestedStations[indexPath.row], at: 0)
        keyStore.recentSearches = recentlySearchedStations.map({ return $0.id })
        
        // dismiss window after selection
        delegate?.didSelectLocation(on: self, location: requestedStations[indexPath.row], viewControllerType: type)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return LayoutValues.rowHeight
    }
}

extension SearchStationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestedStations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchLocationCell.cellIdentfier, for: indexPath) as? SearchLocationCell else {
            fatalError("Error dequeue-ing")
        }
        
        cell.updateUI(with: requestedStations[indexPath.row].name, subtextSize: searchedText?.count ?? 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return isSearching ? "" : "Recent"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .white
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let isLastCellDisplayed = indexPath.row == requestedStations.count - 1
        
        if isLastCellDisplayed && stationFinderService.canLoadMore {
            loadStops(newValue: searchedText ?? "", loadNextPage: true, elementsPerPage: elementsPerPage)
        }
    }
}

extension SearchStationViewController: SearchBarViewDelegate {
    func textDidChange(newValue: String) {
        searchedText = newValue
        
        if searchedText!.isEmpty {
            timer?.invalidate()
            isSearching = false
            requestedStations = recentlySearchedStations
            stationFinderService.canLoadMore = false
            tableView.backgroundView = nil
            tableView.tableHeaderView = self.tableViewHeader
            tableView.separatorStyle = .singleLine
            tableView.reloadData()
        } else {
            isSearching = true
            
            if let unwrapedTimer = timer {
                unwrapedTimer.invalidate()
            }
            loadStops(newValue: newValue, loadNextPage: false, elementsPerPage: elementsPerPage)
        }
    }
}

extension SearchStationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location?.coordinate else { return }
        locationFinderService.getClosestStation(latitude: location.latitude, longitude: location.longitude, completion: { [weak self] stations in
            guard let self = self else { return }
            self.delegate?.didSelectLocation(on: self, location: stations[0], viewControllerType: self.type)
        }, failure: { [weak self] error in
            self?.presentAlert(customTitle: "Error!", customMessage: "Cannot retrieve the current location! Check your internet connection")
        })
    }
}
