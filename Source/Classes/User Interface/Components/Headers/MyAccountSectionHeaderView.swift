//
//  MyAccountSectionHeaderView.swift
//  Choo Choo
//
//  Created by halcyonmobile on 31/07/2019.
//

import UIKit

class MyAccountSectionHeaderView: UITableViewHeaderFooterView {
    private let titleSection = UILabel()
    var sectionName: String? {
        didSet {
            titleSection.text = sectionName
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initView()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initView() {
        contentView.backgroundColor = .white
        titleSection.font = Font.barlowBold(size: .medium)
        contentView.addSubview(titleSection)
    }
    
    private func setUpConstraints() {
        var constraints = [NSLayoutConstraint] ()
        
        titleSection.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(titleSection.topAnchor.constraint(equalTo: contentView.centerYAnchor))
        constraints.append(titleSection.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: Padding.custom))
        constraints.append(titleSection.trailingAnchor.constraint(lessThanOrEqualTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -Padding.medium))
        
        NSLayoutConstraint.activate(constraints)
    }
}
