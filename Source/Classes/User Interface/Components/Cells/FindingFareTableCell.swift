//
//  FindingFareTableCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import UIKit

class FindingFareTableCell: UITableViewCell {
    private let shadowContainerView : UIView = UIView()
    private let fareDetailsView: FareDetailsView = FareDetailsView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        shadowContainerView.addSubview(fareDetailsView)
        
        shadowContainerView.layer.backgroundColor = UIColor.white.cgColor
        shadowContainerView.layer.masksToBounds = false
        shadowContainerView.layer.cornerRadius = StyleValues.radiusValue
        shadowContainerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        shadowContainerView.layer.shadowOpacity = StyleValues.shadowOpacity
        shadowContainerView.layer.shadowColor = UIColor.black.cgColor
        shadowContainerView.layer.shadowRadius = StyleValues.radiusValue
        
        contentView.addSubview(shadowContainerView)
        contentView.sendSubviewToBack(shadowContainerView)
    }

    private func setupConstraints() {
        fareDetailsView.translatesAutoresizingMaskIntoConstraints = false
        shadowContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            shadowContainerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: PaddingValues.verySmallPadding),
            shadowContainerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: PaddingValues.mediumPadding),
            shadowContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -PaddingValues.smallPadding),
            shadowContainerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -PaddingValues.mediumPadding),
            
            fareDetailsView.topAnchor.constraint(equalTo: shadowContainerView.topAnchor),
            fareDetailsView.leadingAnchor.constraint(equalTo: shadowContainerView.leadingAnchor),
            fareDetailsView.bottomAnchor.constraint(equalTo: shadowContainerView.bottomAnchor),
            fareDetailsView.trailingAnchor.constraint(equalTo: shadowContainerView.trailingAnchor)
        ])
    }

    public func configure(withFare journey: Journey) {
        fareDetailsView.configure(withJourney: journey)
    }
}

extension FindingFareTableCell {
    private enum PaddingValues {
        static let verySmallPadding: CGFloat = 4.0
        static let smallPadding: CGFloat = 8.0
        static let mediumPadding: CGFloat = 16.0
        static let bigPadding: CGFloat = 24.0
        static let hugePadding: CGFloat = 48.0
    }
    
    private enum StyleValues {
        static let radiusValue: CGFloat = 6.0
        static let shadowOpacity: Float = 0.3
        static let buttonWidth: CGFloat = 91.0
        static let buttonHeight: CGFloat = 34.0
    }
}

extension String {
    func getHourMinutes() -> String {
        return self.split(separator: ":")[0] + ":" + self.split(separator: ":")[1]
    }
}
