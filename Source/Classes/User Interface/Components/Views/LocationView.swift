//
//  LocationView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 30/07/2019.
//

import UIKit

class LocationView: UIView {
    
    private var locationViewType: LocationViewType
    private let destinationTypeLabel = UILabel()
    private let inputLocationTextField = UITextField()
    
    var inputLocation: String {
        get { return inputLocationTextField.text ?? "" }
        set { inputLocationTextField.text = newValue }
    }
    
    var didTouchTextField: (() -> Void)?
    
    init(locationViewType: LocationViewType) {
        self.locationViewType = locationViewType
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(destinationTypeLabel)
        addSubview(inputLocationTextField)
        destinationTypeLabel.text = locationViewType.title
        destinationTypeLabel.font = Font.barlowRegular(size: FontSize.mediumSmall)
        inputLocationTextField.placeholder = locationViewType.placeholder
        inputLocationTextField.font = Font.barlowMedium(size: FontSize.mediumLarge)
        inputLocationTextField.addTarget(self, action: #selector(didTouchedTextField), for: .touchDown)
    }
    
    private func setupConstraints() {
        var constraints: [NSLayoutConstraint] = []
        
        destinationTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(destinationTypeLabel.topAnchor.constraint(equalTo: topAnchor, constant: AppStyle.PaddingValues.mediumPadding))
        constraints.append(destinationTypeLabel.leadingAnchor.constraint(equalTo: leadingAnchor))
        constraints.append(destinationTypeLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor))
        
        inputLocationTextField.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(inputLocationTextField.topAnchor.constraint(equalTo: destinationTypeLabel.bottomAnchor, constant: AppStyle.PaddingValues.smallPadding))
        constraints.append(inputLocationTextField.heightAnchor.constraint(equalToConstant: LayoutValues.smallHeight))
        constraints.append(inputLocationTextField.widthAnchor.constraint(equalToConstant: LayoutValues.bigWidth))
        constraints.append(inputLocationTextField.bottomAnchor.constraint(equalTo: bottomAnchor))

        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func didTouchedTextField() {
        didTouchTextField?()
    }
}

extension LocationView {
    private enum LayoutValues {
        static let bigWidth: CGFloat = 250
        static let smallHeight: CGFloat = 40
    }
}
