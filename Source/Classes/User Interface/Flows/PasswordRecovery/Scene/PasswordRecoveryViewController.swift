//
//  PasswordRecoveryViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit

protocol PasswordRecoveryViewControllerFlowDelegate: AnyObject {
    func didTouchSendLinkButton(on viewController: PasswordRecoveryViewController, email: String)
}

class PasswordRecoveryViewController: BaseViewController {
    
    enum Paddings {
        static let noPadding: CGFloat = 0
        static let smallestPadding: CGFloat = 1
        static let cornerRadius: CGFloat = 5
        static let bigPadding: CGFloat = 36
        static let veryBigPadding: CGFloat = 48
    }
    
    weak var delegate: PasswordRecoveryViewControllerFlowDelegate?
    private let authService = AuthenticationService()
    
    private let mainContent = UIView()
    private let emailUnderline = UIView()
    
    private var mainSackView: UIStackView!
    private var emailStackView: UIStackView!
    
    private let titleLabel = UILabel()
    private let informationLabel = UILabel()
    private let emailLabel = UILabel()
    
    private let sendLinkButton = UIButton()
    
    private let emailIcon = UIImageView()
    private let backButton = UIImageView()
    
    private var emailTextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initConstraints()
    }
    
    private func initView() {
        emailTextField.delegate = self
        
        emailIcon.image = Asset.icNavigationEmail.image
        emailIcon.tintColor = .iconColor
        emailTextField.placeholder = "Email address"
        emailTextField.autocapitalizationType = .none
        emailUnderline.backgroundColor = .textDescriptionColor
        emailLabel.text = "Email address"
        emailLabel.textColor = .white
        emailLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        setupLabels()
        setupStackViews()
        setupNavBar()
        setupButtons()
        
        view.backgroundColor = .white
        
        mainContent.addSubview(mainSackView)
        view.addSubview(mainContent)
    }
    
    private func setupButtons() {
        sendLinkButton.setTitle("Send reset link", for: .normal)
        sendLinkButton.titleLabel?.font = Font.barlowBold(size: .medium)
        sendLinkButton.backgroundColor = .lola
        sendLinkButton.isEnabled = false
        sendLinkButton.layer.cornerRadius = Paddings.cornerRadius
        sendLinkButton.addTarget(self, action: #selector(sendLinkButtonTapped), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "Password recovery"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain, target: self,
                                                           action: #selector(backButtonTapped))
    }
    
    private func setupLabels() {
        titleLabel.text = "Forgot your password?"
        titleLabel.font = Font.barlowBold(size: .mediumLarge)
        
        informationLabel.text = "Please enter the email address for your account. We’ll send you a link for resetting your password."
        informationLabel.font = Font.barlowRegular(size: .medium)
        informationLabel.numberOfLines = 3
    }
    
    private func setupStackViews() {
        emailStackView = UIStackView(arrangedSubviews: [emailIcon, emailTextField])
        emailStackView.axis = .horizontal
        emailStackView.distribution = .fillProportionally
        emailStackView.spacing = Padding.mediumBig
        
        mainSackView = UIStackView(arrangedSubviews: [titleLabel, informationLabel, emailLabel, emailStackView, emailUnderline, sendLinkButton])
        mainSackView.axis = .vertical
        mainSackView.distribution = .fill
        mainSackView.spacing = Padding.mediumBig
    }
    
    private func initConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        mainContent.translatesAutoresizingMaskIntoConstraints = false
        mainSackView.translatesAutoresizingMaskIntoConstraints = false
        emailIcon.translatesAutoresizingMaskIntoConstraints = false
        sendLinkButton.translatesAutoresizingMaskIntoConstraints = false
        informationLabel.translatesAutoresizingMaskIntoConstraints = false
        emailUnderline.translatesAutoresizingMaskIntoConstraints = false
        
        constraints.append(mainContent.topAnchor.constraint(equalTo: view.topAnchor, constant: Paddings.noPadding))
        constraints.append(mainContent.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Paddings.noPadding))
        constraints.append(mainContent.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: Paddings.noPadding))
        constraints.append(mainContent.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: Paddings.noPadding))
        
        constraints.append(mainSackView.topAnchor.constraint(equalTo: mainContent.topAnchor, constant: Paddings.bigPadding))
        constraints.append(mainSackView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.medium))
        constraints.append(mainSackView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Padding.medium)))
        
        constraints.append(emailUnderline.heightAnchor.constraint(equalToConstant: Paddings.smallestPadding))
        
        constraints.append(emailIcon.widthAnchor.constraint(equalToConstant: Padding.extraLarge))
        constraints.append(emailIcon.heightAnchor.constraint(equalToConstant: Padding.big))
        
        constraints.append(sendLinkButton.heightAnchor.constraint(equalToConstant: Paddings.veryBigPadding))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func sendLinkButtonTapped() {
        sendLinkButton.isEnabled = false
        let email = EmailDto(email: emailTextField.text ?? "")
        let emailString: String = self.emailTextField.text ?? ""
        authService.recoverPassword(email: email, completion: {
            let alert = UIAlertController(title: "Cool!", message: "Email sent", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }, failure: { _ in
            let alert = UIAlertController(title: "Ups!", message: "Something went wrong. Try again!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        })
        self.delegate?.didTouchSendLinkButton(on: self, email: emailString)
    }
    
    @objc private func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
}

extension PasswordRecoveryViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        emailLabel.textColor = .primary
        emailUnderline.backgroundColor = .primary
        emailUnderline.frame.size.height = 2
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let userEnteredString = emailTextField.text, let enteredString = userEnteredString as NSString?,
            let newEnteredString = enteredString.replacingCharacters(in: range, with: string) as NSString? else {
            return false
        }
        if newEnteredString != "" {
            sendLinkButton.backgroundColor = .primary
            sendLinkButton.isEnabled = true
        } else {
            sendLinkButton.backgroundColor = .lola
            sendLinkButton.isEnabled = false
        }
        return true
    }
}
