//
//  DestinationFacilitiesCell.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 08/08/2019.
//

import UIKit

class DestinationFacilitiesCell: UITableViewCell {
    public static let cellIdentifier = "DestinationFacilitiesCell"
    private let transportationIcon = UIImageView()
    private let walkingIcon = UIImageView()
    private let walkingLabel = UILabel()
    private let stackView = UIStackView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        contentView.addSubview(transportationIcon)
        contentView.addSubview(walkingIcon)
        contentView.addSubview(walkingLabel)
        contentView.addSubview(stackView)
        
        walkingIcon.contentMode = .scaleAspectFill
        walkingIcon.image = Asset.icTicketDetailsWalk.image
        walkingLabel.font = Font.barlowRegular(size: .small)
        transportationIcon.contentMode = .scaleAspectFill
        stackView.axis = .horizontal
        stackView.spacing = 10.0
        stackView.distribution = .equalCentering
    }
    
    func updateUI(facility: Facility) {
        walkingLabel.text = "\(facility.walkingTime) min"
        transportationIcon.image = facility.type.image
        
        for connection in facility.connections {
            let label = UILabel()
            label.text = hasBorderedText(type: facility.type) ? " \(connection) " : connection
            label.layer.cornerRadius = 4
            label.layer.borderColor = hasBorderedText(type: facility.type) ? UIColor.textDescriptionColor.cgColor : UIColor.white.cgColor
            label.layer.borderWidth = 1
            stackView.addArrangedSubview(label)
        }
    }
    
    private func hasBorderedText(type: FacilityType) -> Bool {
        switch type {
        case .bus, .tram: return true
        case .taxi, .bike: return false
        }
    }
    
    private func setupConstraints() {
        transportationIcon.translatesAutoresizingMaskIntoConstraints = false
        walkingLabel.translatesAutoresizingMaskIntoConstraints = false
        walkingIcon.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            transportationIcon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            transportationIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            
            walkingLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            walkingLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -AppStyle.PaddingValues.mediumSmallPadding),
            
            walkingIcon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            walkingIcon.trailingAnchor.constraint(equalTo: walkingLabel.leadingAnchor, constant: -AppStyle.PaddingValues.smallPadding),
            
            stackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: transportationIcon.trailingAnchor, constant: AppStyle.Layout.padding),
            stackView.trailingAnchor.constraint(lessThanOrEqualTo: walkingIcon.leadingAnchor, constant: -AppStyle.Layout.padding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
