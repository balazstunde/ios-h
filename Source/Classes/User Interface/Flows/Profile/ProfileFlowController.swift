//
//  ProfileFlowController.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import UIKit

class ProfileFlowController: NavigationFlowController {
    
    override var firstScreen: UIViewController {
        let profileViewController = ProfileViewController()
        return profileViewController
    }
}
