//
//  ResetPasswordDto.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation

class ResetPasswordDto: Codable {
    var password: String
    var resetPasswordToken: String
    
    enum CodingKeys: String, CodingKey {
        case password = "password"
        case resetPasswordToken = "resetPasswordToken"
    }
}
