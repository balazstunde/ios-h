//
//  TrainLoadingAnimationView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 02/08/2019.
//

import UIKit

final class TrainLoadingAnimationView: UIView {
    
    private let leftLoadingBar = UIView()
    private let rightLoadingBar = UIView()
    private let trainImageView = UIImageView()
    private let leftCircle = UIView()
    private let rightCircle = UIView()
    private let leftLabel = UILabel()
    private let rightLabel = UILabel()
    private let animationDuration = 2.0
    
    private enum LoadingScreen {
        static let smallPadding: CGFloat = 10.0
        static let padding: CGFloat = 10.0
        static let circleHeight: CGFloat = 17
        static let circleBorderWidth: CGFloat = 3.0
        static let circleCornerRadius: CGFloat = 8.5
        static let loadingBarHeight: CGFloat = 5.0
        static let loadingBarWidth: CGFloat = 1.0
        static let loadingBarCornerRadius: CGFloat = 4.0
    }
    
    init(from departure: String, to destination: String) {
        super.init(frame: .zero)

        setupView(departure: departure, destination: destination)
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var didFinishAnimation: (() -> Void)?
    
    private func setupView(departure: String, destination: String) {
        addSubview(leftLoadingBar)
        addSubview(rightLoadingBar)
        addSubview(leftLabel)
        addSubview(rightLabel)
        addSubview(leftCircle)
        addSubview(rightCircle)
        addSubview(trainImageView)

        backgroundColor = .white
        
        leftCircle.backgroundColor = .white
        leftCircle.layer.borderColor = UIColor.shiraz.cgColor
        leftCircle.layer.borderWidth = LoadingScreen.circleBorderWidth
        leftCircle.layer.cornerRadius = LoadingScreen.circleCornerRadius
        
        rightCircle.backgroundColor = .white
        rightCircle.layer.borderColor = UIColor.shiraz.cgColor
        rightCircle.layer.borderWidth = LoadingScreen.circleBorderWidth
        rightCircle.layer.cornerRadius = LoadingScreen.circleCornerRadius
        
        leftLabel.text = departure
        leftLabel.textAlignment = .center
        leftLabel.font = Font.barlowMedium(size: .medium)
        
        rightLabel.text = destination
        rightLabel.textAlignment = .center
        rightLabel.font = Font.barlowMedium(size: .medium)
        
        trainImageView.image = Asset.icNavigationTrain.image
        trainImageView.image?.withRenderingMode(.alwaysTemplate)
        trainImageView.tintColor = .shiraz
        trainImageView.contentMode = .scaleAspectFit
        
        leftLoadingBar.backgroundColor = .shiraz
        leftLoadingBar.layer.cornerRadius = LoadingScreen.loadingBarCornerRadius
        rightLoadingBar.backgroundColor = .lola
        rightLoadingBar.layer.cornerRadius = LoadingScreen.loadingBarCornerRadius
    }
    
    private func setupConstraints() {
        leftCircle.translatesAutoresizingMaskIntoConstraints = false
        rightCircle.translatesAutoresizingMaskIntoConstraints = false
        leftLabel.translatesAutoresizingMaskIntoConstraints = false
        rightLabel.translatesAutoresizingMaskIntoConstraints = false
        trainImageView.translatesAutoresizingMaskIntoConstraints = false
        leftLoadingBar.translatesAutoresizingMaskIntoConstraints = false
        rightLoadingBar.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            leftCircle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LoadingScreen.padding),
            leftCircle.centerYAnchor.constraint(equalTo: centerYAnchor),
            leftCircle.heightAnchor.constraint(equalToConstant: LoadingScreen.circleHeight),
            leftCircle.widthAnchor.constraint(equalTo: leftCircle.heightAnchor),
            
            leftLabel.topAnchor.constraint(equalTo: leftCircle.bottomAnchor, constant: LoadingScreen.smallPadding),
            leftLabel.leadingAnchor.constraint(equalTo: leftCircle.leadingAnchor),
            
            trainImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            leftLoadingBar.heightAnchor.constraint(equalToConstant: LoadingScreen.loadingBarHeight),
            leftLoadingBar.widthAnchor.constraint(equalToConstant: LoadingScreen.loadingBarWidth),
            leftLoadingBar.leadingAnchor.constraint(equalTo: leftCircle.trailingAnchor, constant: LoadingScreen.padding),
            leftLoadingBar.trailingAnchor.constraint(equalTo: trainImageView.leadingAnchor, constant: -LoadingScreen.padding),
            leftLoadingBar.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            rightLoadingBar.leadingAnchor.constraint(equalTo: trainImageView.trailingAnchor, constant: LoadingScreen.padding),
            rightLoadingBar.trailingAnchor.constraint(equalTo: rightCircle.leadingAnchor, constant: -LoadingScreen.padding),
            rightLoadingBar.centerYAnchor.constraint(equalTo: centerYAnchor),
            rightLoadingBar.heightAnchor.constraint(equalToConstant: LoadingScreen.loadingBarHeight),
            
            rightCircle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LoadingScreen.padding),
            rightCircle.centerYAnchor.constraint(equalTo: centerYAnchor),
            rightCircle.heightAnchor.constraint(equalToConstant: LoadingScreen.circleHeight),
            rightCircle.widthAnchor.constraint(equalTo: rightCircle.heightAnchor),
            
            rightLabel.topAnchor.constraint(equalTo: rightCircle.bottomAnchor, constant: LoadingScreen.smallPadding),
            rightLabel.trailingAnchor.constraint(equalTo: rightCircle.trailingAnchor),
            
            leftLabel.trailingAnchor.constraint(lessThanOrEqualTo: rightLabel.leadingAnchor, constant: -LoadingScreen.smallPadding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func stopAnimation() {
        trainImageView.layer.removeAllAnimations()
        leftLoadingBar.layer.removeAllAnimations()
        rightLoadingBar.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        let distance = rightLoadingBar.frame.size.width
        let rightLoadingBarFrame = rightLoadingBar.frame
        
        UIView.animate(withDuration: animationDuration, delay: 0.0, options: .curveEaseInOut, animations: { [weak self] in
            guard let self = self else { return }
            self.trainImageView.frame.origin = CGPoint(x: self.trainImageView.frame.origin.x + distance, y: self.trainImageView.frame.origin.y)
            
            self.leftLoadingBar.frame.size = CGSize(width: distance, height: self.leftLoadingBar.frame.size.height)
            self.rightLoadingBar.frame = CGRect(x: rightLoadingBarFrame.origin.x + distance, y: rightLoadingBarFrame.origin.y, width: 0, height: rightLoadingBarFrame.size.height)
            }, completion: { (_ : Bool) in
                self.stopAnimation()
                self.didFinishAnimation?()
            })
    }
}
