//
//  SearchStationPlaceholderView.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 08/08/2019.
//

import UIKit

class SearchStationPlaceholderView: UIView {
    
    private let topLabel = UILabel()
    private let bottomLabel = UILabel()
    
    private enum LayoutValues {
        static let errorTextPadding: CGFloat = 40.0
    }
    
    init() {
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        
        topLabel.text = "We've found no matching stations."
        topLabel.textAlignment = .center
        topLabel.font = Font.barlowBold(size: .medium)
        
        bottomLabel.text = "Please check if there's a typo and try again."
        bottomLabel.textAlignment = .center
        bottomLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        addSubview(topLabel)
        addSubview(bottomLabel)
    }
    
    private func setupConstraints() {
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            topLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            topLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -LayoutValues.errorTextPadding),
            bottomLabel.topAnchor.constraint(equalTo: topLabel.bottomAnchor),
            bottomLabel.centerXAnchor.constraint(equalTo: topLabel.centerXAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
