//
//  Station.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 06/08/2019.
//

import Foundation

struct Station: Codable, Equatable {
    let id: Int
    let name: String
}
