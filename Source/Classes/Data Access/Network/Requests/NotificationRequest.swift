//
//  NotificationRequest.swift
//  Choo Choo
//
//  Created by Halcyon on 21/08/2019.
//

import Foundation

extension Request.NotificationRequest {
    struct PutNotificationRequest: RequestRepresentable {
        var paramType: PostParamType = .body
        var suffix: String
        var method: HTTPMethod = .put
        var parameters: Parameters?
        
        init(userId: Int, changedPlatform: Bool, marketing: Bool, push: Bool, trainArriving: Bool, tripDelay: Bool, tripDisruption: Bool) {
            suffix = API.Path.signup + "/\(userId)" + API.Path.notificationSettings
            parameters = [API.Param.changedPlatformNotification: changedPlatform,
                               API.Param.marketingNotification: marketing,
                               API.Param.pushNotification: push,
                               API.Param.trainArrivingNotification: trainArriving,
                               API.Param.tripDelayNotification: tripDelay,
                               API.Param.tripDisruptionNotification: tripDisruption]
        }
    }
    
    struct GetNotificationRequest: RequestRepresentable {
        let paramType: PostParamType = .body
        var suffix: String
        var method: HTTPMethod = .get
        var parameters: Parameters?
        
        init(userId: Int) {
            suffix = API.Path.signup + "/\(userId)" + API.Path.notifications
        }
    }
}
