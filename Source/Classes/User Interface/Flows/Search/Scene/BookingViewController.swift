//
//  BookingViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 05/08/2019.
//

import Foundation
import UIKit

struct Service {
    let icon: UIImage
    let name: String
}

enum TrainClass: String {
    case firstClass = "1st Class"
    case secondClass = "2nd Class"
    case coach = "Coach"
}

protocol BookingViewControllerDelegate: AnyObject {
    func presentActivityVC(on viewController: UIViewController, departureStation: String, destinationStation: String, trainNumber: String, departureTime: String)
    func didTapToLabel(on viewController: BookingViewController, type: SearchStationType)
    func didTapFromLabel(on viewController: BookingViewController, type: SearchStationType)
    func didTapCheckOutButton(on viewController: BookingViewController, journey: Journey)
    func didTapSeeStopsButton(on viewController: BookingViewController, journey: Journey)
}

class BookingViewController: BaseViewController {
    private let header = BookingHeaderView()
    private var ticketDetailsView: TicketDetailsHeaderView!
    private let tableView = UITableView()
    private let checkOutButton = MainButton(title: "Go to checkout")
    private let journey: Journey!
    private let bookingService: BookingService!
    private let trainClass = TrainClass.secondClass
    
    weak var delegate: BookingViewControllerDelegate?
    
    private let services = [
        Service(icon: Asset.icNavigationWifi.image, name: "Wi-fi"),
        Service(icon: Asset.icNavigationPower.image, name: "Power plug adapter"),
        Service(icon: Asset.icNavigationBike.image, name: "Bike facilities"),
        Service(icon: Asset.icNavigationRestaurant.image, name: "Restaurant"),
        Service(icon: Asset.icNavigationSleeping.image, name: "Sleeping cart")
    ]
    
    init(journey: Journey, bookingService: BookingService) {
        self.journey = journey
        self.bookingService = bookingService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        ticketDetailsView = TicketDetailsHeaderView(widthSize: view.frame.size.width)
        super.viewDidLoad()
        
        view.backgroundColor = .alabaster
        setupView()
        
        setupTicketTopDetailsView()
        setupTicketBottomDetailsView()
        setupTicketPriceView()
        
        setupConstraints()
        setupListeners()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        ticketDetailsView.updateDashedView()
    }
    
    private func setupView() {
        view.addSubview(header)
        view.addSubview(tableView)
        view.addSubview(checkOutButton)
        
        navigationItem.title = "Booking"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTicket))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: Asset.icNavigationBack.image, style: .plain,
                                                           target: self, action: #selector(didTapBackButton))
        
        setupTableView()
        
        checkOutButton.layer.cornerRadius = LayoutValues.cornerRadius
        
        header.setTitleText(with: "Here is the selected train from")
        
        let fromLocation = bookingService.ticketDetails.fromDestination?.name ?? ""
        let toLocation = bookingService.ticketDetails.toDestination?.name ?? ""
        
        header.setFromLabelText(with: fromLocation)
        header.setToLabelText(with: toLocation)
    }
    
    private func setupListeners() {
        header.didTapFromLabelClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapFromLabel(on: self, type: .departure)
        }
        
        header.didTapToLabelClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapToLabel(on: self, type: .destination)
        }
        
        checkOutButton.didTapButtonClosure = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapCheckOutButton(on: self, journey: self.journey)
        }
        
        ticketDetailsView.didTapSeeStopsButton = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didTapSeeStopsButton(on: self, journey: self.journey)
        }
    }
    
    private func setupTableView() {
        tableView.tableHeaderView = ticketDetailsView
        tableView.register(TrainServicesCell.self, forCellReuseIdentifier: "TrainServicesCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupConstraints() {
        header.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        ticketDetailsView.translatesAutoresizingMaskIntoConstraints = false
        checkOutButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            header.topAnchor.constraint(equalTo: view.topAnchor),
            header.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            header.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            checkOutButton.heightAnchor.constraint(equalToConstant: LayoutValues.buttonHeight),
            checkOutButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.marginPadding),
            checkOutButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.marginPadding),
            checkOutButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -LayoutValues.marginPadding),
            
            tableView.topAnchor.constraint(equalTo: header.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: checkOutButton.topAnchor, constant: -LayoutValues.marginPadding),
            
            ticketDetailsView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            ticketDetailsView.widthAnchor.constraint(equalTo: tableView.widthAnchor),
            ticketDetailsView.topAnchor.constraint(equalTo: tableView.topAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
        tableView.tableHeaderView?.layoutIfNeeded()
    }
    
    @objc private func shareTicket() {
        delegate?.presentActivityVC(on: self, departureStation: header.getFromLabelText(), destinationStation: header.getToLabelText(), trainNumber: ticketDetailsView.getTrainNumberLabelText(), departureTime: ticketDetailsView.getDateTimeLabelText())
    }
    
    @objc private func didTapBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    private func setupTicketTopDetailsView() {
        let trainName = journey.trainIdentifier
        
        guard let departureTime = DateFormatter.getStringAsDate(inFormat: DateFormats.hourMinutesSeconds, date: journey.departureStation.departureTime),
            let arrivalTime = DateFormatter.getStringAsDate(inFormat: DateFormats.hourMinutesSeconds, date: journey.arrivalStation.arrivalTime) else {
                print("error while parsing fare time")
                return
        }
        let totalTime = DateFormatter.getIntervalBetween(startDate: arrivalTime, endDate: departureTime)
        
        let time = journey.departureStation.departureTime.getHourMinutes() + " - " + journey.arrivalStation.arrivalTime.getHourMinutes()
        let date = DateFormatter.getDateAsString(inFormat: .nameOfTheDayMonthDay, date: bookingService.ticketDetails.date)
        
        let fromTo = journey.departureStation.stopName + " - " + journey.arrivalStation.stopName
        
        ticketDetailsView.setupTicketTopDetailsView(trainName: trainName, totalTime: totalTime, date: date, time: time, fromTo: fromTo)
    }
    
    private func setupTicketBottomDetailsView() {
        ticketDetailsView.setupTicketBottomDetailsView(trainClass: trainClass.rawValue)
    }
    
    private func setupTicketPriceView() {
        var passengers: [String] = []
        var price: [String] = []
        var totalPrice: Double = 0
        
        guard let map = bookingService.getDetails() else { return }
        guard let priceMap = KeyStore.shared.fareCategoriesPriceMap else { return }
        
        for (person, personNumber) in map where personNumber > 0 {
            passengers.append("\(personNumber) x \(person)")
            
            guard let discountPercentage = priceMap[person] else { continue }
            let priceAsDouble = journey.pricingInformation.secondClassPrice.value * Double(personNumber) * discountPercentage
            price.append("\(String(format: "%.2f", priceAsDouble)) \(journey.pricingInformation.secondClassPrice.unit.rawValue)")
            totalPrice += priceAsDouble
        }
        
        ticketDetailsView.setupTicketPriceView(passenger: passengers.joined(separator: "\n"), price: price.joined(separator: "\n"), totalPrice: String(format: "%.2f", totalPrice) + " " + journey.pricingInformation.secondClassPrice.unit.rawValue)
    }
}

extension BookingViewController {
    private enum LayoutValues {
        static let marginPadding: CGFloat = 16
        static let cornerRadius: CGFloat = 3
        static let buttonHeight: CGFloat = 48
    }
}

extension BookingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = UIView()
        sectionView.backgroundColor = .seashel
        
        let label = UILabel()
        sectionView.addSubview(label)
        label.text = "Train Services"
        label.font = Font.barlowMedium(size: .mediumSmall)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: sectionView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: sectionView.leadingAnchor, constant: LayoutValues.marginPadding).isActive = true
        
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36.0
    }
}

extension BookingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TrainServicesCell", for: indexPath) as? TrainServicesCell else { return UITableViewCell() }

        let service = services[indexPath.row]

        cell.configureCell(name: service.name, icon: service.icon)
        return cell
    }
}
