//
//  Foundation+StaticString.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 07/02/2019.
//

import Foundation

extension URL {

    init(staticString: StaticString) {
        self.init(string: "\(staticString)")!
    }
}
