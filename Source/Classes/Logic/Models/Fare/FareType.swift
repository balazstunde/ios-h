//
//  FareType.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 29/07/2019.
//

import Foundation

enum FareUser: String, Codable {
    case adults = "ADULT"
    case students = "STUDENT"
    case infants = "INFANT"
    case child = "CHILD"
    case youth = "YOUTH"
    case seniors = "SENIOR"
}

enum FareType: String, Codable {
    case standard
    case free
    case discounted
    
    var typeName: String {
        switch self {
        case .discounted:
            return "Discounted fares"
        case .free:
            return "Free of charge"
        case .standard:
            return "Standard fares"
        }
    }
}
