//
//  LoginOptionFlowController.swift
//  Choo Choo
//
//  Created by halcyonmobile on 02/08/2019.
//

import Foundation
import UIKit

protocol LogInOptionFlowControllerDelegate: AnyObject {
    func didLogin(_ loginOptionFlowController: LoginOptionFlowController)
}

class LoginOptionFlowController: NavigationFlowController {

    override var firstScreen: UIViewController {
        let loginOptionViewController = LoginOptionViewController(loginType: LoginType.normal)
        loginOptionViewController.delegate = self
        return loginOptionViewController
    }
    
    weak var flowDelegate: LogInOptionFlowControllerDelegate?
}

extension LoginOptionFlowController: LogInOptionViewControllerFlowDelegate {
    func didTouchLogInButton(on viewController: LoginOptionViewController) {
        let loginFlowController = LogInFlowController(from: self)
        loginFlowController.flowDelegate = self
        loginFlowController.start(presentationStyle: .push)
    }
    
    func didTouchCreateAccountButton(on viewController: LoginOptionViewController) {
        let createAccountFlowController = CreateNewAccountFlowController(from: self)
        createAccountFlowController.start(presentationStyle: .push)
    }
    
    func didTouchTermButton(on viewController: LoginOptionViewController) {
        let browserFlowController = BrowserViewFlowController(browserType: .terms, from: self)
        browserFlowController.start(presentationStyle: FlowPresentationStyle.push)
    }
    
    func didTouchPrivacyButton(on viewController: LoginOptionViewController) {
        let browserFlowController = BrowserViewFlowController(browserType: .privacy, from: self)
        browserFlowController.start(presentationStyle: FlowPresentationStyle.push)
    }
}

extension LoginOptionFlowController: LogInFlowControllerDelegate {
    func didLogin(_ loginFlowController: LogInFlowController) {
        removeChild(flow: loginFlowController)
        flowDelegate?.didLogin(self)
    }
}
