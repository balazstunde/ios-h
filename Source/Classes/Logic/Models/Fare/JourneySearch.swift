//
//  JourneySearch.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import Foundation

struct JourneySearch: Codable {
    let date: String
    let endStationId: Int
    let startStationId: Int
    let desiredTickets: [String: Int]
    let pagingInfo: PageableSearch
}
