//
//  Journey.swift
//  Choo Choo
//
//  Created by Halcyon Mobile on 05/08/2019.
//

import Foundation

struct Journey: Codable {
    let arrivalStation: TrainStop
    let pricingInformation: PricingInformation
    let departureStation: TrainStop
    let stops: [TrainStop]
    let trainIdentifier: String
}
