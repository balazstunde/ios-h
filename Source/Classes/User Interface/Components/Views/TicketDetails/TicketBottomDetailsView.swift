//
//  TicketBottomDetailsView.swift
//  Choo Choo
//
//  Created by Halcyon on 07/08/2019.
//

import Foundation
import UIKit

class TicketBottomDetailsView: UIView {
    private let icon = UIImageView()
    private let classLabel = UILabel()
    private let changeLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        layer.cornerRadius = 3
        
        addSubview(icon)
        addSubview(classLabel)
        addSubview(changeLabel)
        
        icon.image = Asset.icNavigationClass.image
        classLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        changeLabel.font = Font.barlowSemiBold(size: .mediumSmall)
        changeLabel.text = "Change class"
        
        setupConstraints()
    }
    
    func setDetails(trainClass: String) {
        classLabel.text = trainClass
    }
    
    private func setupConstraints() {
        icon.translatesAutoresizingMaskIntoConstraints = false
        classLabel.translatesAutoresizingMaskIntoConstraints = false
        changeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            heightAnchor.constraint(equalToConstant: LayoutValues.viewHeight),
            
            icon.centerYAnchor.constraint(equalTo: centerYAnchor),
            icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            
            classLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            classLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: LayoutValues.labelLeftPadding),
            
            changeLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            changeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension TicketBottomDetailsView {
    private enum LayoutValues {
        static let marginPadding: CGFloat = 16
        static let labelLeftPadding: CGFloat = 10
        static let viewHeight: CGFloat = 40
    }
}
