//
//  UIViewControllerAlert.swift
//  Choo Choo
//
//  Created by halcyonmobile on 12/08/2019.
//

import UIKit

extension UIViewController {
    func presentAlert(customTitle: String, customMessage: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: customTitle, message: customMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
