//
//  Colors.swift
//  Choo Choo
//
//  Created by Atti Tamas on 31/07/2019.
//

import UIKit

extension UIColor {
    static let primary = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
    static let secondary: UIColor = #colorLiteral(red: 0.6980392157, green: 0, blue: 0.2156862745, alpha: 1)
    static let selected: UIColor = #colorLiteral(red: 0.3254901961, green: 0.01176470588, blue: 0.1450980392, alpha: 1)
    static let iconColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
    static let textDescriptionColor = #colorLiteral(red: 0.737254902, green: 0.7019607843, blue: 0.7176470588, alpha: 1)
    static let switchButtonColor = #colorLiteral(red: 0.8666666667, green: 0.8156862745, blue: 0.8392156863, alpha: 1)
    static let switchButtonOffColor = #colorLiteral(red: 0.737254902, green: 0.7019607843, blue: 0.7176470588, alpha: 1)
    static let separatorColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
    static let shiraz = #colorLiteral(red: 0.6980392157, green: 0, blue: 0.2156862745, alpha: 1)
    static let lola = #colorLiteral(red: 0.862745098, green: 0.8156862745, blue: 0.8392156863, alpha: 1)
    static let seashel = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
    static let alabaster = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
    static let alto = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
    static let softPeach = #colorLiteral(red: 0.9568627451, green: 0.9333333333, blue: 0.9450980392, alpha: 1)
    static let highlightedText = #colorLiteral(red: 0.6509803922, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
    
    static let pinkSwan = #colorLiteral(red: 0.737254902, green: 0.7019607843, blue: 0.7176470588, alpha: 1)
    static let paleSlate = #colorLiteral(red: 0.8, green: 0.7529411765, blue: 0.7764705882, alpha: 1)
}
