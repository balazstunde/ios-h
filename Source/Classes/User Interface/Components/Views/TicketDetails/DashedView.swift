//
//  DashedView.swift
//  Choo Choo
//
//  Created by Halcyon on 07/08/2019.
//

import Foundation
import UIKit

class DashedView: UIView {
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
    }
    
    func updateView() {
        addDashedBorder()
        concaveEnds()
    }
}
