//
//  SecondPasswordRecoveryViewController.swift
//  Choo Choo
//
//  Created by Halcyon on 02/08/2019.
//

import UIKit
import MessageUI

class SecondPasswordRecoveryViewController: BaseViewController, MFMailComposeViewControllerDelegate {
    private let authService = AuthenticationService()
    private let mainContent = UIView()
    private let emailLabel = UILabel()
    private let infoLabel = UILabel()
    private let openMailButton = UIButton()
    private let resendLinkbutton = UIButton()
    private var mainStackView: UIStackView!
    private let backArrowIcon = UIImageView()
    private let spinner = UIActivityIndicatorView(style: .gray)
    
    init(email: String) {
        emailLabel.text = email
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initConstraints()
    }
    
    private func initView() {
        view.backgroundColor = .white
        
        setupNavBar()
        setupStackView()
        setupLabels()
        setupButtons()
        
        spinner.isHidden = true
        resendLinkbutton.addSubview(spinner)
        mainContent.addSubview(mainStackView)
        view.addSubview(mainContent)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = .primary
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "Password recovery"
        backArrowIcon.image = Asset.icNavigationBack.image
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backArrowIcon.image, style: .plain, target: self,
                                                           action: #selector(backButtonTapped))
    }
    
    private func setupButtons() {
        openMailButton.setTitle("Open mail", for: .normal)
        openMailButton.titleLabel?.font = Font.barlowBold(size: .medium)
        openMailButton.setTitleColor(UIColor.secondary, for: .normal)
        openMailButton.backgroundColor = .white
        openMailButton.layer.cornerRadius = 5
        openMailButton.layer.borderColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        openMailButton.layer.borderWidth = 2
        openMailButton.addTarget(self, action: #selector(openMailButtonTapped), for: .touchUpInside)
        
        resendLinkbutton.setTitle("Resend link", for: .normal)
        resendLinkbutton.setTitleColor(.black, for: .normal)
        resendLinkbutton.titleLabel?.font = Font.barlowBold(size: .medium)
        resendLinkbutton.backgroundColor = .white
        resendLinkbutton.addTarget(self, action: #selector(resendLinkButtonTapped), for: .touchUpInside)
    }
    
    private func setupLabels() {
        infoLabel.text = "An email with the reset link \nhas been sent to"
        infoLabel.numberOfLines = 2
        infoLabel.font = Font.barlowRegular(size: .medium)
        infoLabel.textAlignment = .center
        
        emailLabel.font = Font.barlowSemiBold(size: .mediumSmall)
    }
    
    private func setupStackView() {
        mainStackView = UIStackView(arrangedSubviews: [infoLabel, emailLabel, openMailButton, resendLinkbutton])
        mainStackView.axis = .vertical
        mainStackView.spacing = Padding.mediumBig
        mainStackView.distribution = .fill
        mainStackView.alignment = .center
    }
    
    private func initConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        mainContent.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        openMailButton.translatesAutoresizingMaskIntoConstraints = false
        spinner.translatesAutoresizingMaskIntoConstraints = false
        resendLinkbutton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints.append(mainContent.topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        constraints.append(mainContent.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        constraints.append(mainContent.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        constraints.append(mainContent.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        
        constraints.append(mainStackView.centerYAnchor.constraint(equalTo: mainContent.centerYAnchor, constant: -(Padding.mediumBig)))
        constraints.append(mainStackView.leadingAnchor.constraint(equalTo: mainContent.leadingAnchor, constant: Padding.mediumBig))
        constraints.append(mainStackView.trailingAnchor.constraint(equalTo: mainContent.trailingAnchor, constant: -(Padding.mediumBig)))
        
        constraints.append(spinner.leadingAnchor.constraint(equalTo: resendLinkbutton.titleLabel!.trailingAnchor, constant: Padding.small))
        
        constraints.append(openMailButton.heightAnchor.constraint(equalToConstant: 49))
        constraints.append(openMailButton.widthAnchor.constraint(equalTo: mainStackView.widthAnchor))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc func openMailButtonTapped() {
        let supportEmail = "abc@xyz.com"
        if let emailURL = URL(string: "mailto:\(supportEmail)"), UIApplication.shared.canOpenURL(emailURL) {
            UIApplication.shared.open(emailURL, options: [:], completionHandler: nil)
        }
    }
    
    @objc func resendLinkButtonTapped() {
        resendLinkbutton.setTitle("Resending link", for: .normal)
        spinner.isHidden = false
        spinner.color = .primary
        spinner.startAnimating()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: {_ in
            self.spinner.stopAnimating()
            self.resendLinkbutton.setTitle("Resend link", for: .normal)
        })
        
        let email = EmailDto(email: emailLabel.text!)
        authService.recoverPassword(email: email, completion: {
            let alert = UIAlertController(title: "Cool!", message: "Email sent", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }, failure: { _ in
            let alert = UIAlertController(title: "Ups!", message: "Something went wrong! Please try again later.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    @objc private func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
}
