//
//  TicketDetailsView.swift
//  Choo Choo
//
//  Created by Halcyon on 07/08/2019.
//

import Foundation
import UIKit

class TicketTopDetailsView: UIView {
    private let checkMarkIcon = UIImageView()
    private let outboundLabel = UILabel()
    private let trainNameLabel = UILabel()
    private let totalTimeLabel = UILabel()
    private let dateLabel = UILabel()
    private let timeLabel = UILabel()
    private let fromToLabel = UILabel()
    private let seeStopsButton = SeeStopsButton()
    
    var didTapSeeStopsButtonClosure: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        layer.cornerRadius = 3
        
        setupIcon()
        setupLabels()
        setupButton()
        
        setupConstraints()
    }
    
    private func setupIcon() {
        addSubview(checkMarkIcon)
        
        checkMarkIcon.image = Asset.icNavigationCheckMark.image
        checkMarkIcon.image = checkMarkIcon.image?.withRenderingMode(.alwaysTemplate)
        checkMarkIcon.tintColor = .shiraz
    }
    
    private func setupLabels() {
        addSubview(outboundLabel)
        addSubview(trainNameLabel)
        addSubview(totalTimeLabel)
        addSubview(dateLabel)
        addSubview(timeLabel)
        addSubview(fromToLabel)
        
        outboundLabel.text = "Outbound"
        outboundLabel.font = Font.barlowRegular(size: .mediumSmall)
        
        trainNameLabel.font = Font.barlowRegular(size: .mediumSmall)
        totalTimeLabel.font = Font.barlowRegular(size: .mediumSmall)
        dateLabel.font = Font.barlowBold(size: .mediumExtra)
        timeLabel.font = Font.barlowBold(size: .mediumExtra)
        fromToLabel.font = Font.barlowRegular(size: .mediumSmall)
    }
    
    private func setupButton() {
        addSubview(seeStopsButton)
        
        seeStopsButton.addTarget(self, action: #selector(didTapSeeStopsButton), for: .touchUpInside)
    }
    
    private func setupConstraints() {
        checkMarkIcon.translatesAutoresizingMaskIntoConstraints = false
        outboundLabel.translatesAutoresizingMaskIntoConstraints = false
        trainNameLabel.translatesAutoresizingMaskIntoConstraints = false
        totalTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        fromToLabel.translatesAutoresizingMaskIntoConstraints = false
        seeStopsButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            checkMarkIcon.topAnchor.constraint(equalTo: topAnchor, constant: LayoutValues.iconTopPadding),
            checkMarkIcon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.iconLeftPadding),
            checkMarkIcon.heightAnchor.constraint(equalToConstant: LayoutValues.iconHeight),
            checkMarkIcon.widthAnchor.constraint(equalTo: checkMarkIcon.heightAnchor),
            
            outboundLabel.centerYAnchor.constraint(equalTo: checkMarkIcon.centerYAnchor),
            outboundLabel.leadingAnchor.constraint(equalTo: checkMarkIcon.trailingAnchor, constant: LayoutValues.outboundLeftPadding),
            
            trainNameLabel.centerYAnchor.constraint(equalTo: checkMarkIcon.centerYAnchor),
            trainNameLabel.leadingAnchor.constraint(equalTo: outboundLabel.trailingAnchor, constant: LayoutValues.trainNameLeftPadding),
            
            totalTimeLabel.centerYAnchor.constraint(equalTo: checkMarkIcon.centerYAnchor),
            totalTimeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            
            dateLabel.topAnchor.constraint(equalTo: checkMarkIcon.bottomAnchor, constant: LayoutValues.dateTopPadding),
            dateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            dateLabel.trailingAnchor.constraint(lessThanOrEqualTo: seeStopsButton.leadingAnchor),
            
            timeLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor),
            timeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            timeLabel.trailingAnchor.constraint(lessThanOrEqualTo: seeStopsButton.leadingAnchor),
            
            fromToLabel.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: LayoutValues.fromToPadding),
            fromToLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: LayoutValues.marginPadding),
            fromToLabel.trailingAnchor.constraint(lessThanOrEqualTo: seeStopsButton.leadingAnchor),
            fromToLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -LayoutValues.fromToPadding),
            
            seeStopsButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -LayoutValues.marginPadding),
            seeStopsButton.bottomAnchor.constraint(equalTo: fromToLabel.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    public func getDateTimeLabelText() -> String {
        return dateLabel.text ?? ""
    }
    
    public func getTrainNumberLabelText() -> String {
        return trainNameLabel.text ?? ""
    }
    
    public func setDetails(trainName: String, totalTime: String, date: String, time: String, fromTo: String) {
        trainNameLabel.text = trainName
        totalTimeLabel.text = totalTime
        dateLabel.text = date
        timeLabel.text = time
        fromToLabel.text = fromTo
    }
    
    @objc private func didTapSeeStopsButton() {
        didTapSeeStopsButtonClosure?()
    }
}

extension TicketTopDetailsView {
    private enum LayoutValues {
        static let iconTopPadding: CGFloat = 13.5
        static let iconLeftPadding: CGFloat = 18
        static let iconHeight: CGFloat = 20
        
        static let outboundLeftPadding: CGFloat = 10
        static let trainNameLeftPadding: CGFloat = 5
        
        static let dateTopPadding: CGFloat = 5
        static let fromToPadding: CGFloat = 5
        
        static let marginPadding: CGFloat = 16
        
        static let viewHeight: CGFloat = 90
    }
}
